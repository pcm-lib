/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 * <liu090@sina.com>
 */

#ifndef _KINETS_SGTL5000_H_
#define _KINETS_SGTL5000_H_

/* soc relation head file */
#include "dma/edma.h"

struct soc_dma_data {
	MQX_EDMA_HARDWARE_REQUEST peripheral_type;
	int priority;
	MQX_EDMA_SCATTER_STRUCT_PTR desc;
	unsigned short fifo_aligned_bits;	/* fifo aligned bits in bit */
	unsigned short fifo_align_type;
	I2S_MemMapPtr ssi_base;
};

#define SSI_FMT_TX_ASYNCHRONOUS	(0x01)
#define SSI_FMT_TX_SYNCHRONOUS	(0x02)
#define SSI_FMT_RX_ASYNCHRONOUS	(0x04)
#define SSI_FMT_RX_SYNCHRONOUS	(0x08)

#define SSI_FMT_TX_BCLK_NORMAL	(0x10)
#define SSI_FMT_TX_BCLK_SWAPPED	(0x20)
#define SSI_FMT_RX_BCLK_NORMAL	(0x40)
#define SSI_FMT_RX_BCLK_SWAPPED	(0x80)

#define SSI_FMT_TX_MASTER			(0x100)
#define SSI_FMT_TX_SLAVE			(0x200)

#define SSI_FMT_RX_MASTER			(0x400)
#define SSI_FMT_RX_SLAVE			(0x800)

/*
** SAI Clock sources
*/
#define SSI_FMT_CLK_INT     (0x1000)
#define SSI_FMT_CLK_EXT     (0x2000)

#define SSI_FMT_BCP_INV       (0x4000)
#define SSI_FMT_FSP_INV 	(0x8000)


#define SSI_FMT_CHAR_BIT    (0x08)

#define FIFO_ALIGNMENT_TYPE_RIGHT   0x00
#define FIFO_ALIGNMENT_TYPE_LEFT    0x01

#define FIFO_BITS_ALIGNED                   32
#define FIFO_BITS_ENDTYPES               FIFO_ALIGNMENT_TYPE_LEFT

#if 0
/* sgtl5000 private structure in codec */
struct sgtl5000_priv {
	int sysclk;	/* sysclk rate */
	int master;	/* i2s master or not */
	int fmt;	/* i2s data format */
};
#endif

#if 0
inline void  soc_pcm_lock(void) 
{
	_bsp_int_disable(_bsp_get_edma_done_vector(0));
}

inline void  soc_pcm_unlock(void) 
{
	_bsp_int_enable(_bsp_get_edma_done_vector(0));
}
#endif


#endif /* _KINETS_SGTL5000_H_ */
