/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 * <liu090@sina.com>
 */

#ifndef _KINETS_K60DAC_H_
#define _KINETS_K60DAC_H_

/* soc relation head file */
//#include "dma/edma.h"

struct soc_dma_data {
	int priority;
	unsigned short fifo_aligned_bits;	/* fifo aligned bits in bit */
	unsigned short fifo_align_type;
      /*
	DAC_MemMapPtr dac0_base;
	DAC_MemMapPtr dac1_base;
      ADC_MemMapPtr adc0_base;
      ADC_MemMapPtr adc1_base;
      */
      volatile void * dev0_base;
      volatile void * dev1_base;
};

typedef struct{
    uint32_t SADDR;                                  /**< TCD Source Address, array offset: 0x1000, array step: 0x20 */
    uint16_t SOFF;                                   /**< TCD Signed Source Address Offset, array offset: 0x1004, array step: 0x20 */
    uint16_t ATTR;                                   /**< TCD Transfer Attributes, array offset: 0x1006, array step: 0x20 */
    union {                                          /* offset: 0x1008, array step: 0x20 */
      uint32_t NBYTES_MLNO;                            /**< TCD Minor Byte Count (Minor Loop Disabled), array offset: 0x1008, array step: 0x20 */
      uint32_t NBYTES_MLOFFNO;                         /**< TCD Signed Minor Loop Offset (Minor Loop Enabled and Offset Disabled), array offset: 0x1008, array step: 0x20 */
      uint32_t NBYTES_MLOFFYES;                        /**< TCD Signed Minor Loop Offset (Minor Loop and Offset Enabled), array offset: 0x1008, array step: 0x20 */
    };
    uint32_t SLAST;                                  /**< TCD Last Source Address Adjustment, array offset: 0x100C, array step: 0x20 */
    uint32_t DADDR;                                  /**< TCD Destination Address, array offset: 0x1010, array step: 0x20 */
    uint16_t DOFF;                                   /**< TCD Signed Destination Address Offset, array offset: 0x1014, array step: 0x20 */
    union {                                          /* offset: 0x1016, array step: 0x20 */
      uint16_t CITER_ELINKYES;                         /**< TCD Current Minor Loop Link, Major Loop Count (Channel Linking Enabled), array offset: 0x1016, array step: 0x20 */
      uint16_t CITER_ELINKNO;                          /**< TCD Current Minor Loop Link, Major Loop Count (Channel Linking Disabled), array offset: 0x1016, array step: 0x20 */
    };
    uint32_t DLAST_SGA;                              /**< TCD Last Destination Address Adjustment/Scatter Gather Address, array offset: 0x1018, array step: 0x20 */
    uint16_t CSR;                                    /**< TCD Control and Status, array offset: 0x101C, array step: 0x20 */
    union {                                          /* offset: 0x101E, array step: 0x20 */
      uint16_t BITER_ELINKNO;                          /**< TCD Beginning Minor Loop Link, Major Loop Count (Channel Linking Disabled), array offset: 0x101E, array step: 0x20 */
      uint16_t BITER_ELINKYES;                         /**< TCD Beginning Minor Loop Link, Major Loop Count (Channel Linking Enabled), array offset: 0x101E, array step: 0x20 */
    };
} DMA_TCD_t, *DMA_TCD_p;

#define MAX_TCD_LISTS				 4   /* must match the number of periods  ! */
#define FIFO_ALIGNMENT_TYPE_RIGHT	0x00
#define FIFO_ALIGNMENT_TYPE_LEFT	0x01

#define FIFO_BITS_ALIGNED		16
#define FIFO_BITS_ENDTYPES		FIFO_ALIGNMENT_TYPE_LEFT

/* Enable global DACx Interrup bit*/
#define VECTOR_DAC0_INT_MASK	2
#define VECTOR_DAC1_INT_MASK	4

/* DAXx registers reset values*/
#define DACx_DAT_RESET			0
#define DACx_SR_RESET			2
#define DACx_C0_RESET			0
#define DACx_C1_RESET			0
#define DACx_C2_RESET			15 //0x0f

/* DACx_C0 bits definition*/

#define DAC_DISABLE				0x00
#define DAC_ENABLE				DAC_C0_DACEN_MASK

#define DAC_SEL_VREFO			0x00
#define DAC_SEL_VDDA			DAC_C0_DACRFS_MASK

#define DAC_SEL_PDB_HW_TRIG	0x00
#define DAC_SEL_SW_TRIG		DAC_C0_DACTRGSEL_MASK

#define DAC_SW_TRIG_STOP		0x00
#define DAC_SW_TRIG_NEXT		DAC_C0_DACSWTRG_MASK

#define DAC_HP_MODE			0x00
#define DAC_LP_MODE			DAC_C0_LPEN_MASK

#define DAC_BFWM_INT_DISABLE	0x00
#define DAC_BFWM_INT_ENABLE	DAC_C0_DACBWIEN_MASK

#define DAC_BFT_PTR_INT_DISABLE	0x00
#define DAC_BFT_PTR_INT_ENABLE		DAC_C0_DACBTIEN_MASK

#define DAC_BFB_PTR_INT_DISABLE	0x00
#define DAC_BFB_PTR_INT_ENABLE	DAC_C0_DACBBIEN_MASK

/* DACx_C1 bits definition*/ 
#define DAC_DMA_DISABLE		0x00
#define DAC_DMA_ENABLE			DAC_C1_DMAEN_MASK

#define DAC_BFWM_1WORD		DAC_C1_DACBFWM(0)  
#define DAC_BFWM_2WORDS		DAC_C1_DACBFWM(1) 
#define DAC_BFWM_3WORDS		DAC_C1_DACBFWM(2)  
#define DAC_BFWM_4WORDS		DAC_C1_DACBFWM(3) 

#define DAC_BF_NORMAL_MODE	DAC_C1_DACBFMD(0)
#define DAC_BF_SWING_MODE		DAC_C1_DACBFMD(1) 
#define DAC_BF_ONE_TIME_MODE	DAC_C1_DACBFMD(2)

#define DAC_BF_DISABLE			0x00
#define DAC_BF_ENABLE			DAC_C1_DACBFEN_MASK 

/* DACx_C2 bits definition*/ 
#define DAC_SET_PTR_AT_BF(x)	DAC_C2_DACBFRP(x)
#define DAC_SET_PTR_UP_LIMIT(x)	DAC_C2_DACBFUP(x)


#define Watermark_One_Word		0
#define Watermark_Two_Words	1
#define Watermark_Three_Words	2
#define Watermark_Four_Words	3

#define Clear_DACBFWMF			0x03
#define Clear_DACBFRPTF			0x05
#define Clear_DACBFRPBF			0x06

#define PDB_DACINTC0_TOE_MASK	0x1u
///////////////////////////////////////////
#define DEFAULT_AUDIO_SAMPLERATE	44100

#define  DAM16WORD_TWICE

/* simulate */
#define DMA_CHANNEL_DAC0 			0

#define DMA_CHANNEL_ADC1 			1

#ifdef DAM16WORD_TWICE
#define DAC_BUFFER_SIZE			(0x10)
#else
#define DAC_BUFFER_SIZE			(0x10 * 2)
#endif
#define AUDIO_DMA_BUFFER_SIZE		0x200
#define DMA_CH0_ISR_NUM			0
#define DMA_ERROR_ISR_NUM			16
#define DMA_REQUEST_DAC0			45
//#define DMA_REQUEST_DAC1			46
#define DMA_REQUEST_ADC1       /*MQX_EDMA_HARD_REQ_ADC1*/41

#define SAMPLE_RATE_44100HZ		44100
#define SAMPLE_RATE_1KHZ			1000

//#define DAC_TWO_CHANNEL


// adc
#define ADC_CFG2_ADLSTS_20             ADC_CFG2_ADLSTS(0)
#define ADC_CFG2_ADLSTS_12             ADC_CFG2_ADLSTS(1)
#define ADC_CFG2_ADLSTS_6              ADC_CFG2_ADLSTS(2)
#define ADC_CFG2_ADLSTS_2              ADC_CFG2_ADLSTS(3)
#define ADC_CFG2_ADLSTS_DEFAULT       (ADC_CFG2_ADLSTS_20)

#define ADC_SC3_AVGS_4                 ADC_SC3_AVGS(0x00)
#define ADC_SC3_AVGS_8                 ADC_SC3_AVGS(0x01)
#define ADC_SC3_AVGS_16                ADC_SC3_AVGS(0x02)
#define ADC_SC3_AVGS_32                ADC_SC3_AVGS(0x03)
#define ADC_SC3_AVGE                   ADC_SC3_AVGE_MASK
#define ADC_SC3_ADCO                   ADC_SC3_ADCO_MASK
#define ADC_SC3_CALF                   ADC_SC3_CALF_MASK
#define ADC_SC3_CAL                    ADC_SC3_CAL_MASK

#define ADC_SC2_REFSEL_VREF            ADC_SC2_REFSEL(0x00)
#define ADC_SC2_REFSEL_VALT            ADC_SC2_REFSEL(0x01)
#define ADC_SC2_REFSEL_VBG             ADC_SC2_REFSEL(0x02)

#define ADC_SC1_ADCH_DISABLED          ADC_SC1_ADCH(0x1F)

#define ADC_CFG1_ADIV_1                ADC_CFG1_ADIV(0x00)
#define ADC_CFG1_ADIV_2                ADC_CFG1_ADIV(0x01)
#define ADC_CFG1_ADIV_4                ADC_CFG1_ADIV(0x02)
#define ADC_CFG1_ADIV_8                ADC_CFG1_ADIV(0x03)

#define ADC_CFG1_ADICLK_BUSCLK         ADC_CFG1_ADICLK(0x00)
#define ADC_CFG1_ADICLK_BUSCLK2        ADC_CFG1_ADICLK(0x01)
#define ADC_CFG1_ADICLK_ALTCLK         ADC_CFG1_ADICLK(0x02)
#define ADC_CFG1_ADICLK_ADACK          ADC_CFG1_ADICLK(0x03)


//// ADCSC1 (register)

// Conversion Complete (COCO) mask
#define COCO_COMPLETE     ADC_SC1_COCO_MASK
#define COCO_NOT          0x00

// ADC interrupts: enabled, or disabled.
#define AIEN_ON           ADC_SC1_AIEN_MASK
#define AIEN_OFF          0x00

// Differential or Single ended ADC input
#define DIFF_SINGLE       0x00
#define DIFF_DIFFERENTIAL ADC_SC1_DIFF_MASK

//// ADCCFG1
// Power setting of ADC
#define ADLPC_LOW         ADC_CFG1_ADLPC_MASK
#define ADLPC_NORMAL      0x00

// Clock divisor
#define ADIV_1            0x00
#define ADIV_2            0x01
#define ADIV_4            0x02
#define ADIV_8            0x03

// Long samle time, or Short sample time
#define ADLSMP_LONG       ADC_CFG1_ADLSMP_MASK
#define ADLSMP_SHORT      0x00

// How many bits for the conversion?  8, 12, 10, or 16 (single ended).
#define MODE_8            0x00
#define MODE_12           0x01
#define MODE_10           0x02
#define MODE_16           0x03

// ADC Input Clock Source choice? Bus clock, Bus clock/2, "altclk", or the 
//                                ADC's own asynchronous clock for less noise
#define ADICLK_BUS        0x00
#define ADICLK_BUS_2      0x01
#define ADICLK_ALTCLK     0x02
#define ADICLK_ADACK      0x03

//// ADCCFG2

// Select between B or A channels
#define MUXSEL_ADCB       ADC_CFG2_MUXSEL_MASK
#define MUXSEL_ADCA       0x00

// Ansync clock output enable: enable, or disable the output of it
#define ADACKEN_ENABLED   ADC_CFG2_ADACKEN_MASK
#define ADACKEN_DISABLED  0x00

// High speed or low speed conversion mode
#define ADHSC_HISPEED     ADC_CFG2_ADHSC_MASK
#define ADHSC_NORMAL      0x00

// Long Sample Time selector: 20, 12, 6, or 2 extra clocks for a longer sample time
#define ADLSTS_20          0x00
#define ADLSTS_12          0x01
#define ADLSTS_6           0x02
#define ADLSTS_2           0x03

////ADCSC2

// Read-only status bit indicating conversion status
#define ADACT_ACTIVE       ADC_SC2_ADACT_MASK
#define ADACT_INACTIVE     0x00

// Trigger for starting conversion: Hardware trigger, or software trigger.
// For using PDB, the Hardware trigger option is selected.
#define ADTRG_HW           ADC_SC2_ADTRG_MASK
#define ADTRG_SW           0x00

// ADC Compare Function Enable: Disabled, or Enabled.
#define ACFE_DISABLED      0x00
#define ACFE_ENABLED       ADC_SC2_ACFE_MASK

// Compare Function Greater Than Enable: Greater, or Less.
#define ACFGT_GREATER      ADC_SC2_ACFGT_MASK
#define ACFGT_LESS         0x00

// Compare Function Range Enable: Enabled or Disabled.
#define ACREN_ENABLED      ADC_SC2_ACREN_MASK
#define ACREN_DISABLED     0x00

// DMA enable: enabled or disabled.
#define DMAEN_ENABLED      ADC_SC2_DMAEN_MASK
#define DMAEN_DISABLED     0x00

// Voltage Reference selection for the ADC conversions
// (***not*** the PGA which uses VREFO only).
// VREFH and VREFL (0) , or VREFO (1).

#define REFSEL_EXT         0x00
#define REFSEL_ALT         0x01
#define REFSEL_RES         0x02     /* reserved */
#define REFSEL_RES_EXT     0x03     /* reserved but defaults to Vref */

////ADCSC3

// Calibration begin or off
#define CAL_BEGIN          ADC_SC3_CAL_MASK
#define CAL_OFF            0x00

// Status indicating Calibration failed, or normal success
#define CALF_FAIL          ADC_SC3_CALF_MASK
#define CALF_NORMAL        0x00

// ADC to continously convert, or do a sinle conversion
#define ADCO_CONTINUOUS    ADC_SC3_ADCO_MASK
#define ADCO_SINGLE        0x00

// Averaging enabled in the ADC, or not.
#define AVGE_ENABLED       ADC_SC3_AVGE_MASK
#define AVGE_DISABLED      0x00

// How many to average prior to "interrupting" the MCU?  4, 8, 16, or 32
#define AVGS_4             0x00
#define AVGS_8             0x01
#define AVGS_16            0x02
#define AVGS_32            0x03

////PGA

// PGA enabled or not?
#define PGAEN_ENABLED      ADC_PGA_PGAEN_MASK
#define PGAEN_DISABLED     0x00 

// Chopper stabilization of the amplifier, or not.
#define PGACHP_CHOP        ADC_PGA_PGACHP_MASK 
#define PGACHP_NOCHOP      0x00

// PGA in low power mode, or normal mode.
#define PGALP_LOW          ADC_PGA_PGALP_MASK
#define PGALP_NORMAL       0x00

// Gain of PGA.  Selectable from 1 to 64.
#define PGAG_1             0x00
#define PGAG_2             0x01
#define PGAG_4             0x02
#define PGAG_8             0x03
#define PGAG_16            0x04
#define PGAG_32            0x05
#define PGAG_64            0x06

#define ADC_SIG_PORTA   (0x01 << 5)
#define ADC_SIG_PORTB   (0x02 << 5)
#define ADC_SIG_PORTC   (0x03 << 5)
#define ADC_SIG_PORTD   (0x04 << 5)
#define ADC_SIG_PORTE   (0x05 << 5)

#if 0
#define PDB_CH_EN(adc_num)\
        * (uint_8 *) (&PDB0_BASE_PTR->CH[adc_num].C1)
#else
#define PIT_MCR_MDIS      (1<<1)
#define PIT_MCR_FRZ       (1<<0)

#define PIT_TCTRL_TIE     (1<<1)
#define PIT_TCTRL_TEN     (1<<0)

#define PIT_TFLG_TIF      (1<<0)
#endif

#endif /* _KINETS_K60DAC_H_ */
