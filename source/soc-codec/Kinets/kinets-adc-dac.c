/*
 * kinets-K60dac.c --  SoC audio for kinets tower boards with
 *                                  dac0 and dac1 (44100HZ ,16bit,2channel)
 *
 */

#include "pcm.h"

#ifdef KINETS_K60ADC_DAC

#include "kinets-adc-dac.h"

#if defined ( __ICCARM__ ) /* IAR Ewarm 5.41+ */
#pragma data_alignment=32
static DMA_TCD_t tcd_list0[MAX_TCD_LISTS];
#pragma data_alignment=32
static DMA_TCD_t tcd_list1[MAX_TCD_LISTS];
#else /*elif defined (  __GNUC__  )*/  /* GCC CS3 2009q3-68 */
/*static*/ DMA_TCD_t tcd_list0[MAX_TCD_LISTS] __attribute__ ((aligned (32)));
/*static*/ DMA_TCD_t tcd_list1[MAX_TCD_LISTS] __attribute__ ((aligned (32)));
#endif

static void dma_load_td_chain(unsigned int ch, DMA_TCD_p tcd_list);
static void dacc_init(struct snd_pcm_runtime *runtime,uint_8 BuffMode,uint_8 Vreference,
						uint_8 TrigMode, uint_8 BuffInitPos, uint_8 BuffUpLimit,uint_8 WatermarkMode);
static void dacc_deinit(struct snd_pcm_runtime *runtime);
static int dac_digital_mute(int mute);
static int preinit_params(struct snd_pcm_runtime *runtime, struct snd_pcm_params *params);
static int codec_pcm_interface_params(struct snd_pcm_runtime *runtime,
				  							struct snd_pcm_params *params);
static int cpu_interface_params(struct snd_pcm_runtime *runtime, struct snd_pcm_params *params);
static int dma_pcm_params(struct snd_pcm_runtime *runtime, struct snd_pcm_params *params);
static void dma_start_channel(unsigned int ch);
static void dma_stop_channel(unsigned int ch);
static inline unsigned short dac_format_in_data (struct snd_pcm_runtime *runtime, uint16_t *inbuf);

static void adcc_init(struct snd_pcm_runtime *runtime);
static void adcc_deinit(struct snd_pcm_runtime *runtime);
static int adc_set_clock(int frame_rate);
static int adc_digital_mute(int mute);
static inline uint_16 adc_format_out_data(struct snd_pcm_runtime *runtime, uint16_t *inbuf);


// global pcm_mutex declare

/* ticks implement */
/* now return is system elapsed ms */
unsigned int  get_curr_ticks(void)
{
	TIME_STRUCT tm;
	MQX_TICK_STRUCT tick1;
	
	_time_get_elapsed_ticks(&tick1);
	/* _time_diff_ticks(&tick2, &tick1, &tick3); */
	_ticks_to_time(&tick1,&tm);
	/* os_printf("%d-%d \n",tm.SECONDS,tm.MILLISECONDS); */
	return (tm.SECONDS * 1000 +  tm.MILLISECONDS);
}

unsigned int  get_hz(void)
{
	return /* _time_get_ticks_per_sec()*/1000;
}

void sco_msleep(unsigned int ms)
{
#if 0
	unsigned int  ticks = ms / (1000/_time_get_ticks_per_sec());
	if(ticks == 0)
		ticks = 1;
	_time_delay(ticks);
#endif
    OS_Time_delay(ms);
}

void  sco_pcm_lock(struct snd_pcm_runtime *runtime)
{
      if(runtime->stream == SNDRV_PCM_STREAM_PLAYBACK)
	    _bsp_int_disable(_bsp_get_edma_done_vector(DMA_CHANNEL_DAC0));
      else
	    _bsp_int_disable(_bsp_get_edma_done_vector(DMA_CHANNEL_ADC1));
	//_int_disable();
}

void  sco_pcm_unlock(struct snd_pcm_runtime *runtime)
{
    if(runtime->stream == SNDRV_PCM_STREAM_PLAYBACK)
	_bsp_int_enable(_bsp_get_edma_done_vector(DMA_CHANNEL_DAC0));
    else
	_bsp_int_enable(_bsp_get_edma_done_vector(DMA_CHANNEL_ADC1));
	//_int_enable();
}

static struct soc_dma_data  edma_data[2];        /* play index 0 and capture index 1 */

int sco_pcm_open(struct snd_pcm_runtime *runtime)
{
	int ret = 0;

	//os_mutex_lock(&pcm_mutex);

	struct sco_pcm_runtime_data *iprtd = (struct sco_pcm_runtime_data *) runtime->private_data;
	struct soc_dma_data * dma_data;

	/* don't allocat ,use gloabal */
      //SNDRV_PCM_STREAM_PLAYBACK index 0
	iprtd->dma_data = (void *)(&edma_data[runtime->stream]);
	os_memset(iprtd->dma_data, 0, sizeof(struct soc_dma_data));

	dma_data = (struct soc_dma_data *)iprtd->dma_data;
      	if(runtime->stream == SNDRV_PCM_STREAM_PLAYBACK) {
		dma_data->dev0_base = DAC0_BASE_PTR;
		dma_data->dev1_base = DAC1_BASE_PTR;
      	}
      else {
		dma_data->dev0_base = ADC0_BASE_PTR;
		dma_data->dev1_base = ADC1_BASE_PTR;
      }

	/* codec ,power, path init at this */

	/* startup the audio subsystem sequence */
	/*  -> dma -> dac -> misc */
	if(runtime->stream == SNDRV_PCM_STREAM_PLAYBACK)
		dacc_init(runtime,DAC_BF_NORMAL_MODE,DAC_SEL_VDDA,
		   DAC_SEL_PDB_HW_TRIG,DAC_SET_PTR_AT_BF(0),DAC_SET_PTR_UP_LIMIT(15),DAC_BFWM_4WORDS);
      else 
		adcc_init(runtime);

	//os_mutex_unlock(&pcm_mutex);
	
	return ret;
}

int sco_pcm_prepare(struct snd_pcm_runtime *runtime)
{
	int ret = 0;

	//os_mutex_lock(&pcm_mutex);
	/* dma prepare => dac prepare */
    if(runtime->stream == SNDRV_PCM_STREAM_PLAYBACK)
	dac_digital_mute(0);
    else
      adc_digital_mute(0);  
	//os_mutex_unlock(&pcm_mutex);
out:
	return ret;
}

int sco_pcm_close(struct snd_pcm_runtime *runtime)
{
	struct sco_pcm_runtime_data *iprtd = (struct sco_pcm_runtime_data *)runtime->private_data;
	struct soc_dma_data * dma_data = (struct soc_dma_data *)iprtd->dma_data;

	//os_mutex_lock(&pcm_mutex);
	if (runtime->stream == SNDRV_PCM_STREAM_PLAYBACK) {  /* playback */
            dac_digital_mute(0);
            dacc_deinit(runtime);
	}
	else {
		adc_digital_mute(0);   
		adcc_deinit(runtime);
	}
	
	/* sequence of close
	cpu pcm interface -> codec -> dma */

	//PCM_mem_free(dma_data);	// Don't free it, because it's using global.
	iprtd->dma_data = NULL;

	if(runtime->dma_area)
		PCM_mem_free(runtime->dma_area);

	/* at this can free runtime member => struct sco_pcm_runtime_data *iprtd */
	//os_mutex_unlock(&pcm_mutex);
	return 0;
}

int sco_pcm_params(struct snd_pcm_runtime *runtime,
				struct snd_pcm_params *params)
{
	int ret = 0;

	// os_mutex_lock(&pcm_mutex);
	/* set paramaters before codec and dma set it */
	ret = preinit_params(runtime,params);   /* preinit_params can move to sco_pcm_open */
	if (ret < 0) {
		printf("soc: preinit set params failed\n");
		goto out;
	}

	/* code interface set params */
	ret = codec_pcm_interface_params(runtime,params);
	if (ret < 0) {
		printf("soc: can't set codec params\n");
		goto codec_err;
	}

	/* cpu interface set params */
	ret = cpu_interface_params(runtime,params);
	if (ret < 0) {
		printf("soc: interface set params failed\n");
		goto interface_err;
	}

	ret = dma_pcm_params(runtime,params);
	if (ret < 0) {
		printf("soc: dma set params failed\n");
		goto dma_err;
	}
			 
	/* statue to SNDRV_PCM_STATE_SETUP after params setup complete */
	// snd_pcm_drop(runtime);  // fixme use drop before the prepare,failed
	runtime->status->state = SNDRV_PCM_STATE_SETUP;
	
out:
	//os_mutex_unlock(&pcm_mutex);
	return ret;

dma_err:

interface_err:

codec_err:

	//os_mutex_unlock(&pcm_mutex);
	return ret;
}

/* dma operation functions */
/******************************************************************************/
int sco_pcm_trigger(struct snd_pcm_runtime *runtime, int cmd)
{
	struct sco_pcm_runtime_data *iprtd = (struct sco_pcm_runtime_data *) runtime->private_data;
	struct soc_dma_data * dma_data = (struct soc_dma_data *)iprtd->dma_data;
	int ret;
	/* cmd handle sequance : codec => edma => ssi */
	switch (cmd) {
	case SNDRV_PCM_TRIGGER_START:
	case SNDRV_PCM_TRIGGER_RESUME:
	case SNDRV_PCM_TRIGGER_PAUSE_RELEASE:

        if (runtime->stream == SNDRV_PCM_STREAM_PLAYBACK) {
		//edma_start(SSI_TX_DMA_CHN);  // fixme only star tx
		//DAC_C0_REG(dma_data->dac0_base) |=  ((DAC_C0_DACEN_MASK));
		PDB0_DACINTC0 |= PDB_DACINTC0_TOE_MASK ;
		dma_start_channel(DMA_CHANNEL_DAC0);
		PDB0_SC |= PDB_SC_SWTRIG_MASK ;
        }
        else {
            dma_start_channel(DMA_CHANNEL_ADC1);
            PIT_TCTRL3 = (/*PIT_TCTRL_TIE|*/PIT_TCTRL_TEN);
        }
        break;

	case SNDRV_PCM_TRIGGER_STOP:
	case SNDRV_PCM_TRIGGER_SUSPEND:
	case SNDRV_PCM_TRIGGER_PAUSE_PUSH:
        if (runtime->stream == SNDRV_PCM_STREAM_PLAYBACK) {
		//PDB0_SC &=  (~PDB_SC_SWTRIG_MASK) ;
		dma_stop_channel(DMA_CHANNEL_DAC0);
		PDB0_DACINTC0 &= ~PDB_DACINTC0_TOE_MASK;
		//DAC_C0_REG(dma_data->dac0_base) &=  (~(DAC_C0_DACEN_MASK));
        }
        else {
            dma_stop_channel(DMA_CHANNEL_ADC1);
            PIT_TCTRL3 &= ~(/*PIT_TCTRL_TIE|*/PIT_TCTRL_TEN);
        }
	  break;
	default:
		return -1;
	}
	
	return 0;
}

int sco_pcm_stream_copy (struct snd_pcm_runtime *runtime, int channel, snd_pcm_uframes_t hwoff, char *buf, snd_pcm_uframes_t count)
{
	struct sco_pcm_runtime_data *iprtd = (struct sco_pcm_runtime_data *)runtime->private_data;
	struct soc_dma_data * dma_data = (struct soc_dma_data *)iprtd->dma_data;

	unsigned short * hwbuf_ch0 = (unsigned short *)((char *)iprtd->buf + frames_to_bytes(runtime, hwoff) / runtime->channels /*2*/);
	uint16_t *in_out_buf =  (uint16_t *)buf;
      
	for(int i = 0; i < (count); i++) {
		//os_memcpy(hwbuf, buf, frames_to_bytes(runtime, frames));
        if (runtime->stream == SNDRV_PCM_STREAM_PLAYBACK)
		*hwbuf_ch0 = dac_format_in_data(runtime, &in_out_buf[i/**2*/]);   // liunote input is 1ch 16bit
        else
             in_out_buf[i/**2*/] = adc_format_out_data(runtime ,hwbuf_ch0);
        
	  hwbuf_ch0 ++;
	}
	return 0;
}

static void dacc_init(struct snd_pcm_runtime *runtime,uint_8 BuffMode,uint_8 Vreference, 
	uint_8 TrigMode, uint_8 BuffInitPos, uint_8 BuffUpLimit,uint_8 WatermarkMode)
{
	struct sco_pcm_runtime_data *iprtd = (struct sco_pcm_runtime_data *)runtime->private_data;
	struct soc_dma_data * dma_data = (struct soc_dma_data *)iprtd->dma_data;
	DAC_MemMapPtr dac0_ptr = dma_data->dev0_base;
	DAC_MemMapPtr dac1_ptr = dma_data->dev1_base;
	uint32_t regval;

	SIM_SCGC2 |= (SIM_SCGC2_DAC0_MASK | SIM_SCGC2_DAC1_MASK);   // Allow clock to enable DAC0 ,DAC1

	regval = (
#ifdef DAM16WORD_TWICE
	DAC_BFB_PTR_INT_DISABLE |             
	DAC_BFT_PTR_INT_ENABLE |             
	DAC_BFWM_INT_ENABLE  |
#else
	DAC_BFB_PTR_INT_ENABLE |             
	DAC_BFT_PTR_INT_DISABLE |             
	DAC_BFWM_INT_DISABLE  |             
#endif	    
	DAC_HP_MODE    |
	DAC_SW_TRIG_STOP |            
	TrigMode |  
	Vreference |     
	DAC_ENABLE
	/* DAC_DISABLE */
	);

	DAC_C0_REG(dac0_ptr) = regval;
#ifdef DAC_TWO_CHANNEL
	DAC_C0_REG(dac1_ptr) = regval;
#endif
	
	if (Vreference == DAC_SEL_VREFO ) {
		SIM_SCGC4 |= !SIM_SCGC4_VREF_MASK ;
		VREF_SC = 0x82;
		while (!(VREF_SC & VREF_SC_VREFST_MASK)  ){}
	}

	regval = (
		DAC_C1_DMAEN_MASK | /* dac dma mode  */
		DAC_BF_ENABLE  |   //Buffer Enabled
		WatermarkMode |  // set 1, 2, 3,or 4 word(s) mode
		BuffMode               // set traversal mode, normal, swing, or onetime
		) ;

	DAC_C1_REG(dac0_ptr) = regval;
#ifdef DAC_TWO_CHANNEL		
	DAC_C1_REG(dac1_ptr) = regval;
#endif

	regval = BuffInitPos | BuffUpLimit;
	DAC_C2_REG(dac0_ptr) = regval;
#ifdef DAC_TWO_CHANNEL	
	DAC_C2_REG(dac1_ptr) = regval;
#endif

	/* pdb0 init */
	SIM_SCGC6 |= SIM_SCGC6_PDB_MASK ;    // SCGC6 is at address 0x4004_803Ch
	PDB0_SC = PDB_SC_PDBEN_MASK;         	// enable PDB module

}

static void dacc_deinit(struct snd_pcm_runtime *runtime)
{
	struct sco_pcm_runtime_data *iprtd = (struct sco_pcm_runtime_data *)runtime->private_data;
	struct soc_dma_data * dma_data = (struct soc_dma_data *)iprtd->dma_data;
	DAC_MemMapPtr dac0_ptr = dma_data->dev0_base;
	DAC_MemMapPtr dac1_ptr = dma_data->dev1_base;
	uint32_t regval;

	/* move to trigger stop */
	regval =  DAC_DISABLE;
	DAC_C0_REG(dac0_ptr) = regval;
	DAC_C0_REG(dac1_ptr) = regval;

#if (MQX_CPU == PSP_CPU_MK70F120M)
	_bsp_int_disable(INT_DMA0_DMA16);
#elif (MQX_CPU == PSP_CPU_MK60D100M )
	_bsp_int_disable(INT_DMA0);
#else
	#error "only support K70,K60 chip"
#endif
	
	SIM_SCGC2 &= (~ (SIM_SCGC2_DAC0_MASK | SIM_SCGC2_DAC1_MASK));   // Disable clock to enable DAC0 ,DAC1

	/* pdb0 deinit */
	PDB0_SC &= (~PDB_SC_PDBEN_MASK);         	// enable PDB module
	SIM_SCGC6 &= (~SIM_SCGC6_PDB_MASK) ;    // SCGC6 is at address 0x4004_803Ch
}

static int dac_set_clock(int frame_rate)
{
	/* sampleRates[source] = samplerate; */
	uint32_t regval;
	// Effective after writting PDBSC_DACTOE = 1  (1/48000KHz) * 1088 = 0.0226757369614512ms (44100)
#if (MQX_CPU == PSP_CPU_MK70F120M)
	regval = 60000000/frame_rate;
#elif (MQX_CPU == PSP_CPU_MK60D100M )
	regval = 48000000/frame_rate;
#else
	#error "only support K70,K60 chip"
#endif
	regval -= 1;
	
	PDB0_DACINT0 = regval;
#ifdef DAC_TWO_CHANNEL
	PDB0_DACINT1 = regval;
#endif

	PDB0_SC |= PDB_SC_LDOK_MASK;      	 // load the value assigned to PDBDACINTV to register
	PDB0_SC |= PDB_SC_TRGSEL(15);        	 // software trigger
	PDB0_SC |= PDB_SC_CONT_MASK ;         	 // CONT mode
	// move to trigger start
	//PDB0_DACINTC0 |= PDB_DACINTC0_TOE_MASK ;
	//PDB0_DACINTC1 |= PDB_DACINTC0_TOE_MASK ;

      return 0;
}

static int adc_calibrate(ADC_MemMapPtr adc_ptr)
{
    uint_16 cal_var;
    struct {
        uint_8  SC2;
        uint_8  SC3;
    } saved_regs;

    if (adc_ptr == NULL)
        return -1; /* no such ADC peripheral exists */

    saved_regs.SC2 = adc_ptr->SC2;
    saved_regs.SC3 = adc_ptr->SC3;

    /* Enable Software Conversion Trigger for Calibration Process */
    adc_ptr->SC2 &= ~ADC_SC2_ADTRG_MASK;

    /* Initiate calibration */
    adc_ptr->SC3 |= ADC_SC3_CAL;

    /* Wait for conversion to complete */
    while (adc_ptr->SC2 & ADC_SC2_ADACT_MASK){}

    /* Check if calibration failed */
    if (adc_ptr->SC3 & ADC_SC3_CALF_MASK)  {
        /* Clear calibration failed flag */
        adc_ptr->SC3 |= ADC_SC3_CALF_MASK;

        /* calibration failed */
        return -1;
    }

    /* Calculate plus-side calibration values according
     * to Calibration function described in reference manual.   */
    /* 1. Initialize (clear) a 16b variable in RAM */
    cal_var  = 0x0000;
    /* 2. Add the following plus-side calibration results CLP0, CLP1,
     *    CLP2, CLP3, CLP4 and CLPS to the variable. */
    cal_var +=  adc_ptr->CLP0;
    cal_var +=  adc_ptr->CLP1;
    cal_var +=  adc_ptr->CLP2;
    cal_var +=  adc_ptr->CLP3;
    cal_var +=  adc_ptr->CLP4;
    cal_var +=  adc_ptr->CLPS;

    /* 3. Divide the variable by two. */
    cal_var = cal_var / 2;

    /* 4. Set the MSB of the variable. */
    cal_var += 0x8000;

    /* 5. Store the value in the plus-side gain calibration registers ADCPGH and ADCPGL. */
    adc_ptr->PG = cal_var;
#if 1 //ADC_HAS_DIFFERENTIAL_CHANNELS // K70 K60 has diff end
    /* Repeat procedure for the minus-side gain calibration value. */
    /* 1. Initialize (clear) a 16b variable in RAM */
    cal_var  = 0x0000;

    /* 2. Add the following plus-side calibration results CLM0, CLM1,
     *    CLM2, CLM3, CLM4 and CLMS to the variable. */
    cal_var  = 0x0000;
    cal_var += adc_ptr->CLM0;
    cal_var += adc_ptr->CLM1;
    cal_var += adc_ptr->CLM2;
    cal_var += adc_ptr->CLM3;
    cal_var += adc_ptr->CLM4;
    cal_var += adc_ptr->CLMS;

    /* 3. Divide the variable by two. */
    cal_var = cal_var / 2;

    /* 4. Set the MSB of the variable. */
    cal_var += 0x8000;

    /* 5. Store the value in the minus-side gain calibration registers ADCMGH and ADCMGL. */
    adc_ptr->MG = cal_var;
#endif /* ADC_HAS_DIFFERENTIAL_CHANNELS */
    /* Clear CAL bit */
    adc_ptr->SC3 = saved_regs.SC3;
    adc_ptr->SC2 = saved_regs.SC2;

    return 0;
}

/* for K60 */
static void adcc_init(struct snd_pcm_runtime *runtime)
{
    struct sco_pcm_runtime_data *iprtd = (struct sco_pcm_runtime_data *)runtime->private_data;
    struct soc_dma_data * dma_data = (struct soc_dma_data *)iprtd->dma_data;
    ADC_MemMapPtr adc_ptr = (ADC_MemMapPtr)(dma_data->dev1_base);  /* use adc1 */
    PORT_MemMapPtr pctl = NULL;
    uint_8 gpio_port;

    /* SIM_SCGC6: ADC0=1 */
    //SIM_SCGC6 |= SIM_SCGC6_ADC0_MASK;
    SIM_SCGC3 |= SIM_SCGC3_ADC1_MASK;

#if 1
    SIM_SCGC6 |= SIM_SCGC6_PIT_MASK;                 /* enable PIT */

    SIM_SOPT7 |= SIM_SOPT7_ADC1ALTTRGEN_MASK;
    SIM_SOPT7 |= (SIM_SOPT7_ADC1PRETRGSEL_MASK); /* Pre Trigger B */
    SIM_SOPT7 &= ~(SIM_SOPT7_ADC1TRGSEL_MASK); 
    SIM_SOPT7 |= SIM_SOPT7_ADC1TRGSEL(7);               /* PIT trigger 3 */

    PIT_MCR_REG(PIT_BASE_PTR) = 0;
#endif    

    gpio_port = ADC_SIG_PORTB | 4;
    pctl = (PORT_MemMapPtr) PORTB_BASE_PTR;
    pctl->PCR[gpio_port & 0x1F] &= ~PORT_PCR_MUX_MASK; /* set pin's multiplexer to analog */

    // Initialize ADC1 =================================================================================
    //averages (4 h/w)   // bus 48M /8/2 = 3M ADC clock
    ADC_CFG1_REG(adc_ptr) = ADLPC_NORMAL | ADC_CFG1_ADIV(ADIV_8) | ADLSMP_LONG | ADC_CFG1_MODE(MODE_16)
      | ADC_CFG1_ADICLK(ADICLK_BUS_2);

    // do calibration
    if(adc_calibrate(adc_ptr))
        pr_err("adc0 calibrate failed\n");

    /* channel A selected */
    ADC_CFG2_REG(adc_ptr)  = /*MUXSEL_ADCA*/  MUXSEL_ADCB   | ADACKEN_ENABLED | ADHSC_HISPEED | ADC_CFG2_ADLSTS(ADLSTS_20);
    //ADC_CV1_REG(adc_ptr) = 0x0;
    //ADC_CV2_REG(adc_ptr) = 0x0;
    ADC_SC1_REG(adc_ptr,0) =  AIEN_ON | DIFF_SINGLE | ADC_SC1_ADCH(10);   /* PTB4 */  /* SC1 A */
    ADC_SC1_REG(adc_ptr,1) =  AIEN_ON | DIFF_SINGLE | ADC_SC1_ADCH(10);   /* PTB4 */  /* SC1 B */

    ADC_SC2_REG(adc_ptr) = ADTRG_HW /*ADTRG_SW*/ | ACFE_DISABLED | ACFGT_GREATER | ACREN_DISABLED
                     | /*DMAEN_DISABLED*/DMAEN_ENABLED | ADC_SC2_REFSEL(REFSEL_EXT);
    ADC_SC3_REG(adc_ptr) = CAL_OFF | ADCO_SINGLE | AVGE_ENABLED | ADC_SC3_AVGS(AVGS_4);  /* averages 4 h/w */
    ADC_PGA_REG(adc_ptr) &= ~ADC_PGA_PGAG_MASK;
    ADC_PGA_REG(adc_ptr) |= 0x1 | 0x00100000 | ADC_PGA_PGAEN_MASK;  /* PGAG set to one */

#if 0 // pdb0 and pre triger
    /* pdb0 init ,todo sync with DAC */
    SIM_SCGC6 |= SIM_SCGC6_PDB_MASK ;    // SCGC6 is at address 0x4004_803Ch
    PDB0_SC = PDB_SC_PDBEN_MASK;         	// enable PDB module

    PDB0_BASE_PTR->SC &= ~PDB_SC_LDMOD_MASK;
    PDB0_BASE_PTR->SC |= PDB_SC_LDMOD(0x00); /* write to MOD and DLY registers immediately */

    // adc1's adc num = 1
    PDB_CH_EN(1) = 0; /* no channel enabled yet */
    PDB0_BASE_PTR->CH[1].C1 |= PDB_C1_TOS_MASK; /* select pre-trigger as a function of delay for all channels */
    PDB0_BASE_PTR->CH[1].C1 &= ~PDB_C1_BB_MASK; /* don't use back-to-back mode, use delayed channel conversions instead */
    SIM_SOPT7_REG &= ~SIM_SOPT7_ADC1ALTTRGEN_MASK; /* set PDB to be source for ADC conversion start */
#endif
}

static void adcc_deinit(struct snd_pcm_runtime *runtime)
{
#if (MQX_CPU == PSP_CPU_MK70F120M)
	_bsp_int_disable(INT_DMA1_DMA17);
#elif (MQX_CPU == PSP_CPU_MK60D100M )
	_bsp_int_disable(INT_DMA1);
#else
	#error "only support K70,K60 chip"
#endif
	PIT_MCR_REG(PIT_BASE_PTR) =  PIT_MCR_MDIS;
	SIM_SCGC3 &= (~SIM_SCGC3_ADC1_MASK);
	SIM_SCGC6 &= ~SIM_SCGC6_PIT_MASK; /* disable PIT */
}

static int adc_set_clock(int frame_rate)
{
    uint32_t period;
    period = (uint32_t)(((BSP_BUS_CLOCK << 1)/frame_rate + 1) >> 1);
    PIT_TCTRL3 = 0;
    PIT_LDVAL3 = period - 1;
    /* Clear any pending interrupt */
    PIT_TFLG3 = PIT_TFLG_TIF;
    /* Enable timer but not interrupt , in trigger function */
    //PIT_TCTRL3 = (/*PIT_TCTRL_TIE|*/PIT_TCTRL_TEN);
}

static int  dma_tcd_list_init(struct snd_pcm_runtime *runtime,unsigned int ch)
{
	int i = 0;
	struct sco_pcm_runtime_data *iprtd = (struct sco_pcm_runtime_data *) runtime->private_data;
	struct soc_dma_data * dma_data = (struct soc_dma_data *)iprtd->dma_data;

	uint32_t length = bytes_fifo_aligned(iprtd->period_bytes, dma_data->fifo_aligned_bits, runtime->sample_phy_bits) / runtime->channels/*2*/;
	uint32_t tcd_lists_cnt = iprtd->periods;

	DMA_TCD_t * tcd_list;
	unsigned int  dac_base_ptr;

	//if (tcd_lists_cnt > MAX_TCD_LISTS)
	//	return -1;
	ASSERT(tcd_lists_cnt <= MAX_TCD_LISTS);
    
	if(ch == DMA_CHANNEL_DAC0) {
		tcd_list = tcd_list0;
		dac_base_ptr = ((unsigned int) (DAC0_BASE_PTR ));
	}
	else if(ch == DMA_CHANNEL_ADC1) {
		tcd_list = tcd_list1;
		dac_base_ptr = ((unsigned int) (ADC1_BASE_PTR));
	}
	else 
		return -1;

	os_memset(tcd_list, 0, sizeof(tcd_list));

	for(; i < tcd_lists_cnt; i++) {
		if (ch == DMA_CHANNEL_DAC0) {
			tcd_list[i].SOFF = 1;
#ifdef DAM16WORD_TWICE
			tcd_list[i].ATTR = DMA_ATTR_SSIZE(0) | DMA_ATTR_DSIZE(1) | DMA_ATTR_DMOD(5);
#else
			// 8bits for D&S, 16bytes wrapped, ie, 4 address bit changing allowed.
			tcd_list[i].ATTR = DMA_ATTR_SSIZE(0) | DMA_ATTR_DSIZE(0) /*| DMA_ATTR_DMOD(5)*/;
#endif
			// We use scatter-gether mode, and the SADDR will be reloaded, but this field still need be set properly.  
			tcd_list[i].SLAST = 0;// -AUDIO_DMA_BUFFER_SIZE; 
#ifdef DAM16WORD_TWICE
			tcd_list[i].DOFF = 2;
			tcd_list[i].DADDR = ((unsigned int) (dac_base_ptr )) + 16; 
#else
			tcd_list[i].DOFF = 1;
			tcd_list[i].DADDR = (unsigned int) dac_base_ptr; // DAC DATA buffer base address
#endif

#ifdef DAM16WORD_TWICE
			tcd_list[i].NBYTES_MLOFFNO = DMA_NBYTES_MLOFFNO_NBYTES(DAC_BUFFER_SIZE);
#else
			/*tcd_list[i].NBYTES_MLOFFNO = DMA_NBYTES_MLOFFNO_NBYTES(DAC_BUFFER_SIZE);*/
			tcd_list[i].NBYTES_MLOFFYES = (DMA_NBYTES_MLOFFYES_DMLOE_MASK | 
			DMA_NBYTES_MLOFFYES_MLOFF(-DAC_BUFFER_SIZE) |        // Destination address minus 32 ever minor_loop
			DMA_NBYTES_MLOFFYES_NBYTES(DAC_BUFFER_SIZE));        // 32 bytes per minor-loop
#endif
			tcd_list[i].SADDR =  (uint_32)(((uint8_t *)iprtd->buf) + (i * length));
		
			tcd_list[i].CITER_ELINKNO = length/DAC_BUFFER_SIZE;
			tcd_list[i].BITER_ELINKNO = length/DAC_BUFFER_SIZE;
      
			tcd_list[i].CSR = (DMA_CSR_ESG_MASK | DMA_CSR_INTMAJOR_MASK);

			if (i != (tcd_lists_cnt - 1))
				tcd_list[i].DLAST_SGA = (unsigned int)(&tcd_list[i+1]);
		}
		else {  // capture
			tcd_list[i].SOFF = 1;
			// 8bits .
			tcd_list[i].ATTR = DMA_ATTR_SSIZE(0) | DMA_ATTR_DSIZE(0) /*| DMA_ATTR_DMOD(5)*/;
			// We use scatter-gether mode, and the SADDR will be reloaded, but this field still need be set properly.  
			tcd_list[i].SLAST = 0;// -AUDIO_DMA_BUFFER_SIZE; 
			tcd_list[i].DOFF = 1;
			tcd_list[i].DADDR = (uint_32)(((uint8_t *)iprtd->buf) + (i * length)); //(unsigned int) dac_base_ptr; // DAC DATA buffer base address

			/*tcd_list[i].NBYTES_MLOFFNO = DMA_NBYTES_MLOFFNO_NBYTES(DAC_BUFFER_SIZE);*/
			tcd_list[i].NBYTES_MLOFFYES = (DMA_NBYTES_MLOFFYES_SMLOE_MASK | 
			DMA_NBYTES_MLOFFYES_MLOFF(-2) |
			DMA_NBYTES_MLOFFYES_NBYTES(2));
			//tcd_list[i].NBYTES_MLNO = DMA_NBYTES_MLNO_NBYTES(2);

			tcd_list[i].SADDR =  &(ADC_R_REG(ADC1_BASE_PTR,1));  // ADC1_RB  // liunote
			// (uint_32)(((uint8_t *)iprtd->buf) + (i * length));
		
			tcd_list[i].CITER_ELINKNO = length / 2;   /* There are  2 bytes to be transfered when minor loop was compeleting */
			tcd_list[i].BITER_ELINKNO = length / 2;
      
			tcd_list[i].CSR = (DMA_CSR_ESG_MASK | DMA_CSR_INTMAJOR_MASK);

			if (i != (tcd_lists_cnt - 1))
				tcd_list[i].DLAST_SGA = (unsigned int)(&tcd_list[i+1]);
		}
	}
  
	tcd_list[tcd_lists_cnt - 1].DLAST_SGA = (unsigned int)(&tcd_list[0]);
	dma_load_td_chain(ch, &tcd_list[0]);

	return 0;
}

static void dma_load_td_chain(unsigned int ch, DMA_TCD_p tcd_list)
{
    DMA_SADDR(ch) = tcd_list->SADDR;
    DMA_SOFF(ch) = tcd_list->SOFF;
    DMA_ATTR(ch) = tcd_list->ATTR;
    DMA_NBYTES_MLOFFYES(ch) = tcd_list->NBYTES_MLOFFYES;
    DMA_SLAST(ch) = tcd_list->SLAST;
  
    DMA_DADDR(ch) = tcd_list->DADDR;
    DMA_DOFF(ch) = tcd_list->DOFF;
    DMA_CITER_ELINKNO(ch) = tcd_list->CITER_ELINKNO;
    DMA_DLAST_SGA(ch) = tcd_list->DLAST_SGA;
    DMA_CSR(ch)   = tcd_list->CSR;
    DMA_BITER_ELINKNO(ch) = tcd_list->BITER_ELINKNO;
}

static inline void dma_start_channel(unsigned int ch)
{
	DMA_SERQ = DMA_SERQ_SERQ(ch);
	//DMA_CSR_REG(DMA_BASE_PTR,ch) = (/*DMA_CSR_DREQ_MASK |*/ DMA_CSR_INTMAJOR_MASK);
}

static void dma_stop_channel(unsigned int ch)
{
	do {
		DMA_CERQ_REG(DMA_BASE_PTR) = ch;
	} while(DMA_CSR_REG(DMA_BASE_PTR, ch) & DMA_CSR_ACTIVE_MASK);
}

static inline uint_16 dac_format_in_data (struct snd_pcm_runtime *runtime, uint16_t *inbuf)
{
	uint_16 retval = 0;
#if 0   /* for normal pcm signed 16bit input */
	int32_t temp = (int16_t)(inbuf[0]);
	temp *= (2 * 90); /* vol = 50 (min 1- max 256) */;
	temp = temp >> (5 + 8);
	temp += 2048;
	temp &= 0xfff; // XXX dither the sample
	
	retval = temp;
#else  /* for adk2012 a2dp input (12bit) or adc loopback */
	/*
	int32_t temp =  *((int16_t *)(inbuf));
	temp += 32768;
	temp = temp >> 4;
	temp &= 0xfff;
	retval = temp;
	*/
	retval = inbuf[0];
#endif
	return  ((unsigned short) retval);
}

static inline uint_16 adc_format_out_data(struct snd_pcm_runtime *runtime, uint16_t *inbuf)
{
	uint_16 retval = 0;
	retval = inbuf[0] >> 4;  /* change to 12bit to dac12,when use dac to ouput this pcm stream */
      retval *= 2;                 /* up volume */
	return  ((unsigned short) retval);
}
	
/* codec and ssi preinit set params  */
static int preinit_params(struct snd_pcm_runtime *runtime,
	struct snd_pcm_params *params)
{
	uint32_t intf_format;
	int ret;
	unsigned int channels = params->channels;

	/* open and init clock */
	return 0;
}

/* mute the dac */
static int dac_digital_mute(int mute)
{
	// todo
	return 0;
}

/* mute the adc */
static int adc_digital_mute(int mute)
{
	// todo
	return 0;
}

static int codec_pcm_interface_params(struct snd_pcm_runtime *runtime,
				  struct snd_pcm_params *params)
{
	//if (runtime->stream != SNDRV_PCM_STREAM_PLAYBACK )
	//	return -1;
	if ((params->format != SNDRV_PCM_FORMAT_S16_LE) && (params->format != SNDRV_PCM_FORMAT_U16))
		return -1;

    if (runtime->stream == SNDRV_PCM_STREAM_PLAYBACK)
        return dac_set_clock(params->rate);
    else
        return adc_set_clock(params->rate);
}

static int cpu_interface_params(struct snd_pcm_runtime *runtime,
			     struct snd_pcm_params *params)
{
	struct sco_pcm_runtime_data *iprtd = (struct sco_pcm_runtime_data *)runtime->private_data;
	struct soc_dma_data * dma_data = (struct soc_dma_data *)iprtd->dma_data;
	int data_bits = 0;
	unsigned int channels = params->channels;
	
	return 0;
}

static void dac_edma_interrupt_handler(void *  data)
{
	audio_dma_irq(data,DMA_CHANNEL_DAC0);
	DMA_CINT_REG(DMA_BASE_PTR) = DMA_CHANNEL_DAC0;
	DMA_CDNE_REG(DMA_BASE_PTR) = DMA_CHANNEL_DAC0;
}

static void adc_edma_interrupt_handler(void *  data)
{
    	audio_dma_irq(data,DMA_CHANNEL_ADC1);
	DMA_CINT_REG(DMA_BASE_PTR) = DMA_CHANNEL_ADC1;
	DMA_CDNE_REG(DMA_BASE_PTR) = DMA_CHANNEL_ADC1;
}

void dma_mux(unsigned int channel, unsigned int request)
{
#if (MQX_CPU == PSP_CPU_MK70F120M)
	DMAMUX0_CHCFG(channel) = (DMAMUX_CHCFG_ENBL_MASK | DMAMUX_CHCFG_SOURCE(request));
#elif (MQX_CPU == PSP_CPU_MK60D100M )
		DMAMUX_CHCFG(channel) |= (DMAMUX_CHCFG_ENBL_MASK | DMAMUX_CHCFG_SOURCE(request));
#else
		#error "only support K70,K60 chip"
#endif
}

static int dma_pcm_params(struct snd_pcm_runtime *runtime,
				struct snd_pcm_params *params)
{
	int ret;
	unsigned int bits;
	snd_pcm_uframes_t frames;
	unsigned int buffer_bytes_adjusted = 0;
	unsigned int buffer_bytes = 0;

	struct sco_pcm_runtime_data *iprtd = (struct sco_pcm_runtime_data *) runtime->private_data;
	struct soc_dma_data * dma_data = (struct soc_dma_data *)iprtd->dma_data;

	if(!sys_dma_inited) {
		sys_dma_inited = 1;
		printf("dma inited\n");
#if	(MQX_CPU == PSP_CPU_MK70F120M)
		SIM_SCGC6 |= SIM_SCGC6_DMAMUX0_MASK; 
#elif  (MQX_CPU == PSP_CPU_MK60D100M )
		SIM_SCGC6 |= SIM_SCGC6_DMAMUX_MASK; 	 // Enable clocks to the DMA Mux.
#else
		#error "only support K70,K60 chip"
#endif
		SIM_SCGC7 |= SIM_SCGC7_DMA_MASK;    	 // Enable clocks to the eDMA module.
		//dma_mux(DMA_CHANNEL_DAC0, DMA_REQUEST_DAC0);

		// clear flags
		DMA_CEEI |= DMA_CEEI_CAEE_MASK;
		DMA_CERQ |= DMA_CERQ_CAER_MASK;
		DMA_CDNE |= DMA_CDNE_CADN_MASK;
		DMA_CERR |= DMA_CERR_CAEI_MASK;
		DMA_CINT |= DMA_CINT_CAIR_MASK;

		DMA_EEI = 0x00;

		/*
		For proper operation, writes to the CR register must be
		performed only when the DMA channels are inactive; that is,
		when TCDn_CSR[ACTIVE] bits are cleared
		*/
		// enable minor off mode
	
#if (MQX_CPU == PSP_CPU_MK70F120M)
		DMA_CR|= DMA_CR_EDBG_MASK;
#elif (MQX_CPU == PSP_CPU_MK60D100M)
    //enable minor off mode
		DMA_CR = DMA_CR_EMLM_MASK;   /* all dma ch use minor loop */
#else
		#error "only support K70,K60 chip"
#endif

	}

    if (runtime->stream == SNDRV_PCM_STREAM_PLAYBACK)
        dma_mux(DMA_CHANNEL_DAC0, DMA_REQUEST_DAC0);
    else
        dma_mux(DMA_CHANNEL_ADC1, DMA_REQUEST_ADC1);
    
	/* Default  params */
	dma_data->fifo_aligned_bits = FIFO_BITS_ALIGNED;
	dma_data->fifo_align_type = FIFO_BITS_ENDTYPES;
	runtime->sample_aligned_bits = dma_data->fifo_aligned_bits;  /* sample_aligned_bits used by pcm.c */
	
	if (params->periods != 0) {  /* params periods high priority */
		runtime->buffer_size = runtime->period_size * params->periods;
		runtime->periods = params->periods;
		buffer_bytes = frames_to_bytes(runtime,runtime->buffer_size);
	}
	else if(params->buffer_bytes != 0) {
		runtime->buffer_size = bytes_to_frames(runtime,params->buffer_bytes);
		runtime->periods =  runtime->buffer_size /runtime->period_size;
		buffer_bytes = params->buffer_bytes;
	}
	else {
		pr_err("err: both params periods and buffer_bytes all zero!\n");
		return -1;
	}

	buffer_bytes_adjusted = bytes_fifo_aligned(buffer_bytes,
			                                 dma_data->fifo_aligned_bits, runtime->sample_phy_bits); /* div runtime->sample_phy_bits is error !*/

	/* runtime->avail_min = runtime->period_size; */

	if ((runtime->periods > MAX_PERIODS) || (runtime->periods < MIN_PERIODS)) {
		pr_err("err: periods number\n");
		return -1;
	}
	
	runtime->boundary = runtime->buffer_size;
	while (runtime->boundary * 2 <= LONG_MAX - runtime->buffer_size)
		runtime->boundary *= 2;

	if (params->start_threshold == THRESHOLD_BOUNDARY)
		runtime->start_threshold = runtime->boundary;
	else	
		runtime->start_threshold = params->start_threshold;

	if (params->stop_threshold == THRESHOLD_BOUNDARY)
		runtime->stop_threshold = runtime->boundary;
	else
		runtime->stop_threshold =  params->stop_threshold;

	if (params->stop_threshold == THRESHOLD_BOUNDARY)
		runtime->silence_threshold = runtime->boundary;
	else
		runtime->silence_threshold = params->silence_threshold;
	runtime->silence_size = params->silence_size;  			/* runtime->period_size */

	runtime->dma_area = PCM_mem_alloc_zero(buffer_bytes_adjusted);
	if(!runtime->dma_area) {
		printf("alloc dma buffer failed\n");
		return -1;
	}
	pr_debug("dma buffer size : snd need = %d,soc allocate %d\n",buffer_bytes,buffer_bytes_adjusted);

	{
		iprtd->size = frames_to_bytes(runtime, runtime->buffer_size);
		iprtd->periods = runtime->periods;
		iprtd->period_bytes = frames_to_bytes(runtime,runtime->period_size);
		iprtd->offset = 0;
		iprtd->period_time = (HZ * runtime->period_size + (runtime->rate -1)) /runtime->rate;
		iprtd->buf = (unsigned int *)runtime->dma_area;
	}

	runtime->dma_bytes =  frames_to_bytes(runtime,runtime->buffer_size) * runtime->sample_aligned_bits / runtime->sample_phy_bits;

    if (runtime->stream == SNDRV_PCM_STREAM_PLAYBACK)
	dma_tcd_list_init(runtime,DMA_CHANNEL_DAC0);
    else
	dma_tcd_list_init(runtime,DMA_CHANNEL_ADC1);

	/* only install DAC0 dma marjor interrput */
#if (MQX_CPU == PSP_CPU_MK70F120M)
        #error "don't support K70"
	//_int_install_isr(INT_DMA0_DMA16, edma_interrupt_handler, (void *)(runtime));
	//_bsp_int_init(INT_DMA0_DMA16, DMA_INTR_PRIO, 0, TRUE);
#elif (MQX_CPU == PSP_CPU_MK60D100M)
     if (runtime->stream == SNDRV_PCM_STREAM_PLAYBACK) {
	_int_install_isr(INT_DMA0, dac_edma_interrupt_handler, (void *)(runtime));
	_bsp_int_init(INT_DMA0, DMA_INTR_PRIO, 0, TRUE);
      }
      else{
        _int_install_isr(INT_DMA1, adc_edma_interrupt_handler, (void *)(runtime));
	  _bsp_int_init(INT_DMA1, DMA_INTR_PRIO, 0, TRUE);
      }

    
#else
	#error "only support K60 chip"
#endif

	/* snd_pcm_prepare(runtime); */   /* drop and setup then prepare !!!  ( prepare always reset all pointer !!! ) */

	return 0;
}

#define MAX_SAMPLE_SKEW  50
static int audsamplerate;
static int audsampleratebase = 44100;

void dac_skew_sample_rate(int skew)
{
	uint32_t regval;
	audsamplerate += skew;
	if (audsamplerate - audsampleratebase > MAX_SAMPLE_SKEW)
		audsamplerate = audsampleratebase + MAX_SAMPLE_SKEW;
	if (audsampleratebase - audsamplerate > MAX_SAMPLE_SKEW)
		audsamplerate = audsampleratebase - MAX_SAMPLE_SKEW;

	//dac_set_clock(audsamplerate);
#if (MQX_CPU == PSP_CPU_MK70F120M)	
	regval = 60000000/audsamplerate;
#elif (MQX_CPU == PSP_CPU_MK60D100M )
	regval = 48000000/audsamplerate;
#else
	#error "only support K70,K60 chip"
#endif

	regval -= 1;
#ifdef DAC_TWO_CHANNEL
	PDB0_DACINT1 = regval;
#endif
	PDB0_DACINT0 = regval;
	PDB0_SC |= PDB_SC_LDOK_MASK;
}

int sco_pcm_vol_down()
{
	return 0;
}

int sco_pcm_vol_up()
{
	return 0;
}

#endif  // KINETS_K60ADC_DAC
