/*
 * kinets-sgtl5000.c --  SoC audio for kinets tower boards with
 *                                  sgtl5000 codec
 *
 * <liu090@sina.com>
 */

#include "pcm.h"

#include "sgtl5000.h"
#include "dma/edma.h"
#include "i2c/i2c_kinets.h"

#ifdef KINETS_SGTL5000

#include "kinets-sgtl5000.h"

#define DELTA_VOL 0x08
#define MAX_VOL 0x78
#define MIN_VOL 0x00

static uint16_t hp_vol = 0x1818;
static uint16_t dac_vol = 0x3c3c;

static int cpu_interface_startup(struct snd_pcm_runtime *runtime);
static int ldo_regulator_enable(int vol);
static int dma_pcm_params(struct snd_pcm_runtime *runtime,
				struct snd_pcm_params *params);
static int cpu_interface_params(struct snd_pcm_runtime *runtime,
				struct snd_pcm_params *params);
static int codec_pcm_interface_params(struct snd_pcm_runtime *runtime,
				struct snd_pcm_params *params);
static int preinit_params(struct snd_pcm_runtime *runtime,
				struct snd_pcm_params *params);


void  sco_pcm_lock(struct snd_pcm_runtime *runtime) 
{
	_bsp_int_disable(_bsp_get_edma_done_vector(0));
}

void  sco_pcm_unlock(struct snd_pcm_runtime *runtime)
{
	_bsp_int_enable(_bsp_get_edma_done_vector(0));
}


int sco_get_ticks_interval(struct snd_pcm_runtime *runtime)
{
#if 0
	MQX_TICK_STRUCT tick1,tick_diff;
	TIME_STRUCT tm;

	_time_get_elapsed_ticks(&tick1);
	_time_diff_ticks(&tick1, &tick_last, &tick_diff);
	tick_last = tick1;
	// printf("%d-%d-%d \n",tick1.TICKS[0],tick1.TICKS[1],tick1.HW_TICKS );

	// runtime->dma_total_interrupts ++;
	//runtime->hw_ptr_ticks_inteval = runtime->hw_ptr_ticks;
	// runtime->hw_ptr_ticks = get_curr_ticks();

	runtime->hw_ptr_ticks_inteval =  tick_diff.TICKS[0] * 100 * 5 + tick_diff.HW_TICKS /1000; /* (runtime->hw_ptr_ticks > runtime->hw_ptr_ticks_inteval )  ?  \
	(runtime->hw_ptr_ticks -runtime->hw_ptr_ticks_inteval) : 0; */
	//printf("%d-%d-%d-%d \n",tick_diff.TICKS[0],tick_diff.TICKS[1],tick_diff.HW_TICKS,runtime->hw_ptr_ticks_inteval );
#endif
	return 0;
}

/* ticks implement */
/* now return is system elapsed ms */
unsigned int  get_curr_ticks(void)
{
	TIME_STRUCT tm;
	MQX_TICK_STRUCT tick1;
	
	_time_get_elapsed_ticks(&tick1);
	/* _time_diff_ticks(&tick2, &tick1, &tick3); */
	_ticks_to_time(&tick1,&tm);
       /* os_printf("%d-%d \n",tm.SECONDS,tm.MILLISECONDS); */
		
	return (tm.SECONDS * 1000 +  tm.MILLISECONDS);
}

unsigned int  get_hz(void)
{
	return /* _time_get_ticks_per_sec() */1000;
}

void sco_msleep(unsigned int ms)
{
	//unsigned int  ticks = ms / (1000/_time_get_ticks_per_sec());
	int w ; 
	_time_delay(ms + 2); /* fix for mqx401 */ 
}

static struct soc_dma_data  edma_data[2];  /* play and capture */
MQX_EDMA_SCATTER_STRUCT_PTR	scatter_list;  /* malloc at init ,free at sco_pcm_close */


static unsigned int init_codec(struct snd_pcm_runtime *runtime)
{
#ifdef  CODEC_MASTER
	uint16_t  i2sctl = 0;
#endif

	static int inited_flag = 0;

	if(!inited_flag)  /* or move to global init */
		inited_flag = 1;
	else
		return(0);
		
#if 0        
	unsigned int  errcode = 0;

	errcode = I2C_Init();
	if (errcode != 0) {
		pr_debug("i2c init failed\n");
		return (errcode);
	}
#endif

	//--------------- Power Supply Configuration----------------
	// NOTE: This next 2 Write calls is needed ONLY if VDDD is
	// internally driven by the chip
	// Configure VDDD level to 1.2V (bits 3:0)
	sgtl_WriteReg(SGTL5000_CHIP_LINREG_CTRL, 0x0008);
	// Power up internal linear regulator (Set bit 9)
	sgtl_WriteReg(SGTL5000_CHIP_ANA_POWER, 0x7260);
	// NOTE: This next Write call is needed ONLY if VDDD is
	// externally driven
	// Turn off startup power supplies to save power (Clear bit 12 and 13)
	sgtl_WriteReg(SGTL5000_CHIP_ANA_POWER, 0x4260);

	// NOTE: The next 2 Write calls is needed only if both VDDA and
	// VDDIO power supplies are less than 3.1V.
	// Enable the internal oscillator for the charge pump (Set bit 11)
	sgtl_WriteReg(SGTL5000_CHIP_CLK_TOP_CTRL, 0x0800);
	// Enable charge pump (Set bit 11)
	sgtl_WriteReg(SGTL5000_CHIP_ANA_POWER, 0x4A60);
	// NOTE: The next 2 modify calls is only needed if both VDDA and
	// VDDIO are greater than 3.1V
	// Configure the charge pump to use the VDDIO rail (set bit 5 and bit 6)
	sgtl_WriteReg(SGTL5000_CHIP_LINREG_CTRL, 0x006C);

	//------ Reference Voltage and Bias Current Configuration----------
	// NOTE: The value written in the next 2 Write calls is dependent
	// on the VDDA voltage value.
	// Set ground, ADC, DAC reference voltage (bits 8:4). The value should
	// be set to VDDA/2. This example assumes VDDA = 1.8V. VDDA/2 = 0.9V.
	// The bias current should be set to 50% of the nominal value (bits 3:1)
	sgtl_WriteReg(SGTL5000_CHIP_REF_CTRL, 0x01FE);
	// Set LINEOUT reference voltage to VDDIO/2 (1.65V) (bits 5:0) and bias current
	// (bits 11:8) to the recommended value of 0.36mA for 10kOhm load with 1nF
	// capacitance
	sgtl_WriteReg(SGTL5000_CHIP_LINE_OUT_CTRL, 0x0322);

	//----------------Other Analog Block Configurations------------------
	// Configure slow ramp up rate to minimize pop (bit 0)
	sgtl_WriteReg(SGTL5000_CHIP_REF_CTRL, 0x01FF);
	// Enable short detect mode for headphone left/right
	// and center channel and set short detect current trip level
	// to 75mA
	sgtl_WriteReg(SGTL5000_CHIP_SHORT_CTRL, 0x1106);
	// Enable Zero-cross detect if needed for HP_OUT (bit 5) and ADC (bit 1)
	sgtl_WriteReg(SGTL5000_CHIP_ANA_CTRL, 0x0133);

	//----------------Power up Inputs/Outputs/Digital Blocks-------------
	// Power up LINEOUT, HP, ADC, DAC
	sgtl_WriteReg(SGTL5000_CHIP_ANA_POWER, 0x6AFF);
	// Power up desired digital blocks
	// I2S_IN (bit 0), I2S_OUT (bit 1), DAP (bit 4), DAC (bit 5),
	// ADC (bit 6) are powered on
	sgtl_WriteReg(SGTL5000_CHIP_DIG_POWER, 0x0073);

	//--------------------Set LINEOUT Volume Level-----------------------
	// Set the LINEOUT volume level based on voltage reference (VAG)
	// values using this formula
	// Value = (int)(40*log(VAG_VAL/LO_VAGCNTRL) + 15)
	// Assuming VAG_VAL and LO_VAGCNTRL is set to 0.9V and 1.65V respectively, the
	// left LO volume (bits 12:8) and right LO volume (bits 4:0) value should be set
	// to 5
	sgtl_WriteReg(SGTL5000_CHIP_LINE_OUT_VOL, 0x0505);

	// Configure SYS_FS clock to 48kHz
	// Configure MCLK_FREQ to 256*Fs
	//sgtl_ModifyReg(CHIP_CLK_CTRL, 0xFFC8, 0x0008); // bits 3:2
	// liutest
	sgtl_ModifyReg(SGTL5000_CHIP_CLK_CTRL, 0xFFFF, 0x0000);

#if 0
#ifdef  CODEC_MASTER
	i2sctl |= SGTL5000_I2S_MASTER;
	i2sctl |= SGTL5000_I2S_MODE_I2S_LJ;
	i2sctl |= (3 << 4); /* 16bit */
	i2sctl |= (1<< 8);
	
	sgtl_WriteReg(SGTL5000_CHIP_I2S_CTRL, i2sctl);  /* liu0911 set 16bit  */
#else
    // Configure the I2S clocks in master mode
    // NOTE: I2S LRCLK is same as the system sample clock
    // Data length = 16 bits
    sgtl_ModifyReg(CHIP_I2S_CTRL, 0xFFFF, 0x01B0); // bit 7
#endif
#endif

    // I2S_IN -> DAC -> HP_OUT
    // Route I2S_IN to DAC
    sgtl_ModifyReg(SGTL5000_CHIP_SSS_CTRL, 0xFFDF, 0x0010);
    // Select DAC as the input to HP_OUT
    sgtl_ModifyReg(SGTL5000_CHIP_ANA_CTRL, 0xFFBF, 0x0000);
    
    // TODO: Configure Microphone -> ADC -> I2S_OUT
    // Microphone -> ADC -> I2S_OUT
    // Set ADC input to Microphone
    //sgtl_ModifyReg(CHIP_ANA_CTRL, 0xFFFB, 0x0000); // bit 2
    sgtl_ModifyReg(SGTL5000_CHIP_ANA_CTRL, 0xFFFF, 0x0004); // Set ADC input to LINE_IN
    // Route ADC to I2S_OUT
    sgtl_ModifyReg(SGTL5000_CHIP_SSS_CTRL, 0xFFFC, 0x0000); // bits 1:0
    
    //---------------- Input Volume Control---------------------
    // Configure ADC left and right analog volume to desired default.
    // Example shows volume of 0dB
    sgtl_WriteReg(SGTL5000_CHIP_ANA_ADC_CTRL, 0x0000);
    // Configure MIC gain if needed. Example shows gain of 20dB
    sgtl_ModifyReg(SGTL5000_CHIP_MIC_CTRL, 0xFFFD, 0x0001); // bits 1:0
    
    //---------------- Volume and Mute Control---------------------
    // Configure HP_OUT left and right volume to minimum, unmute
    // HP_OUT and ramp the volume up to desired volume.
    sgtl_WriteReg(SGTL5000_CHIP_ANA_HP_CTRL, 0x7F7F);
    // Code assumes that left and right volumes are set to same value
    sgtl_WriteReg(SGTL5000_CHIP_ANA_HP_CTRL, 0x4040);
    sgtl_ModifyReg(SGTL5000_CHIP_ANA_CTRL, 0xFFEF, 0x0000); // bit 5	
    // LINEOUT and DAC volume control
    sgtl_ModifyReg(SGTL5000_CHIP_ANA_CTRL, 0xFEFF, 0x0000); // bit 8
    // Configure DAC left and right digital volume. Example shows
    // volume of 0dB
    sgtl_WriteReg(SGTL5000_CHIP_DAC_VOL, 0x3C3C);
    sgtl_ModifyReg(SGTL5000_CHIP_ADCDAC_CTRL, 0xFFFB, 0x0000); // bit 2
    sgtl_ModifyReg(SGTL5000_CHIP_ADCDAC_CTRL, 0xFFF7, 0x0000); // bit 3
    // Unmute ADC
    sgtl_ModifyReg(SGTL5000_CHIP_ANA_CTRL, 0xFFFE ,0x0000); // bit 0

#ifdef  CODEC_MASTER
		//ldo_regulator_enable(LDO_VOLTAGE);
		//sgtl_ModifyReg( SGTL5000_CHIP_ANA_POWER, ~SGTL5000_VAG_POWERUP, 0);
#endif

    return (0);
}

static inline uint_32 ssi_format_in_data (struct snd_pcm_runtime *runtime,/*unsigned*/ char * input)
{
	int_32 retval = 0;
	uint_8 i;
	uint_8 bits = /* pcm_format_physical_width(runtime->format); */runtime->sample_phy_bits;
	uint_8 bytes = bits / 8;

	retval |= (((uint_32)  *(input ) ) | ((uint_32)  (*(input + 1) <<  8)));
	if(bytes == 3) { 
		retval |= ((uint_32)  (*(input + 2) <<  16));
		/* only for sgtl master 24bit */
		retval = retval << 8;
	}

	return  ((unsigned) retval);
}

int sco_pcm_stream_copy (struct snd_pcm_runtime *runtime, int channel, snd_pcm_uframes_t hwoff, char *buf, snd_pcm_uframes_t count)
{
	struct sco_pcm_runtime_data *iprtd = (struct sco_pcm_runtime_data *)runtime->private_data;
	struct soc_dma_data * dma_data = (struct soc_dma_data *)iprtd->dma_data;
	
	int i;
	uint_8 bytes = /* pcm_format_width(runtime->format)*/runtime->sample_phy_bits / 8;
	char * hwbuf = (char * )(runtime->dma_area + frames_to_bytes(runtime, hwoff) * /* runtime->sample_aligned_bits*/
		dma_data->fifo_aligned_bits / /* runtime->sample_bits */runtime->sample_phy_bits);
	unsigned int * p_int = (unsigned int *)hwbuf;
	//uchar * p_buf = (uchar *)buf;
	for(i = 0; i < (count * runtime->channels); i++) {
		//os_memcpy(hwbuf, buf, frames_to_bytes(runtime, frames));
		*p_int = ssi_format_in_data(runtime,buf);
		p_int ++;
		buf += bytes;
	}
	return 0;
}

/* mute the codec */
static int codec_digital_mute(int mute)
{
	//uint16_t adcdac_ctrl = SGTL5000_DAC_MUTE_LEFT | SGTL5000_DAC_MUTE_RIGHT;
	//sgtl_ModifyReg(SGTL5000_CHIP_ADCDAC_CTRL, ~(adcdac_ctrl),  mute ? adcdac_ctrl : 0);
	if(mute)
		 sgtl_ModifyReg(SGTL5000_CHIP_ANA_CTRL, 0xFFFF, 0x0010); 	
	else
		 sgtl_ModifyReg(SGTL5000_CHIP_ANA_CTRL, 0xFFEF, 0x0000); 

	return 0;
}

int sco_pcm_prepare(struct snd_pcm_runtime *runtime)
{
	int ret = 0;

	/* dma prepare => ssi prepare */
	codec_digital_mute(0);

out:
	return ret;
}

int sco_pcm_open(struct snd_pcm_runtime *runtime)
{
	int ret = 0;

	// os_mutex_lock(&pcm_mutex);	
	struct sco_pcm_runtime_data *iprtd = (struct sco_pcm_runtime_data *) runtime->private_data;
	struct soc_dma_data * dma_data;

	//sco_pcm_lock();	
	/* don't allocat ,use gloabal */
	iprtd->dma_data = (void *)(&edma_data[runtime->stream]);
	os_memset(iprtd->dma_data, 0, sizeof(struct soc_dma_data));

	dma_data = (struct soc_dma_data *)iprtd->dma_data;
	dma_data->ssi_base = I2S0_BASE_PTR;

	/* codec ,power, path init at this */

	/* startup the audio subsystem */
	/* ssi -> dma -> codec -> misc */
	cpu_interface_startup(runtime); /* check with sai_init */

	init_codec(runtime);
	
	/* (open codec sysclk input ? ) */
	/* if 24567000 could open then do it at this */

	// os_mutex_unlock(&pcm_mutex);
	//sco_pcm_unlock();
	return ret;
}

int sco_pcm_params(struct snd_pcm_runtime *runtime,
				struct snd_pcm_params *params)
{
	int ret = 0;

	// os_mutex_lock(&pcm_mutex);
	/* set paramaters before codec and dma set it */
	ret = preinit_params(runtime,params);   /* fixme ,preinit_params move to sco_pcm_open ? */
	if (ret < 0) {
		printf("soc: preinit set params failed\n");
		goto out;
	}

	/* code interface set params */
	ret = codec_pcm_interface_params(runtime,params);
	if (ret < 0) {
		printf("soc: can't set codec params\n");
		goto codec_err;
	}

	/* cpu interface set params */
	ret = cpu_interface_params(runtime,params);
	if (ret < 0) {
		printf("soc: interface set params failed\n");
		goto interface_err;
	}

	ret = dma_pcm_params(runtime,params);
	if (ret < 0) {
		printf("soc: dma set params failed\n");
		goto dma_err;
	}
			 
	/* statue to SNDRV_PCM_STATE_SETUP after params setup complete */
	// snd_pcm_drop(runtime);  // fixme , use drop before the prepare but fail
	runtime->status->state = SNDRV_PCM_STATE_SETUP;
	
out:
	//os_mutex_unlock(&pcm_mutex);
	return ret;

dma_err:

interface_err:

codec_err:

	//os_mutex_unlock(&pcm_mutex);
	return ret;
}


/* dma operation functions */
/******************************************************************************/
int sco_pcm_trigger(struct snd_pcm_runtime *runtime, int cmd)
{
	struct sco_pcm_runtime_data *iprtd = (struct sco_pcm_runtime_data *) runtime->private_data;
	struct soc_dma_data * dma_data = (struct soc_dma_data *)iprtd->dma_data;
	I2S_MemMapPtr i2s_ptr = dma_data->ssi_base;
	int ret;

	/* cmd handle sequance : codec => edma => ssi */
	switch (cmd) {
	case SNDRV_PCM_TRIGGER_START:
	case SNDRV_PCM_TRIGGER_RESUME:
	case SNDRV_PCM_TRIGGER_PAUSE_RELEASE:
		//edma_start(SSI_TX_DMA_CHN);  // fixme only star tx
		edma_start(SSI_TX_DMA_CHN);
		i2s_ptr->TCSR |= (I2S_TCSR_FWDE_MASK | I2S_TCSR_FRDE_MASK);
		i2s_ptr->TCSR |= I2S_TCSR_TE_MASK;	

		break;

	case SNDRV_PCM_TRIGGER_STOP:
	case SNDRV_PCM_TRIGGER_SUSPEND:
	case SNDRV_PCM_TRIGGER_PAUSE_PUSH:
		edma_force_stop(SSI_TX_DMA_CHN);
		i2s_ptr->TCSR &= ~I2S_TCSR_TE_MASK;
		//edma_force_stop(SSI_TX_DMA_CHN);

		//edma_free_chan(SSI_TX_DMA_CHN); /* move to sco_pcm_close */

		break;
	default:
		return -1;
	}
	
	return 0;
}

int sco_pcm_close(struct snd_pcm_runtime *runtime)
{
	struct sco_pcm_runtime_data *iprtd = (struct sco_pcm_runtime_data *)runtime->private_data;
	struct soc_dma_data * dma_data = (struct soc_dma_data *)iprtd->dma_data;
	I2S_MemMapPtr i2s_ptr = dma_data->ssi_base;

	if (runtime->stream == SNDRV_PCM_STREAM_PLAYBACK)  /* only  playback */
		codec_digital_mute(0);

	// cpu pcm interface -> codec -> dma 
	edma_free_chan(SSI_TX_DMA_CHN);
	//edma_close_chan(SSI_TX_DMA_CHN);

	if(dma_data->desc)
		PCM_mem_free(dma_data->desc);

	//PCM_mem_free(dma_data);  // don't free, use global
	iprtd->dma_data = NULL;

	if(runtime->dma_area)
		PCM_mem_free(runtime->dma_area);

#ifndef CODEC_MASTER
	//clk_disable
	i2s_ptr->MCR &= ~(I2S_MCR_MOE_MASK);  /* this only use when codec slave mode */
#endif

	if(scatter_list) {
		PCM_mem_free(scatter_list);
		scatter_list = NULL;
	}

	/* at this can free runtime member => struct sco_pcm_runtime_data *iprtd */
	return 0;
}

int sco_pcm_vol_down()
{
	if(( hp_vol&0x7f ) >= MAX_VOL)	//already min volume
		return 0;
	hp_vol += (DELTA_VOL<<7 | DELTA_VOL);
	
	return sgtl_WriteReg(SGTL5000_CHIP_ANA_HP_CTRL, hp_vol);
}

int sco_pcm_vol_up()
{		
	if((hp_vol&0x7f) <= MIN_VOL)	//already max volume
		return 0;
	hp_vol -= (DELTA_VOL<<7 | DELTA_VOL);
	
	return sgtl_WriteReg(SGTL5000_CHIP_ANA_HP_CTRL, hp_vol);
}

static int cpu_interface_startup(struct snd_pcm_runtime *runtime)
{

	struct sco_pcm_runtime_data *iprtd = (struct sco_pcm_runtime_data *)runtime->private_data;
	struct soc_dma_data * dma_data = (struct soc_dma_data *)iprtd->dma_data;
	I2S_MemMapPtr i2s_ptr = dma_data->ssi_base;
	
	 _bsp_sai_io_init(/*HW_CHANNEL*/0);   /* I2S0 */

	//clk_enable
	if (runtime->stream == SNDRV_PCM_STREAM_PLAYBACK) { /* fixme ,only support playback */
		/* 
		** Software reset -> reset internal transmitter logic including the FIFO writer
		*/
		i2s_ptr->TCSR = 0;
		i2s_ptr->TCSR |= I2S_TCSR_SR_MASK;
		i2s_ptr->TCSR &= ~(I2S_TCSR_SR_MASK);
		/*
		** Enable transmitter in debug mode - this must be done else synchronous mode 
		** will not work while debugging.
		*/
		i2s_ptr->TCSR |= I2S_TCSR_DBGE_MASK;
		/* 
		** Set FIFO watermark
		*/
		i2s_ptr->TCR1 = I2S_TCR1_TFW(SSI_FIFO_DEFAULT_WATERMARK );
		/* 
		** Set the synch mode, clock polarity, master clock source and bit clock 
		*/
		i2s_ptr->TCR2 = 0;
#if 0		
		i2s_ptr->TCR2 |= (
			I2S_TCR2_BCP_MASK | /* Bit clock active low           */
			I2S_TCR2_MSEL(1)  | /* MCLK from divider (CLKGEN)     */
			I2S_TCR2_DIV(3)     /* Bit clock frequency = MCLK / 8 */
			);
#endif
		/*
		** First word in frame sets start of word flag
		*/
		i2s_ptr->TCR3 = 0;
		i2s_ptr->TCR3 |= (I2S_TCR3_WDFL(0));

		/* 
		** Enable selected transmitter channel
		*/
		/* 
		** Enable selected transmitter channel
		*/
		// TX_CHANNEL = 0
		i2s_ptr->TCR3 |= (I2S_TCR3_TCE(1));

		/*
		** Configure frame size, sync width, MSB and frame sync
		*/
		i2s_ptr->TCR4 = 0;
			i2s_ptr->TCR4 = (
				I2S_TCR4_FRSZ(1)           | /* Two words in each frame          */
				I2S_TCR4_SYWD(16/* runtime->sample_bits*/ -1 ) | /* Synch width as long as data word */
				I2S_TCR4_MF_MASK           | /* MSB shifted out first            */
				I2S_TCR4_FSE_MASK            /* Frame sync one bit early         */
		);
		/* 
		** Fist bit shifted is always MSB (bit with highest index in register)
		** and word length is set according to user configuration.
		*/
		i2s_ptr->TCR5 = 0;
		i2s_ptr->TCR5 = (
			I2S_TCR5_WNW(/* runtime->sample_bits*/16 -1) | /* Word length for Nth word is data_bits   */
			I2S_TCR5_W0W(/* runtime->sample_bits*/16 -1) | /* Word length for 0th word is data_bits   */
			I2S_TCR5_FBT(/* runtime->sample_bits*/16 -1)     /*  First bit shifted is on data_bits index */
		);
		/*
		** Reset word mask 
		*/
		i2s_ptr->TMR = 0;
	}
	else {
		i2s_ptr->RCSR = 0;
		/* 
		** Software reset -> reset internal receiver logic including the FIFO writer
		*/
		i2s_ptr->RCSR |= I2S_RCSR_SR_MASK;
		i2s_ptr->RCSR &= ~(I2S_RCSR_SR_MASK);
		/*
		** Enable receiver in debug mode - this must be done else synchronous mode
		** will not work while debugging.
		*/
		i2s_ptr->RCSR |= I2S_RCSR_DBGE_MASK;
		/* 
		** Set FIFO watermark
		*/
		i2s_ptr->RCR1 = I2S_RCR1_RFW(I2S_RX_FIFO_DEFAULT_WATERMARK);
		/* 
		** Set the clock polarity and master clock source 
		*/
		i2s_ptr->RCR2 = 0;
#if 0		
		i2s_ptr->RCR2 |= (
			I2S_RCR2_BCP_MASK |                          /* Bit clock active low           */
			I2S_RCR2_MSEL(1)  |                          /* MCLK from divider (CLKGEN)     */
			I2S_RCR2_DIV((/*DEFAULT_BCLK_DIV*/8 / 2) - 1)     /* Bit clock frequency = MCLK / 8 */
			);
#endif
		/*
		** First word in frame sets start of word flag
		*/
		i2s_ptr->RCR3 |= (I2S_RCR3_WDFL(0));
		/* 
		** Enable selected receiver channel
		*/
		// RX_CHANNEL =0 
		i2s_ptr->RCR3 |= (I2S_RCR3_RCE(1));

		/*
		** Configure frame size, sync width, MSB and frame sync
		*/
		i2s_ptr->RCR4 = 0;
		i2s_ptr->RCR4 = (
			I2S_RCR4_FRSZ(1)           | /* Two words in each frame          */
			I2S_RCR4_SYWD(/* pcm_format_width(runtime->format)*/16 - 1) | /* Synch width as long as data word */
			I2S_RCR4_MF_MASK           | /* MSB shifted out first            */
			I2S_RCR4_FSE_MASK            /* Frame sync one bit early         */
			);
		/* 
		** Fist bit shifted is always MSB (bit with highest index in register)
		** and word length is set according to user configuration.
		*/
		i2s_ptr->RCR5 = 0;
		i2s_ptr->RCR5 = (
			I2S_RCR5_WNW(/* pcm_format_width(runtime->format)*/16 - 1) | /* Word length for Nth word is data_bits   */
			I2S_RCR5_W0W(/* pcm_format_width(runtime->format)*/16 - 1) | /* Word length for 0th word is data_bits   */
			I2S_RCR5_FBT(/* pcm_format_width(runtime->format)*/16 - 1)     /* First bit shifted is on data_bits index */
			);
		/*
		** Reset word mask 
		*/
		i2s_ptr->RMR = 0;
	}

	/* I2S_TX_ASYNCHRONOUS =>  Transmitter is asynchronous 
	i2s_ptr->TCR2 &= ~(I2S_TCR2_SYNC_MASK);
	i2s_ptr->TCR2 &= ~(I2S_TCR2_BCS_MASK); */
 
	return 0;
}


static uint_32 ssi_set_master_mode_fmt (
	struct snd_pcm_runtime *runtime, unsigned int fmt
)
{
	struct sco_pcm_runtime_data *iprtd = (struct sco_pcm_runtime_data *)runtime->private_data;
	struct soc_dma_data * dma_data = (struct soc_dma_data *)iprtd->dma_data;
	I2S_MemMapPtr i2s_ptr = dma_data->ssi_base;

	boolean set_tx_as_master = (fmt & I2S_TX_MASTER) && (runtime->stream == SNDRV_PCM_STREAM_PLAYBACK);
	boolean set_rx_as_master = (fmt & I2S_RX_MASTER) && (runtime->stream == SNDRV_PCM_STREAM_CAPTURE);
	/* 
	** If the master clock source is internal, enable it and recalculate
	** values of frequencies in the internal info structure.
	*/
	if (fmt & SSI_FMT_CLK_INT) {

	}
	/*
	** Else disable master clock signal and set pin as input. Also reset values
	** of frequencies in the info structure.
	*/
	else if (fmt & SSI_FMT_CLK_EXT) {
		i2s_ptr->MCR &= ~(I2S_MCR_MOE_MASK);
	}
	/*
	** In case we want to set the transmitter and it is enabled, device is busy.
	*//*
	if ((i2s_ptr->TCSR & I2S_TCSR_TE_MASK) && (set_tx_as_master)) 
		return -1; */
	if ((fmt & SSI_FMT_TX_ASYNCHRONOUS) && (set_tx_as_master)) {
		/* Set Tx into master mode */
		/* |= SSI_FMT_TX_MASTER; */
		i2s_ptr->TCR2 |= (I2S_TCR2_BCD_MASK);
		i2s_ptr->TCR4 |= (I2S_TCR4_FSD_MASK);
		i2s_ptr->TCR4 |= (I2S_TCR4_FSP_MASK);
		/* 
		** If the the receiver is synchronous settings must same as the
		** transmitter settings.
		*/
		if (fmt  & SSI_FMT_RX_SYNCHRONOUS) {
		/* |= SSI_FMT_RX_MASTER; */
		i2s_ptr->RCR2 |= (I2S_RCR2_BCD_MASK);
		i2s_ptr->RCR4 |= (I2S_RCR4_FSD_MASK);
		}
		/* 
		** If transmitter have swapped bit clock. set receiver bit clock to be
		** generated internally
		*/
		if (fmt & SSI_FMT_TX_BCLK_SWAPPED) {
			i2s_ptr->RCR2 |= (I2S_RCR2_BCD_MASK);
		}
	}

	/*
	** In case we want to the set receiver and it is enabled, device is busy.
	*/ /*
	if ((i2s_ptr->RCSR & I2S_RCSR_RE_MASK) && (set_rx_as_master))
		return -1; */
	if ((fmt & SSI_FMT_RX_ASYNCHRONOUS) && (set_rx_as_master)) {
		/* Set Rx in master mode */
		/* |= SSI_FMT_RX_MASTER; */
		i2s_ptr->RCR2 |= (I2S_RCR2_BCD_MASK);
		i2s_ptr->RCR4 |= (I2S_RCR4_FSD_MASK);
		/* 
		** If the the transmitter is synchronous settings must same as the
		** receiver settings.
		*/
		if (fmt & SSI_FMT_TX_SYNCHRONOUS) {
			/* |= SSI_FMT_TX_MASTER; */
			i2s_ptr->TCR2 |= (I2S_TCR2_BCD_MASK);
			i2s_ptr->TCR4 |= (I2S_TCR4_FSD_MASK);
		}
		/* 
		** If receiver have swapped bit clock. Set transmitter bit clock to be
		** generated internally.
		*/
		if (fmt & SSI_FMT_RX_BCLK_SWAPPED) {
			i2s_ptr->TCR2 |= (I2S_TCR2_BCD_MASK);
		}
	}
	
	return 0;
}

static uint_32 ssi_set_slave_mode_fmt (
    struct snd_pcm_runtime *runtime, unsigned int fmt
)
{
	struct sco_pcm_runtime_data *iprtd = (struct sco_pcm_runtime_data *)runtime->private_data;
	struct soc_dma_data * dma_data = (struct soc_dma_data *)iprtd->dma_data;
	I2S_MemMapPtr i2s_ptr = dma_data->ssi_base;
	
	boolean set_tx_as_slave = (fmt & SSI_FMT_TX_SLAVE) &&  (runtime->stream == SNDRV_PCM_STREAM_PLAYBACK);
	boolean set_rx_as_slave = (fmt & SSI_FMT_RX_SLAVE) &&  (runtime->stream == SNDRV_PCM_STREAM_CAPTURE);
	/*
	** Disable the master clock signal and set pin as input. Also reset values
	** of frequencies in the info structure.
	*/
	i2s_ptr->MCR &= ~(I2S_MCR_MOE_MASK);
	/*
	** In case we want to setup the transmitter and it is enabled, device is busy.
	*/ /*
	if ((i2s_ptr->TCSR & I2S_TCSR_TE_MASK) && (set_tx_as_slave))
		return -1; */
	if ((fmt & SSI_FMT_TX_ASYNCHRONOUS) && (set_tx_as_slave)) {
		/* Set Tx into slave mode */
		/* |= SSI_FMT_TX_SLAVE; */
		i2s_ptr->TCR2 &= ~(I2S_TCR2_BCD_MASK);
		i2s_ptr->TCR4 &= ~(I2S_TCR4_FSD_MASK);
		/* 
		** If the the receiver is synchronous settings must same as the
		** transmitter settings.
		*/
		if (fmt & SSI_FMT_RX_SYNCHRONOUS) {
			/* |= SSI_FMT_RX_SLAVE; */
			i2s_ptr->RCR2 &= ~(I2S_RCR2_BCD_MASK);
			i2s_ptr->RCR4 &= ~(I2S_RCR4_FSD_MASK);
		}
		/* 
		** If transmitter have swapped bit clock set receiver bit clock to be
		** generated externally
		*/
		if (fmt & SSI_FMT_TX_BCLK_SWAPPED) {
			i2s_ptr->RCR2 &= ~(I2S_RCR2_BCD_MASK);
		}

		if (fmt & SSI_FMT_BCP_INV) {
			i2s_ptr->TCR2 |= (I2S_TCR2_BCP_MASK);
		}
		if (fmt & SSI_FMT_FSP_INV) {
			i2s_ptr->TCR4 |= (I2S_TCR4_FSP_MASK);
		}
		
	}

	/*
	** In case we want to the setup receiver and it is enabled, device is busy.
	*/ /*
	if ((i2s_ptr->RCSR & I2S_RCSR_RE_MASK) && (set_rx_as_slave))
		return -1; */

	if ((fmt & SSI_FMT_RX_ASYNCHRONOUS) && (set_rx_as_slave)) {
		/* Set Rx in master mode */
		/* |= SSI_FMT_RX_SLAVE; */
		i2s_ptr->RCR2 &= ~(I2S_RCR2_BCD_MASK);
		i2s_ptr->RCR4 &= ~(I2S_RCR4_FSD_MASK);
		/* 
		** If the the transmitter is synchronous settings must same as the
		** receiver settings.
		*/
		if (fmt & SSI_FMT_TX_SYNCHRONOUS) {
			/* |= I2S_TX_SLAVE; */
			i2s_ptr->TCR2 &= ~(I2S_TCR2_BCD_MASK);
			//i2s_ptr->TCR2 |= I2S_TCR2_BCI_MASK;
			i2s_ptr->TCR4 &= ~(I2S_TCR4_FSD_MASK);
		}
		/* 
		** If receiver have swapped bit clock. Set transmitter bit clock to be
		** generated externally.
		*/
		if (fmt & SSI_FMT_RX_BCLK_SWAPPED) {
			i2s_ptr->TCR2 &= ~(I2S_TCR2_BCD_MASK);
		}

		if (fmt & SSI_FMT_BCP_INV) {
			i2s_ptr->RCR2 |= (I2S_RCR2_BCP_MASK);
		}
		if (fmt & SSI_FMT_FSP_INV) {
			i2s_ptr->RCR4 |= (I2S_RCR4_FSP_MASK);
		}

	}
	return 0;
}

static uint_32 ssi_set_bclk_mode (
    struct snd_pcm_runtime *runtime, unsigned int fmt
)
{
	struct sco_pcm_runtime_data *iprtd = (struct sco_pcm_runtime_data *)runtime->private_data;
	struct soc_dma_data * dma_data = (struct soc_dma_data *)iprtd->dma_data;
	I2S_MemMapPtr i2s_ptr = dma_data->ssi_base;
	
	if ((fmt & SSI_FMT_TX_SYNCHRONOUS) && (fmt & SSI_FMT_RX_SYNCHRONOUS))
		/* Invalid combination of bclk modes */
		return -1;
	/*
	** Transmitter clock:
	*/
	if (fmt & SSI_FMT_TX_ASYNCHRONOUS) { // Transmitter is asynchronous
		i2s_ptr->TCR2 &= ~(I2S_TCR2_SYNC_MASK);
	}
	else if (fmt & SSI_FMT_TX_SYNCHRONOUS) {// Transmitter is synchronous
		/*
		** If transmitter is synchronous, receiver must be asynchronous
		*/
		if (i2s_ptr->RCR2 & I2S_RCR2_SYNC_MASK) {
			/* Invalid combination of bclk modes */
			return -1; 
		}
		i2s_ptr->TCR2 |= (I2S_TCR2_SYNC(1));
	}

	if (fmt & SSI_FMT_TX_BCLK_NORMAL) { // Transmitter BCLK not swapped
		i2s_ptr->TCR2 &= ~(I2S_TCR2_BCS_MASK);
	}
	else if (fmt & SSI_FMT_TX_BCLK_SWAPPED) { // Transmitter BCLK swapped
		/*
		** TX_BCLK = SAI_RX_BCLK
		** TX_FS = SAI_TX_SYNC
		*/
		i2s_ptr->TCR2 |= (I2S_TCR2_BCS_MASK);
		/*
		** When Tx is synch. BCS bit must be set also for Rx.
		*/
		if (fmt & SSI_FMT_TX_SYNCHRONOUS) {
			/*
			** TX_BCLK = SAI_TX_BCLK
			** TX_FS = SAI_RX_SYNC
			*/
			i2s_ptr->RCR2 |= (I2S_RCR2_BCS_MASK);
		}
	}

	/*
	** Receiver clock:
	*/
	if (fmt & SSI_FMT_RX_ASYNCHRONOUS) { // Receiver is asynchronous
		i2s_ptr->RCR2 &= ~(I2S_RCR2_SYNC_MASK);
	}
	else if (fmt & SSI_FMT_RX_SYNCHRONOUS) { // Receiver is synchronous
		/*
		** If receiver is synchronous, transmitter must be asynchronous
		*/
		if (i2s_ptr->TCR2 & I2S_TCR2_SYNC_MASK) {
			/* Invalid combination of bclk modes */
			return -1; 
		}
		i2s_ptr->RCR2 |= (I2S_RCR2_SYNC(1));
	}

	if (fmt & SSI_FMT_RX_BCLK_NORMAL) {// Receiver BCLK not swapped
		i2s_ptr->RCR2 &= ~(I2S_RCR2_BCS_MASK);
	}
	else if (fmt & SSI_FMT_RX_BCLK_SWAPPED) {// Receiver BCLK swapped
		/*
		** RX_BCLK = SAI_TX_BCLK
		** RX_FS = SAI_RX_SYNC
		*/
		i2s_ptr->RCR2 |= (I2S_RCR2_BCS_MASK);
		/*
		** When Rx is synch. BCS bit must be set also for Tx.
		*/
		if (fmt & SSI_FMT_RX_SYNCHRONOUS) {
			/*
			** RX_BCLK = SAI_RX_BCLK
			** RX_FS = SAI_TX_SYNC
			*/
			i2s_ptr->TCR2 |= (I2S_TCR2_BCS_MASK);
		}
	}
	return 0;
}

/*
 * Set cpu(ssi) format
 * Should only be called when port is inactive.
 */
static int cpu_set_interface_fmt(struct snd_pcm_runtime *runtime, unsigned int fmt)
{
	struct sco_pcm_runtime_data *iprtd = (struct sco_pcm_runtime_data *)runtime->private_data;
	struct soc_dma_data * dma_data = (struct soc_dma_data *)iprtd->dma_data;
	I2S_MemMapPtr i2s_ptr = dma_data->ssi_base;
	
	uint32_t ret = 0;
	uint32_t ssi_fmt = 0;

	/* dai_format = SND_SOC_DAIFMT_I2S | SND_SOC_DAIFMT_NB_NF |
		SND_SOC_DAIFMT_CBM_CFM;  this setting be used for codec master */
	/* DAI mode */
	switch (fmt & SND_SOC_INTFFMT_FORMAT_MASK) {
	case SND_SOC_INTFFMT_I2S: /* use the sync mode (SSI_FMT_TX_ASYNCHRONOUS,SSI_FMT_RX_SYNCHRONOUS)
	                                              (tx,rx use the same bclk,fs) */
		ssi_fmt |= SSI_FMT_TX_ASYNCHRONOUS | SSI_FMT_RX_SYNCHRONOUS;
		ssi_fmt |= SSI_FMT_TX_BCLK_NORMAL | SSI_FMT_RX_BCLK_NORMAL;										  
		break;
	case SND_SOC_INTFFMT_LEFT_J:
		/* data on rising edge of bclk, frame high with data */
	case SND_SOC_INTFFMT_DSP_B:
		/* data on rising edge of bclk, frame high with data */
	case SND_SOC_INTFFMT_DSP_A:
		/* data on rising edge of bclk, frame high 1clk before data */
		return -1;  /*  don't support */
	}
    
	/* DAI clock inversion */
	switch (fmt & SND_SOC_INTFFMT_INV_MASK) {
	case SND_SOC_INTFFMT_IB_IF:
		ssi_fmt |= (SSI_FMT_BCP_INV | SSI_FMT_FSP_INV);
		break;
	case SND_SOC_INTFFMT_IB_NF:
		ssi_fmt |= (SSI_FMT_BCP_INV);
		break;
	case SND_SOC_INTFFMT_NB_IF:
		ssi_fmt |= (SSI_FMT_FSP_INV);
		break;
	case SND_SOC_INTFFMT_NB_NF:
		break;
	}

	/* DAI clock master masks */
	switch (fmt & SND_SOC_INTFFMT_MASTER_MASK) {
	case SND_SOC_INTFFMT_CBS_CFS:  /* set ssi to master */
		ssi_fmt |=  (SSI_FMT_TX_MASTER | SSI_FMT_RX_MASTER);
		ssi_set_master_mode_fmt(runtime,ssi_fmt);
		break;
	case SND_SOC_INTFFMT_CBM_CFM:   /* set ssi to slave ,codec to master */
		ssi_fmt |=  (SSI_FMT_TX_SLAVE | SSI_FMT_RX_SLAVE);
		ssi_set_slave_mode_fmt(runtime,ssi_fmt);
		break;
	default:
		return -1;
	}

	ret = ssi_set_bclk_mode(runtime,ssi_fmt);

	return ret;
}

/*  set codec(sgtl5000) format */
static int codec_set_interface_fmt(struct snd_pcm_runtime *runtime, unsigned int fmt)
{

	uint16_t i2sctl = 0;

	//sgtl5000->master = 0;

	switch (fmt & SND_SOC_INTFFMT_MASTER_MASK) {
	case SND_SOC_INTFFMT_CBS_CFS:
		break;
	case SND_SOC_INTFFMT_CBM_CFM:
		i2sctl |= SGTL5000_I2S_MASTER;   /* use this */
		// sgtl5000->master = 1;
		break;
	default:
		return -1;
	}

	/* setting i2s data format */
	switch (fmt & SND_SOC_INTFFMT_FORMAT_MASK) {
	case SND_SOC_INTFFMT_DSP_A:
		i2sctl |= SGTL5000_I2S_MODE_PCM;
		break;
	case SND_SOC_INTFFMT_DSP_B:
		i2sctl |= SGTL5000_I2S_MODE_PCM;
		i2sctl |= SGTL5000_I2S_LRALIGN;
		break;
	case SND_SOC_INTFFMT_I2S:
		i2sctl |= SGTL5000_I2S_MODE_I2S_LJ;  /* use this */
		break;
	case SND_SOC_INTFFMT_RIGHT_J:
		i2sctl |= SGTL5000_I2S_MODE_RJ;
		i2sctl |= SGTL5000_I2S_LRPOL;
		break;
	case SND_SOC_INTFFMT_LEFT_J:
		i2sctl |= SGTL5000_I2S_MODE_I2S_LJ;
		i2sctl |= SGTL5000_I2S_LRALIGN;
		break;
	default:
		return -1;
	}

	// sgtl5000->fmt = fmt & SND_SOC_INTFFMT_FORMAT_MASK;

	/* Clock inversion */
	switch (fmt & SND_SOC_INTFFMT_INV_MASK) {
	case SND_SOC_INTFFMT_NB_NF:
		break;
	case SND_SOC_INTFFMT_IB_NF:
	case SND_SOC_INTFFMT_IB_IF:	
		i2sctl |= SGTL5000_I2S_SCLK_INV;
		break;
	default:
		return -1;
	}

	sgtl_WriteReg(SGTL5000_CHIP_I2S_CTRL, i2sctl);

	return 0;
}

/* codec and ssi preinit set params  */
static int preinit_params(struct snd_pcm_runtime *runtime,
	struct snd_pcm_params *params)
{
	uint32_t intf_format;
	int ret;
	unsigned int channels = params->channels;

	/* set SYSCLK, LRCLK (open and init clock)*/

	intf_format = SND_SOC_INTFFMT_I2S | SND_SOC_INTFFMT_NB_NF |
		SND_SOC_INTFFMT_CBM_CFM;

	/* set codec interface configuration */
	ret = codec_set_interface_fmt(runtime, intf_format);
	if (ret < 0)
		return ret;

	/* set cpu(ssi) interface configuration */
	intf_format = SND_SOC_INTFFMT_I2S | /*  SND_SOC_INTFFMT_NB_NF */ SND_SOC_INTFFMT_IB_IF  |
		SND_SOC_INTFFMT_CBM_CFM;
	ret = cpu_set_interface_fmt(runtime, intf_format);
	if (ret < 0)
		return ret;

	return 0;
}

static int ldo_regulator_enable(int vol)          /* vddd alway use internal ldo */
{
	uint16_t  reg;
	int voltage;
	voltage = vol;

	/* set regulator value firstly */  /* ldo->voltage =1200mv */
	reg = (1600 - voltage / 1000) / 50;
	reg = clamp(reg, 0x0, 0xf);

	/* amend the voltage value, unit: uV */
	voltage = (1600 - reg * 50) * 1000;

	/* set voltage to register */
	sgtl_WriteReg(SGTL5000_CHIP_LINREG_CTRL, reg);

	sgtl_ReadReg(SGTL5000_CHIP_ANA_POWER, &reg);
	reg |= SGTL5000_LINEREG_D_POWERUP;
	sgtl_WriteReg(SGTL5000_CHIP_ANA_POWER, reg);

	reg &= ~SGTL5000_LINREG_SIMPLE_POWERUP;
	/* when internal ldo enabled, simple digital power can be disabled */
	sgtl_WriteReg(SGTL5000_CHIP_ANA_POWER, reg);

	os_msleep(1);

	return 0;
}

static int sgtl5000_set_clock(int frame_rate)
{
	int clk_ctl = 0;
	int sys_fs;	/* sample freq */
	
	/*
	 * sample freq should be divided by frame clock,
	 * if frame clock lower than 44.1khz, sample feq should set to
	 * 32khz or 44.1khz.
	 */
	switch (frame_rate) {
	case 8000:
	case 16000:
		sys_fs = 32000;
		break;
	case 11025:
	case 22050:
		sys_fs = 44100;
		break;
	default:
		sys_fs = frame_rate;
		break;
	}

	/* set divided factor of frame clock */
	switch (sys_fs / frame_rate) {
	case 4:
		clk_ctl |= SGTL5000_RATE_MODE_DIV_4 << SGTL5000_RATE_MODE_SHIFT;
		break;
	case 2:
		clk_ctl |= SGTL5000_RATE_MODE_DIV_2 << SGTL5000_RATE_MODE_SHIFT;
		break;
	case 1:
		clk_ctl |= SGTL5000_RATE_MODE_DIV_1 << SGTL5000_RATE_MODE_SHIFT;
		break;
	default:
		return -1;
	}

	/* set the sys_fs according to frame rate */
	switch (sys_fs) {
	case 32000:
		clk_ctl |= SGTL5000_SYS_FS_32k << SGTL5000_SYS_FS_SHIFT;
		break;
	case 44100:
		clk_ctl |= SGTL5000_SYS_FS_44_1k << SGTL5000_SYS_FS_SHIFT;
		break;
	case 48000:
		clk_ctl |= SGTL5000_SYS_FS_48k << SGTL5000_SYS_FS_SHIFT;
		break;
	case 96000:
		clk_ctl |= SGTL5000_SYS_FS_96k << SGTL5000_SYS_FS_SHIFT;
		break;
	default:
		printf("frame rate %d not supported\n",frame_rate);
		return -1;
	}

	/*
	 * calculate the divider of mclk/sample_freq,
	 * factor of freq =96k can only be 256, since mclk in range (12m,27m)
	 */
	switch (SGTL5000_MCLK / sys_fs) {
	case 256:   /* 96K */
		clk_ctl |= SGTL5000_MCLK_FREQ_256FS <<
			SGTL5000_MCLK_FREQ_SHIFT;
		break;
	case 384:  /* 64K */
		clk_ctl |= SGTL5000_MCLK_FREQ_384FS <<
			SGTL5000_MCLK_FREQ_SHIFT;
		break;
	case 512:  /* 48K */
		clk_ctl |= SGTL5000_MCLK_FREQ_512FS <<
			SGTL5000_MCLK_FREQ_SHIFT;
		break;
	default:
		/* if mclk not satisify the divider, use pll */
			clk_ctl |= SGTL5000_MCLK_FREQ_PLL <<
				SGTL5000_MCLK_FREQ_SHIFT;
	}

	/* if using pll, please check manual 6.4.2 for detail */
	if ((clk_ctl & SGTL5000_MCLK_FREQ_MASK) == SGTL5000_MCLK_FREQ_PLL) {
		/*u64*/uint32_t out, t;
		int div2;
		int pll_ctl;
		unsigned int in, int_div, frac_div;

		if (SGTL5000_MCLK > 17000000) {
			div2 = 1;
			in = SGTL5000_MCLK / 2;    /* 12288000 */
		} else {
			div2 = 0;
			in = SGTL5000_MCLK;
		}
		if (sys_fs == 44100)
			out = 180633600;
		else {
			out = 196608000;
		}
		 
             t = out % in ;
		out = out / in;
			 
		int_div = out;
#if 0 /* don't support 64bit div */		
		t *= 2048;
		t = t / in;
		frac_div = t;
#else
		if (sys_fs == 44100)
			t =  1433 + 1 ;
		else
			t =  0; 
		frac_div = t;
#endif
		pll_ctl = int_div << SGTL5000_PLL_INT_DIV_SHIFT |
		    frac_div << SGTL5000_PLL_FRAC_DIV_SHIFT;

		sgtl_WriteReg(SGTL5000_CHIP_PLL_CTRL, pll_ctl);
		if (div2)
			sgtl_ModifyReg(SGTL5000_CHIP_CLK_TOP_CTRL, ~SGTL5000_INPUT_FREQ_DIV2, SGTL5000_INPUT_FREQ_DIV2);
		else
			sgtl_ModifyReg(SGTL5000_CHIP_CLK_TOP_CTRL, ~SGTL5000_INPUT_FREQ_DIV2, 0);

		/* power up pll */
		sgtl_ModifyReg(SGTL5000_CHIP_ANA_POWER, ~(SGTL5000_PLL_POWERUP | SGTL5000_VCOAMP_POWERUP), 
			(SGTL5000_PLL_POWERUP | SGTL5000_VCOAMP_POWERUP));
	} else {
		/* power down pll */
		sgtl_ModifyReg(SGTL5000_CHIP_ANA_POWER, ~(SGTL5000_PLL_POWERUP | SGTL5000_VCOAMP_POWERUP), 
				(SGTL5000_PLL_POWERUP | SGTL5000_VCOAMP_POWERUP));
	}

	/* if using pll, clk_ctrl must be set after pll power up */
	sgtl_WriteReg(SGTL5000_CHIP_CLK_CTRL, clk_ctl);

	return 0;

}

/*
 * Set codec(sgtl5000) PCM interface bit size and sample rate.
 * input: params_rate, params_fmt
 */
static int codec_pcm_interface_params(struct snd_pcm_runtime *runtime,
				  struct snd_pcm_params *params)
{
	int channels = params->channels;
	int i2s_ctl = 0;
	int stereo;
	int ret;

	if (runtime->stream == SNDRV_PCM_STREAM_PLAYBACK)
		stereo = SGTL5000_DAC_STEREO;
	else
		stereo = SGTL5000_ADC_STEREO;

	/* set mono to save power */
	sgtl_ModifyReg(SGTL5000_CHIP_ANA_POWER, ~stereo,
			channels == 1 ? 0 : stereo);

	/* set codec clock base on lrclk */
	ret = sgtl5000_set_clock(params->rate);
	if (ret)
		return ret;

	/* set i2s data format */
	switch (params->format) {
	case SNDRV_PCM_FORMAT_S16_LE:
		/* if (sgtl5000->fmt == SND_SOC_INTFFMT_RIGHT_J)
			return -1; */
		i2s_ctl |= SGTL5000_I2S_DLEN_16 << SGTL5000_I2S_DLEN_SHIFT;
		i2s_ctl |= SGTL5000_I2S_SCLKFREQ_32FS <<
		    SGTL5000_I2S_SCLKFREQ_SHIFT;
		break;
	case SNDRV_PCM_FORMAT_S20_3LE:
		i2s_ctl |= SGTL5000_I2S_DLEN_20 << SGTL5000_I2S_DLEN_SHIFT;
		i2s_ctl |= SGTL5000_I2S_SCLKFREQ_64FS <<
		    SGTL5000_I2S_SCLKFREQ_SHIFT;
		break;
	case SNDRV_PCM_FORMAT_S24_LE:
	case SNDRV_PCM_FORMAT_S24_3LE:	
		i2s_ctl |= SGTL5000_I2S_DLEN_24 << SGTL5000_I2S_DLEN_SHIFT;
		i2s_ctl |= SGTL5000_I2S_SCLKFREQ_64FS <<
		    SGTL5000_I2S_SCLKFREQ_SHIFT;
		break;
	case SNDRV_PCM_FORMAT_S32_LE:
		/* if (sgtl5000->fmt == SND_SOC_INTFFMT_RIGHT_J)
			return -1; */
		i2s_ctl |= SGTL5000_I2S_DLEN_32 << SGTL5000_I2S_DLEN_SHIFT;
		i2s_ctl |= SGTL5000_I2S_SCLKFREQ_64FS <<
		    SGTL5000_I2S_SCLKFREQ_SHIFT;
		break;
	default:
		return -1;
	}

	sgtl_ModifyReg(SGTL5000_CHIP_I2S_CTRL,
			~(SGTL5000_I2S_DLEN_MASK | SGTL5000_I2S_SCLKFREQ_MASK),
			i2s_ctl);

	return 0;
}

static int cpu_interface_params(struct snd_pcm_runtime *runtime,
			     struct snd_pcm_params *params)
{
	struct sco_pcm_runtime_data *iprtd = (struct sco_pcm_runtime_data *)runtime->private_data;
	struct soc_dma_data * dma_data = (struct soc_dma_data *)iprtd->dma_data;
	I2S_MemMapPtr i2s_ptr = dma_data->ssi_base;
	int data_bits = 0;
	unsigned int channels = params->channels;

	/* Tx/Rx config */
	if (runtime->stream == SNDRV_PCM_STREAM_PLAYBACK /* || (fmt & SSI_FMT_RX_SYNCHRONOUS) */) {
		i2s_ptr->TCR4 &= ~(I2S_TCR4_FRSZ_MASK);
		i2s_ptr->TCR4 |= (I2S_TCR4_FRSZ(1));
		/* Mask second transmitter channel if there is only one data channel */
		if (channels == 1)
			i2s_ptr->TMR = 0x02;
		else
			i2s_ptr->TMR = 0x00;
	} else { /*  || (fmt & SSI_FMT_TX_SYNCHRONOUS)*/
		i2s_ptr->RCR4 &= ~(I2S_RCR4_FRSZ_MASK);
		i2s_ptr->RCR4 |= (I2S_RCR4_FRSZ(1));
		/* Mask second receiver channel if there is only one data channel */
		if (channels == 1)
			i2s_ptr->RMR = 0x02;
		else
			i2s_ptr->RMR = 0x00;
	}
	
	/* interface data (word) size */
#if 0	
	switch (params->format) {
	case SNDRV_PCM_FORMAT_S16_LE:
		data_bits = 16;
		break;
	case SNDRV_PCM_FORMAT_S24_3LE:
		data_bits = 24;
		break;
	case SNDRV_PCM_FORMAT_S24_LE:
	case SNDRV_PCM_FORMAT_S32_LE:
		data_bits = 32;
		break;
	default:
		return -1;
	}
#endif

	data_bits = pcm_format_physical_width(params->format);
	// fix for sgtl5000 master 24 bit 
	if(data_bits == 24)
		data_bits = 32;

	if (runtime->stream == SNDRV_PCM_STREAM_PLAYBACK /* || (fmt & SSI_FMT_RX_SYNCHRONOUS) */) {
		/* Set 0th word length */
		i2s_ptr->TCR5 &= ~(I2S_TCR5_W0W_MASK);
		i2s_ptr->TCR5 |= I2S_TCR5_W0W(data_bits -1);
		/* Set Nth word length */
		i2s_ptr->TCR5 &= ~(I2S_TCR5_WNW_MASK);
		i2s_ptr->TCR5 |= I2S_TCR5_WNW(data_bits -1);
		/* Set first bit shifted to highest index in register */
		i2s_ptr->TCR5 &= ~(I2S_TCR5_FBT_MASK);
		i2s_ptr->TCR5 |= I2S_TCR5_FBT(data_bits - 1);
		/* Set sync width to match word length */
		i2s_ptr->TCR4 &= ~(I2S_TCR4_SYWD_MASK);
		i2s_ptr->TCR4 |= I2S_TCR4_SYWD(data_bits -1);
	}
	else {
		/* Set 0th word length */
		i2s_ptr->RCR5 &= ~(I2S_RCR5_W0W_MASK);
		i2s_ptr->RCR5 |= I2S_RCR5_W0W(data_bits-1);
		/* Set Nth word length */
		i2s_ptr->RCR5 &= ~(I2S_RCR5_WNW_MASK);
		i2s_ptr->RCR5 |= I2S_RCR5_WNW(data_bits-1);
		/* Set first bit shifted to highest index in register */
		i2s_ptr->RCR5 &= ~(I2S_RCR5_FBT_MASK);
		i2s_ptr->RCR5 |= I2S_RCR5_FBT(data_bits-1);
		/* Set sync width to match word length */
		i2s_ptr->RCR4 &= ~(I2S_RCR4_SYWD_MASK);
		i2s_ptr->RCR4 |= I2S_RCR4_SYWD(data_bits-1);
	}
	
	return 0;
}

static int dma_pcm_params(struct snd_pcm_runtime *runtime,
				struct snd_pcm_params *params)
{
	int ret;
	unsigned int bits;
	snd_pcm_uframes_t frames;
	unsigned int buffer_bytes_adjusted = 0;
	unsigned int buffer_bytes = 0;

	struct sco_pcm_runtime_data *iprtd = (struct sco_pcm_runtime_data *) runtime->private_data;
	struct soc_dma_data * dma_data = (struct soc_dma_data *)iprtd->dma_data;
	//MQX_EDMA_SCATTER_STRUCT_PTR	scatter_list;  /* malloc at init ,free at sco_pcm_close */

	if(!sys_dma_inited) {
		sys_dma_inited = 1;
		printf("dma inited\n");
		dma_init();   /* don't deinit */
	}
	
	/* Default  params */
	dma_data->fifo_aligned_bits = FIFO_BITS_ALIGNED;
	dma_data->fifo_align_type = FIFO_BITS_ENDTYPES;
	runtime->sample_aligned_bits = dma_data->fifo_aligned_bits;  /* usb by pcm.c */
	
	if (params->periods != 0) {  /* params periods high priority */
		runtime->buffer_size = runtime->period_size * params->periods;    /* frame unit */
		runtime->periods = params->periods;
		buffer_bytes = frames_to_bytes(runtime,runtime->buffer_size);

	}
	else if(params->buffer_bytes != 0) {
		runtime->buffer_size = bytes_to_frames(runtime,params->buffer_bytes);
		runtime->periods =  runtime->buffer_size /runtime->period_size;
		buffer_bytes = params->buffer_bytes;
	}
	else {
		pr_err("err: both params periods and buffer_bytes all zero!\n");
		return -1;
	}

	buffer_bytes_adjusted = bytes_fifo_aligned(buffer_bytes,
			                                 dma_data->fifo_aligned_bits,/* runtime->sample_bits*/runtime->sample_phy_bits);

	/* runtime->avail_min = runtime->period_size; */

	if ((runtime->periods > MAX_PERIODS) || (runtime->periods < MIN_PERIODS)) {
		pr_err("err: periods number\n");
		return -1;
	}
	
	runtime->boundary = runtime->buffer_size;
	while (runtime->boundary * 2 <= LONG_MAX - runtime->buffer_size)
		runtime->boundary *= 2;

	if (params->start_threshold == THRESHOLD_BOUNDARY)
		runtime->start_threshold = runtime->boundary;
	else	
		runtime->start_threshold = params->start_threshold;
            /* runtime->periods * runtime->period_size / 2; */       /* normally ,start at the half of the buffer */

	if (params->stop_threshold == THRESHOLD_BOUNDARY)
		runtime->stop_threshold = runtime->boundary;
	else
		runtime->stop_threshold =  params->stop_threshold;
		/* runtime->periods *  runtime->period_size; */

	if (params->stop_threshold == THRESHOLD_BOUNDARY)
		runtime->silence_threshold = runtime->boundary;
	else
		runtime->silence_threshold = params->silence_threshold;
	runtime->silence_size = params->silence_size;  /* runtime->period_size */

	runtime->dma_area = PCM_mem_alloc_zero(buffer_bytes_adjusted); //error in K70 ->  PCM_mem_alloc_uncached_align(buffer_bytes_adjusted, 4);
	printf("dma area at 0x%X\n",runtime->dma_area);
	if(!runtime->dma_area) {
		printf("alloc dma buffer failed\n");
		return -1;
	}
	pr_debug("dma buffer size : snd need = %d,soc allocate %d\n",buffer_bytes,buffer_bytes_adjusted);

	{
		iprtd->size = frames_to_bytes(runtime, runtime->buffer_size);
		iprtd->periods = runtime->periods;
		iprtd->period_bytes = frames_to_bytes(runtime,runtime->period_size);
		iprtd->offset = 0;
		iprtd->period_time = (HZ * runtime->period_size + (runtime->rate -1)) /runtime->rate;

		iprtd->buf = (unsigned int *)runtime->dma_area;
	}

	/* edma request */
	{
		I2S_MemMapPtr i2s_ptr = dma_data->ssi_base;

		MQX_EDMA_SCATTER_STRUCT_PTR p_list;
		scatter_list = PCM_mem_alloc(sizeof(MQX_EDMA_SCATTER_STRUCT) * iprtd->periods);
		dma_data->desc = scatter_list;
		dma_data->priority = DMA_INTR_PRIO;
		dma_data->peripheral_type = MQX_EDMA_HARD_REQ_I2S0_TX;
		
		p_list = scatter_list;

		// fixme ,only support playback
		for(int i = 0; i <  iprtd->periods; i++) {
			p_list->dst_addr = (uint_32)(&i2s_ptr->TDR[TX_CH]);  /* fifo io */
			p_list->length = bytes_fifo_aligned(iprtd->period_bytes, dma_data->fifo_aligned_bits, 
											runtime->sample_phy_bits);
			p_list->src_addr =  (uint_32)(((uint8_t *)iprtd->buf) + (i * p_list->length));
			p_list ++;
		}

		ret = edma_request_channel(SSI_TX_DMA_CHN, audio_dma_irq, runtime, dma_data->priority, NULL, NULL);
		if(ret != MQX_OK) {
		 	printf ("\n  _i2s_init: Error - edma_request_channel 0x%x\n", ret);
			 return  -1;
		}

		ret = edma_config_scatter(SSI_TX_DMA_CHN, MQX_EDMA_MEM_TO_PERI, dma_data->peripheral_type,
			scatter_list, MQX_EDMA_TRANS_SIZE_32_BITS, iprtd->periods,TX_BURST_SIZE_OF_BYTES/* burstsize*/);
		if (ret != MQX_OK) {
			printf("\n	_i2s_tx: Error - edma_config_scatter. 0x%x\n", ret);
			return  -1;
		}
	}

	/* snd_pcm_prepare(runtime);*/   /* drop and setup then prepare !!!  ( prepare alway reset all pointer !!! ) */

	return 0;
}


#endif  // KINETS_SGTL5000
