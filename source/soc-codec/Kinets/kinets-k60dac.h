/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 * <liu090@sina.com>
 */

#ifndef _KINETS_K60DAC_H_
#define _KINETS_K60DAC_H_

/* soc relation head file */
//#include "dma/edma.h"

struct soc_dma_data {
	int priority;
	unsigned short fifo_aligned_bits;	/* fifo aligned bits in bit */
	unsigned short fifo_align_type;
	DAC_MemMapPtr dac0_base;
	DAC_MemMapPtr dac1_base;
};

typedef struct{
    uint32_t SADDR;                                  /**< TCD Source Address, array offset: 0x1000, array step: 0x20 */
    uint16_t SOFF;                                   /**< TCD Signed Source Address Offset, array offset: 0x1004, array step: 0x20 */
    uint16_t ATTR;                                   /**< TCD Transfer Attributes, array offset: 0x1006, array step: 0x20 */
    union {                                          /* offset: 0x1008, array step: 0x20 */
      uint32_t NBYTES_MLNO;                            /**< TCD Minor Byte Count (Minor Loop Disabled), array offset: 0x1008, array step: 0x20 */
      uint32_t NBYTES_MLOFFNO;                         /**< TCD Signed Minor Loop Offset (Minor Loop Enabled and Offset Disabled), array offset: 0x1008, array step: 0x20 */
      uint32_t NBYTES_MLOFFYES;                        /**< TCD Signed Minor Loop Offset (Minor Loop and Offset Enabled), array offset: 0x1008, array step: 0x20 */
    };
    uint32_t SLAST;                                  /**< TCD Last Source Address Adjustment, array offset: 0x100C, array step: 0x20 */
    uint32_t DADDR;                                  /**< TCD Destination Address, array offset: 0x1010, array step: 0x20 */
    uint16_t DOFF;                                   /**< TCD Signed Destination Address Offset, array offset: 0x1014, array step: 0x20 */
    union {                                          /* offset: 0x1016, array step: 0x20 */
      uint16_t CITER_ELINKYES;                         /**< TCD Current Minor Loop Link, Major Loop Count (Channel Linking Enabled), array offset: 0x1016, array step: 0x20 */
      uint16_t CITER_ELINKNO;                          /**< TCD Current Minor Loop Link, Major Loop Count (Channel Linking Disabled), array offset: 0x1016, array step: 0x20 */
    };
    uint32_t DLAST_SGA;                              /**< TCD Last Destination Address Adjustment/Scatter Gather Address, array offset: 0x1018, array step: 0x20 */
    uint16_t CSR;                                    /**< TCD Control and Status, array offset: 0x101C, array step: 0x20 */
    union {                                          /* offset: 0x101E, array step: 0x20 */
      uint16_t BITER_ELINKNO;                          /**< TCD Beginning Minor Loop Link, Major Loop Count (Channel Linking Disabled), array offset: 0x101E, array step: 0x20 */
      uint16_t BITER_ELINKYES;                         /**< TCD Beginning Minor Loop Link, Major Loop Count (Channel Linking Enabled), array offset: 0x101E, array step: 0x20 */
    };
} DMA_TCD_t, *DMA_TCD_p;

#define MAX_TCD_LISTS				 4   /* must match the number of periods  ! */
#define FIFO_ALIGNMENT_TYPE_RIGHT	0x00
#define FIFO_ALIGNMENT_TYPE_LEFT	0x01

#define FIFO_BITS_ALIGNED		16
#define FIFO_BITS_ENDTYPES		FIFO_ALIGNMENT_TYPE_LEFT

/* Enable global DACx Interrup bit*/
#define VECTOR_DAC0_INT_MASK	2
#define VECTOR_DAC1_INT_MASK	4

/* DAXx registers reset values*/
#define DACx_DAT_RESET			0
#define DACx_SR_RESET			2
#define DACx_C0_RESET			0
#define DACx_C1_RESET			0
#define DACx_C2_RESET			15 //0x0f

/* DACx_C0 bits definition*/

#define DAC_DISABLE				0x00
#define DAC_ENABLE				DAC_C0_DACEN_MASK

#define DAC_SEL_VREFO			0x00
#define DAC_SEL_VDDA			DAC_C0_DACRFS_MASK

#define DAC_SEL_PDB_HW_TRIG	0x00
#define DAC_SEL_SW_TRIG		DAC_C0_DACTRGSEL_MASK

#define DAC_SW_TRIG_STOP		0x00
#define DAC_SW_TRIG_NEXT		DAC_C0_DACSWTRG_MASK

#define DAC_HP_MODE			0x00
#define DAC_LP_MODE			DAC_C0_LPEN_MASK

#define DAC_BFWM_INT_DISABLE	0x00
#define DAC_BFWM_INT_ENABLE	DAC_C0_DACBWIEN_MASK

#define DAC_BFT_PTR_INT_DISABLE	0x00
#define DAC_BFT_PTR_INT_ENABLE		DAC_C0_DACBTIEN_MASK

#define DAC_BFB_PTR_INT_DISABLE	0x00
#define DAC_BFB_PTR_INT_ENABLE	DAC_C0_DACBBIEN_MASK

/* DACx_C1 bits definition*/ 
#define DAC_DMA_DISABLE		0x00
#define DAC_DMA_ENABLE			DAC_C1_DMAEN_MASK

#define DAC_BFWM_1WORD		DAC_C1_DACBFWM(0)  
#define DAC_BFWM_2WORDS		DAC_C1_DACBFWM(1) 
#define DAC_BFWM_3WORDS		DAC_C1_DACBFWM(2)  
#define DAC_BFWM_4WORDS		DAC_C1_DACBFWM(3) 

#define DAC_BF_NORMAL_MODE	DAC_C1_DACBFMD(0)
#define DAC_BF_SWING_MODE		DAC_C1_DACBFMD(1) 
#define DAC_BF_ONE_TIME_MODE	DAC_C1_DACBFMD(2)

#define DAC_BF_DISABLE			0x00
#define DAC_BF_ENABLE			DAC_C1_DACBFEN_MASK 

/* DACx_C2 bits definition*/ 
#define DAC_SET_PTR_AT_BF(x)	DAC_C2_DACBFRP(x)
#define DAC_SET_PTR_UP_LIMIT(x)	DAC_C2_DACBFUP(x)


#define Watermark_One_Word		0
#define Watermark_Two_Words	1
#define Watermark_Three_Words	2
#define Watermark_Four_Words	3

#define Clear_DACBFWMF			0x03
#define Clear_DACBFRPTF			0x05
#define Clear_DACBFRPBF			0x06

#define PDB_DACINTC0_TOE_MASK	0x1u
///////////////////////////////////////////
#define DEFAULT_AUDIO_SAMPLERATE	44100

#define  DAM16WORD_TWICE

/* simulate */
#define DMA_CHANNEL_DAC0 			0
#define DMA_CHANNEL_DAC1 			1

#ifdef DAM16WORD_TWICE
#define DAC_BUFFER_SIZE			(0x10)
#else
#define DAC_BUFFER_SIZE			(0x10 * 2)
#endif
#define AUDIO_DMA_BUFFER_SIZE		0x200
#define DMA_CH0_ISR_NUM			0
#define DMA_ERROR_ISR_NUM			16
#define DMA_REQUEST_DAC0			45
#define DMA_REQUEST_DAC1			46
#define SAMPLE_RATE_44100HZ		44100
#define SAMPLE_RATE_1KHZ			1000

#define DAC_TWO_CHANNEL


#endif /* _KINETS_K60DAC_H_ */
