/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 * soc_cfg.h - select a soc codec interface 
 * <liu090@sina.com>
 */

#ifndef _SOC_CFG_H_
#define _SOC_CFG_H_

#define __LITTLE_ENDIAN
 /* define for only support 8,16,24,32 LE,BE signed,unsinged 
     or support all formats */
#define LIMITED_FORMATS   

/* soc codec select */
//#define KINETS_SGTL5000
//#define KINETS_K60DAC   // K60/K70 DAC 2 channels
//#define KINETS_WM8962
#define KINETS_K60ADC_DAC  // K60 ADC/DAC

#ifdef KINETS_SGTL5000
/**************************KINETS-SGTL5000 setting***************************/
#include "kinets\kinets-sgtl5000.h"

/* KINETS-SGTL5000 */
#define CODEC_MASTER

#ifdef CODEC_MASTER
#define SGTL5000_MCLK  24576000
#define LDO_VOLTAGE		1200000
#endif

#define  Wait_avail_min					1    /* unit is frame */

#define SSI_FIFO_DEFAULT_WATERMARK  	4    /* burstsize */
#define FIFO_SIZE_OF_INTS				8
#define TX_BURST_SIZE_OF_BYTES			/*(sizeof (int )) */ (4 * (FIFO_SIZE_OF_INTS - SSI_FIFO_DEFAULT_WATERMARK))

#define DMA_INTR_PRIO					/*5*/5     /* dma complete intr priority */
#define TX_CH							0     /* sync mode */
#define RX_CH							TX_CH   /* same as init_sai.c or init_sai_dma.h */

#define SSI_TX_DMA_CHN					0                         	/* the edma channel of the all of 16 chs */
#define SSI_RX_DMA_CHN					1                        


#define I2S_TX_FIFO_DEFAULT_WATERMARK  4
#define I2S_RX_FIFO_DEFAULT_WATERMARK  4
#define I2S_FIFO_DEFAULT_WATERMARK        4    /* fifo legth is 8 x32bit  */ /* 4 words */

/**************************************************************************/
#endif  /* KINETS_SGTL5000 */


#ifdef KINETS_K60DAC
/**************************KINETS-K60DAC setting***************************/
#define DMA_INTR_PRIO					5     /* dma complete intr priority */
#define  Wait_avail_min					1       /* unit is frame */

#endif  /* KINETS_K60DAC */


#ifdef KINETS_WM8962
/**************************KINETS-WM8962 setting***************************/
#define DMA_INTR_PRIO					5     /* dma complete intr priority */
#define  Wait_avail_min					1       /* unit is frame */

#endif  /* KINETS_WM8962 */

#ifdef KINETS_K60ADC_DAC
/**************************KINETS-K60ADC_DAC setting***************************/
#define DMA_INTR_PRIO					5     /* dma complete intr priority */
#define  Wait_avail_min					1       /* unit is frame */
#endif

#endif  /* _SOC_CFG_H_ */

/* EOF */
