/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 * soc_codec.h - soc codec interface function declare
 * <liu090@sina.com>
 */

#ifndef _SOC_CODEC_H_
#define _SOC_CODEC_H_

/* soc-codec funtion declare */
int sco_pcm_open(struct snd_pcm_runtime *runtime);
int sco_pcm_close(struct snd_pcm_runtime *runtime);
int sco_pcm_prepare(struct snd_pcm_runtime *runtime);
int sco_pcm_params(struct snd_pcm_runtime *runtime,	struct snd_pcm_params *params);
int sco_pcm_trigger(struct snd_pcm_runtime *runtime, int cmd);
int sco_pcm_stream_copy (struct snd_pcm_runtime *runtime, int channel, 
								snd_pcm_uframes_t hwoff,
								char *buf, snd_pcm_uframes_t count);
void  sco_pcm_lock(struct snd_pcm_runtime *runtime);
void  sco_pcm_unlock(struct snd_pcm_runtime *runtime);
void sco_msleep(unsigned int ms);
int sco_get_ticks_interval(struct snd_pcm_runtime *runtime);

int sco_pcm_vol_down();
int sco_pcm_vol_up();

/* ticks */
/* now return is system elapsed ms */
unsigned int get_curr_ticks(void);
unsigned int  get_hz(void);

/* type declare */
struct sco_pcm_runtime_data {
	int period_bytes;
	int periods;
	int dma;
	unsigned long offset;
	unsigned long size;
	void *buf;
	int period_time;
	/* struct soc_dma_data dma_data; */
	void * dma_data;
};

static inline unsigned int bytes_fifo_aligned(unsigned int buffer_bytes,
	unsigned int fifo_bits, unsigned int sample_bits)
{
	/* cut down
	if(fifo_bits % sample_bits)
		return (buffer_bytes * sample_bits)/fifo_bits;
	else
		return buffer_bytes/(fifo_bits/sample_bits); */
	/* scale up */	
	return (buffer_bytes * fifo_bits)/sample_bits;
}

#define  NEW_PCM_SEM(pcm_sem) \
do { \
	    _lwsem_create((LWSEM_STRUCT_PTR)(&pcm_sem), 1);\
} while(0);


#define   GET_PCM_SEM(pcm_sem) \
do { \
	    _lwsem_wait((LWSEM_STRUCT_PTR)&pcm_sem); \
} while(0);

#define   PUT_PCM_SEM(pcm_sem) \
do { \
	    _lwsem_post((LWSEM_STRUCT_PTR)&pcm_sem); \
} while(0);

#define  DEL_PCM_SEM(pcm_sem) \
do { \
	   _lwsem_destroy((LWSEM_STRUCT_PTR)&pcm_sem);\
} while(0);

#endif  /* _SOC_CODEC_H_ */ 

/* EOF */
