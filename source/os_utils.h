/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 * os_utils.h - system layer function declare
 * 
 * <liu090@sina.com>
 */
#ifndef OS_UTILS_H
#define OS_UTILS_H
/*
#include <ctype.h>
#include <string.h>
#include <stdlib.h>
*/
#include <mqx.h>
#include <bsp.h>

#include "lwevent.h"
#include "lwmsgq.h"
#include "mutex.h"
#include "lwsem.h"

_mqx_uint _lwmsgq_deinit(pointer location);
#if _DEBUG == 1
//#define OS_DEBUG
#endif

/* build */
typedef int	ssize_t;

/* building ***************************************************/
/* os function */
#if MQX_USE_UNCACHED_MEM && PSP_HAS_DATA_CACHE
    #define PCM_mem_alloc_uncached(n)         	_mem_alloc_system_uncached(n)
    #define PCM_mem_alloc_uncached_zero(n)    _mem_alloc_system_zero_uncached(n)
    #define PCM_mem_alloc_uncached_align(n,a) _mem_alloc_align_uncached(n,a)
#else
    #define PCM_mem_alloc_uncached(n)        	 _mem_alloc_system(n)
    #define PCM_mem_alloc_uncached_zero(n)    _mem_alloc_system_zero(n)
    #define PCM_mem_alloc_uncached_align(n,a) _mem_alloc_align(n,a)
#endif   /*  PSP_HAS_DATA_CACHE */

#define PCM_mem_alloc(n)                      _mem_alloc_system(n)
#define PCM_mem_alloc_zero(n)              _mem_alloc_system_zero(n)
#define PCM_mem_alloc_align(n,a)			_mem_alloc_align(n,a)

#define PCM_mem_free(ptr)                     _mem_free(ptr)
#define PCM_mem_zero(ptr,n)                 _mem_zero(ptr,n)
#define PCM_mem_copy(src,dst,n)           _mem_copy(src,dst,n)

#define os_memcpy	memcpy
#define os_memset	memset
#define os_malloc	malloc
#define os_free		free
#define os_zalloc   	PCM_mem_alloc_zero

/* misc */
#define PCM_lock(x)					sco_pcm_lock(x)
#define PCM_unlock(x)					sco_pcm_unlock(x)

#define snd_pcm_stream_lock_irq(x)  \
do { \
	PCM_lock(x); \
	/*_int_disable();*/ \
} while (0)

#define snd_pcm_stream_unlock_irq(x) \
do { \
	PCM_unlock(x); \
	 /*_int_enable();*/ \
} while (0)

#define snd_pcm_stream_lock_irqsave(x, flags) \
do { \
	PCM_lock(x); \
	/*_int_disable();*/ \
} while (0)

#define snd_pcm_stream_unlock_irqrestore(x, flags) \
do { \
	PCM_unlock(x); \
	/* _int_enable();*/ \
} while (0)

/* debug message */
#define __stringify_1(x...)	#x
#define __stringify(x...)	__stringify_1(x)

#define halt() do { for(;;); } while (0)
#define ASSERT(x) do { if (!(x)) { printf("%s:%d ASSERT failed: %s\n", __FILE__, __LINE__, #x); halt(); } } while (0)

#if 0
#define snd_BUG_ON(cond)	\
	do { \
		if(cond)  printf(1, __FILE__, __LINE__, "BUG? (%s)\n", __stringify(cond)); \
	} while (0)
#else
static inline int snd_BUG_ON(int cond)
{
  if(cond)  printf("BUG? in file %s:%d \n", __FILE__, __LINE__);
  return cond;
}
#endif

/* If you are writing a driver, please use dev_dbg instead */
#if defined(OS_DEBUG)
#define pr_debug(fmt, ...) \
	printf(pr_fmt(fmt), ##__VA_ARGS__)
#define pr_warning(fmt, ...) \
	printf(pr_fmt(fmt), ##__VA_ARGS__)
#define pr_warn pr_warning	
#define os_printf(fmt, ...) \
	printf(pr_fmt(fmt), ##__VA_ARGS__)
#else
#define pr_debug(fmt, ...)
#define pr_warning(fmt, ...)
#define pr_warn	
#define os_printf(fmt, ...)
#endif

#ifndef pr_fmt
#define pr_fmt(fmt) fmt
#endif

#define pr_err(fmt, ...) \
	printf(pr_fmt(fmt), ##__VA_ARGS__)

#define USHRT_MAX	((u16)(~0U))
#define SHRT_MAX	((s16)(USHRT_MAX>>1))
#define SHRT_MIN	((s16)(-SHRT_MAX - 1))
#define INT_MAX		((int)(~0U>>1))
#define INT_MIN		(-INT_MAX - 1)
#define UINT_MAX	(~0U)
#define LONG_MAX	((long)(~0UL>>1))
#define LONG_MIN	(-LONG_MAX - 1)
#define ULONG_MAX	(~0UL)
#define LLONG_MAX	((long long)(~0ULL>>1))
#define LLONG_MIN	(-LLONG_MAX - 1)
#define ULLONG_MAX	(~0ULL)

#ifndef min
#define min(x, y)  (x < y ? x : y)
#endif
#ifndef max
#define max(x, y)  (x > y ? x : y)
#endif

#define ULONG_MAX	(~0UL)
#define LONG_MAX	((long)(~0UL>>1))
/* building ***************************************************/

#define ARRAY_SIZE(arr) (sizeof(arr) / sizeof((arr)[0]) /*+ __must_be_array(arr)*/)

/* critial , mutex */
//#define OS_DEFINE_MUTEX
#define os_mutex_init
#define os_mutex_lock
#define os_mutex_unlock
                
/* IO */
#define writeb(v,a)	(*(volatile unsigned char __force  *)(a) = (v))
#define writew(v,a)	(*(volatile unsigned short __force *)(a) = (v))
#define writel(v,a)	(*(volatile unsigned int __force   *)(a) = (v))

#define readb(a)		(*(volatile unsigned char __force  *)(a))
#define readw(a)		(*(volatile unsigned short __force *)(a))
#define readl(a)		(*(volatile unsigned int __force   *)(a))

/* ticks implement move to soc-codec */
extern uint32_t HZ;
#define os_msleep   sco_msleep
#define os_get_ticks_interval  //  sco_get_ticks_interval

/* for sgtl5000 internal power */
static inline uint16_t clamp(uint16_t val,uint16_t min,uint16_t max)
{	
	uint16_t __val = (val);		
	uint16_t __min = (min);	
	uint16_t __max = (max);
	__val = __val < __min ? __min: __val;
	return (__val > __max ? __max: __val); 
}

static inline unsigned int ld2(uint32_t v)
{
        unsigned r = 0;

        if (v >= 0x10000) {
                v >>= 16;
                r += 16;
        }
        if (v >= 0x100) {
                v >>= 8;
                r += 8;
        }
        if (v >= 0x10) {
                v >>= 4;
                r += 4;
        }
        if (v >= 4) {
                v >>= 2;
                r += 2;
        }
        if (v >= 2)
                r++;
        return r;
}

#endif /* OS_UTILS_H */
