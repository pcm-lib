/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 * pcm.c - pcm internal function
 * <liu090@sina.com>
 */

#include "os_utils.h"

#include "pcm.h"

/* global var */
uint32_t HZ = 0;
unsigned int sys_dma_inited = 0;   /* if other driver inited dma ,set this flag to ture */

/* pcm misc */
#define INT	int

//#define XRUN_AND_INTRLOST_DEBUG

static const struct pcm_format_data pcm_formats[(INT)SNDRV_PCM_FORMAT_LAST+1] = {
	[SNDRV_PCM_FORMAT_S8] = {
		.width = 8, .phys = 8, .le = -1, .signd = 1,
		.silence = {0},
	},
	[SNDRV_PCM_FORMAT_U8] = {
		.width = 8, .phys = 8, .le = -1, .signd = 0,
		.silence = { 0x80 },
	},
	[SNDRV_PCM_FORMAT_S16_LE] = {
		.width = 16, .phys = 16, .le = 1, .signd = 1,
		.silence = {0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00},
	},
	[SNDRV_PCM_FORMAT_S16_BE] = {
		.width = 16, .phys = 16, .le = 0, .signd = 1,
		.silence = {0},
	},
	[SNDRV_PCM_FORMAT_U16_LE] = {
		.width = 16, .phys = 16, .le = 1, .signd = 0,
		.silence = { 0x00, 0x80 },
	},
	[SNDRV_PCM_FORMAT_U16_BE] = {
		.width = 16, .phys = 16, .le = 0, .signd = 0,
		.silence = { 0x80, 0x00 },
	},
	[SNDRV_PCM_FORMAT_S24_LE] = {
		.width = 24, .phys = 32, .le = 1, .signd = 1,
		.silence = {0},
	},
	[SNDRV_PCM_FORMAT_S24_BE] = {
		.width = 24, .phys = 32, .le = 0, .signd = 1,
		.silence = {0},
	},
	[SNDRV_PCM_FORMAT_U24_LE] = {
		.width = 24, .phys = 32, .le = 1, .signd = 0,
		.silence = { 0x00, 0x00, 0x80 },
	},
	[SNDRV_PCM_FORMAT_U24_BE] = {
		.width = 24, .phys = 32, .le = 0, .signd = 0,
		.silence = { 0x00, 0x80, 0x00, 0x00 },
	},
	[SNDRV_PCM_FORMAT_S32_LE] = {
		.width = 32, .phys = 32, .le = 1, .signd = 1,
		.silence = {0},
	},
	[SNDRV_PCM_FORMAT_S32_BE] = {
		.width = 32, .phys = 32, .le = 0, .signd = 1,
		.silence = {0},
	},
	[SNDRV_PCM_FORMAT_U32_LE] = {
		.width = 32, .phys = 32, .le = 1, .signd = 0,
		.silence = { 0x00, 0x00, 0x00, 0x80 },
	},
	[SNDRV_PCM_FORMAT_U32_BE] = {
		.width = 32, .phys = 32, .le = 0, .signd = 0,
		.silence = { 0x80, 0x00, 0x00, 0x00 },
	},
#if 0
	[SNDRV_PCM_FORMAT_FLOAT_LE] = {
		.width = 32, .phys = 32, .le = 1, .signd = -1,
		.silence = {0},
	},
	[SNDRV_PCM_FORMAT_FLOAT_BE] = {
		.width = 32, .phys = 32, .le = 0, .signd = -1,
		.silence = {0},
	},
	[SNDRV_PCM_FORMAT_FLOAT64_LE] = {
		.width = 64, .phys = 64, .le = 1, .signd = -1,
		.silence = {0},
	},
	[SNDRV_PCM_FORMAT_FLOAT64_BE] = {
		.width = 64, .phys = 64, .le = 0, .signd = -1,
		.silence = {0},
	},
#else
	[SNDRV_PCM_FORMAT_S24_3LE] = {
		.width = 24, .phys = 24, .le = 1, .signd = 1,
		.silence = {0},
	},
	[SNDRV_PCM_FORMAT_S24_3BE] = {
		.width = 24, .phys = 24, .le = 0, .signd = 1,
		.silence = {0},
	},
	[SNDRV_PCM_FORMAT_U24_3LE] = {
		.width = 24, .phys = 24, .le = 1, .signd = 0,
		.silence = { 0x00, 0x00, 0x80 },
	},
	[SNDRV_PCM_FORMAT_U24_3BE] = {
		.width = 24, .phys = 24, .le = 0, .signd = 0,
		.silence = { 0x80, 0x00, 0x00 },
	},
#endif
#ifndef LIMITED_FORMATS        
	[SNDRV_PCM_FORMAT_IEC958_SUBFRAME_LE] = {
		.width = 32, .phys = 32, .le = 1, .signd = -1,
		.silence = {0},
	},
	[SNDRV_PCM_FORMAT_IEC958_SUBFRAME_BE] = {
		.width = 32, .phys = 32, .le = 0, .signd = -1,
		.silence = {0},
	},
	[SNDRV_PCM_FORMAT_MU_LAW] = {
		.width = 8, .phys = 8, .le = -1, .signd = -1,
		.silence = { 0x7f },
	},
	[SNDRV_PCM_FORMAT_A_LAW] = {
		.width = 8, .phys = 8, .le = -1, .signd = -1,
		.silence = { 0x55 },
	},
	[SNDRV_PCM_FORMAT_IMA_ADPCM] = {
		.width = 4, .phys = 4, .le = -1, .signd = -1,
		.silence = {0},
	},
	[SNDRV_PCM_FORMAT_G723_24] = {
		.width = 3, .phys = 3, .le = -1, .signd = -1,
		.silence = {0},
	},
	[SNDRV_PCM_FORMAT_G723_40] = {
		.width = 5, .phys = 5, .le = -1, .signd = -1,
		.silence = {0},
	},
	/* FIXME: the following three formats are not defined properly yet */
	[SNDRV_PCM_FORMAT_MPEG] = {
		.le = -1, .signd = -1,
	},
	[SNDRV_PCM_FORMAT_GSM] = {
		.le = -1, .signd = -1,
	},
	[SNDRV_PCM_FORMAT_SPECIAL] = {
		.le = -1, .signd = -1,
	},
#if 0        
	[SNDRV_PCM_FORMAT_S24_3LE] = {
		.width = 24, .phys = 24, .le = 1, .signd = 1,
		.silence = {0},
	},
	[SNDRV_PCM_FORMAT_S24_3BE] = {
		.width = 24, .phys = 24, .le = 0, .signd = 1,
		.silence = {0},
	},
	[SNDRV_PCM_FORMAT_U24_3LE] = {
		.width = 24, .phys = 24, .le = 1, .signd = 0,
		.silence = { 0x00, 0x00, 0x80 },
	},
	[SNDRV_PCM_FORMAT_U24_3BE] = {
		.width = 24, .phys = 24, .le = 0, .signd = 0,
		.silence = { 0x80, 0x00, 0x00 },
	},
#else
	[SNDRV_PCM_FORMAT_FLOAT_LE] = {
		.width = 32, .phys = 32, .le = 1, .signd = -1,
		.silence = {0},
	},
	[SNDRV_PCM_FORMAT_FLOAT_BE] = {
		.width = 32, .phys = 32, .le = 0, .signd = -1,
		.silence = {0},
	},
	[SNDRV_PCM_FORMAT_FLOAT64_LE] = {
		.width = 64, .phys = 64, .le = 1, .signd = -1,
		.silence = {0},
	},
	[SNDRV_PCM_FORMAT_FLOAT64_BE] = {
		.width = 64, .phys = 64, .le = 0, .signd = -1,
		.silence = {0},
	},
#endif
	[SNDRV_PCM_FORMAT_S20_3LE] = {
		.width = 20, .phys = 24, .le = 1, .signd = 1,
		.silence = {0},
	},
	[SNDRV_PCM_FORMAT_S20_3BE] = {
		.width = 20, .phys = 24, .le = 0, .signd = 1,
		.silence = {0},
	},
	[SNDRV_PCM_FORMAT_U20_3LE] = {
		.width = 20, .phys = 24, .le = 1, .signd = 0,
		.silence = { 0x00, 0x00, 0x08 },
	},
	[SNDRV_PCM_FORMAT_U20_3BE] = {
		.width = 20, .phys = 24, .le = 0, .signd = 0,
		.silence = { 0x08, 0x00, 0x00 },
	},
	[SNDRV_PCM_FORMAT_S18_3LE] = {
		.width = 18, .phys = 24, .le = 1, .signd = 1,
		.silence = {0},
	},
	[SNDRV_PCM_FORMAT_S18_3BE] = {
		.width = 18, .phys = 24, .le = 0, .signd = 1,
		.silence = {0},
	},
	[SNDRV_PCM_FORMAT_U18_3LE] = {
		.width = 18, .phys = 24, .le = 1, .signd = 0,
		.silence = { 0x00, 0x00, 0x02 },
	},
	[SNDRV_PCM_FORMAT_U18_3BE] = {
		.width = 18, .phys = 24, .le = 0, .signd = 0,
		.silence = { 0x02, 0x00, 0x00 },
	},
	[SNDRV_PCM_FORMAT_G723_24_1B] = {
		.width = 3, .phys = 8, .le = -1, .signd = -1,
		.silence = {0},
	},
	[SNDRV_PCM_FORMAT_G723_40_1B] = {
		.width = 5, .phys = 8, .le = -1, .signd = -1,
		.silence = {0},
	},
#endif	
};

/* pre declare */
static void snd_pcm_period_elapsed(struct snd_pcm_runtime *runtime);
static int  snd_pcm_attach_runtime(struct snd_pcm_runtime **runtime);
static void snd_pcm_detach_runtime(struct snd_pcm_runtime *runtime);
int snd_pcm_update_hw_ptr(struct snd_pcm_runtime *runtime);


/******************************************************************************/
/* dma operation functions */
static snd_pcm_uframes_t snd_pcm_pointer(struct snd_pcm_runtime *runtime,int in_interrupt)
{
	struct sco_pcm_runtime_data *iprtd = runtime->private_data;

	/* printf("%s: %ld %ld,%d\n", __func__, iprtd->offset,
			bytes_to_frames(runtime, iprtd->offset),in_interrupt);
	*/
	return bytes_to_frames(runtime, iprtd->offset);
}

/**
* dma interrupt callback
*/
void audio_dma_irq(void *data,uint_8 chan)
{
	struct snd_pcm_runtime *runtime = (struct snd_pcm_runtime *)data;
	struct sco_pcm_runtime_data *iprtd = (struct sco_pcm_runtime_data *)runtime->private_data;

	iprtd->offset += iprtd->period_bytes;
	iprtd->offset %= iprtd->period_bytes * iprtd->periods;  /* offeset = 0,1period, 2periods ... n periods .(circle) */

	snd_pcm_period_elapsed(runtime);   /* update ring buffer by call snd_pcm_update_hw_ptr0 */

	// os_get_ticks_interval (runtime);  // hw_ptr_ticks_inteval ,get accurate intrrupts interval
}

/**
 * set the silence data on the buffer
 */
static int snd_pcm_format_set_silence(struct snd_pcm_runtime *runtime, void *data, unsigned int samples)
{
	int width;
	unsigned char *dst;
	const unsigned char * pat;
	snd_pcm_format_t format;

	format = runtime->format;

	if ((INT)format < 0 || (INT)format > (INT)SNDRV_PCM_FORMAT_LAST)
		return -1;
	if (samples == 0)
		return 0;
	/* physical width */
	width = pcm_formats[(INT)format].phys;
	pat = pcm_formats[(INT)format].silence;
	if (! width)
		return -1;
	/* signed or 1 byte data */
	if (pcm_formats[(INT)format].signd == 1 || width <= 8) {
		unsigned int bytes = samples * width / 8;
		// fixme
		bytes = (bytes  * runtime->sample_aligned_bits / width  /*runtime->sample_bits*/);
		os_memset(data, *pat, bytes);
		return 0;
	}
	/* non-zero samples, fill using a loop */
	width /= 8;
	dst = data;
#if 0
	while (samples--) {
		memcpy(dst, pat, width);
		dst += width;
	}
#else
	/* a bit optimization for constant width */
	switch (width) {
	case 2:
		while (samples--) {
			os_memcpy(dst, pat, 2);
			dst += 2;
		}
		break;
	case 3:
		while (samples--) {
			os_memcpy(dst, pat, 3);
			dst += 3;
		}
		break;
	case 4:
		while (samples--) {
			os_memcpy(dst, pat, 4);
			dst += 4;
		}
		break;
	case 8:
		while (samples--) {
			os_memcpy(dst, pat, 8);
			dst += 8;
		}
		break;
	}
#endif
	return 0;
}
/* end pcm misc */

/*
 * fill ring buffer with silence
 * runtime->silence_start: starting pointer to silence area
 * runtime->silence_filled: size filled with silence
 * runtime->silence_threshold: threshold from application
 * runtime->silence_size: maximal size from application
 *
 * when runtime->silence_size >= runtime->boundary - fill processed area with silence immediately
 */
void snd_pcm_playback_silence(struct snd_pcm_runtime *runtime, snd_pcm_uframes_t new_hw_ptr)
{
	snd_pcm_uframes_t frames, ofs, transfer;

	if (runtime->silence_size < runtime->boundary) {
		snd_pcm_sframes_t noise_dist, n;
		if (runtime->silence_start != runtime->control->appl_ptr) {
			n = runtime->control->appl_ptr - runtime->silence_start;
			if (n < 0)
				n += runtime->boundary;
			if ((snd_pcm_uframes_t)n < runtime->silence_filled)
				runtime->silence_filled -= n;
			else
				runtime->silence_filled = 0;
			runtime->silence_start = runtime->control->appl_ptr;
		}
		if (runtime->silence_filled >= runtime->buffer_size)
			return;
		noise_dist = snd_pcm_playback_hw_avail(runtime) + runtime->silence_filled;
		if (noise_dist >= (snd_pcm_sframes_t) runtime->silence_threshold)
			return;
		frames = runtime->silence_threshold - noise_dist;
		if (frames > runtime->silence_size)
			frames = runtime->silence_size;
	} else {
		if (new_hw_ptr == ULONG_MAX) {	/* initialization */
			snd_pcm_sframes_t avail = snd_pcm_playback_hw_avail(runtime);
			if (avail > runtime->buffer_size)
				avail = runtime->buffer_size;
			runtime->silence_filled = avail > 0 ? avail : 0;
			runtime->silence_start = (runtime->status->hw_ptr +
						  runtime->silence_filled) %
						 runtime->boundary;    /* initialization silence_start pointer always at the end of valid pcm data */
		} else {
			ofs = runtime->status->hw_ptr;
			frames = new_hw_ptr - ofs;
			if ((snd_pcm_sframes_t)frames < 0)
				frames += runtime->boundary;
			runtime->silence_filled -= frames;
			if ((snd_pcm_sframes_t)runtime->silence_filled < 0) {
				runtime->silence_filled = 0;
				runtime->silence_start = new_hw_ptr;
			} else {
				runtime->silence_start = ofs;
			}
		}
		/* at init, fill silence from the end of valid pcm data to the end of buffer  */
		frames = runtime->buffer_size - runtime->silence_filled;
	}
	if (snd_BUG_ON(frames > runtime->buffer_size))
		return;
	if (frames == 0)
		return;
	ofs = runtime->silence_start % runtime->buffer_size;
	while (frames > 0) {
		transfer = ofs + frames > runtime->buffer_size ? runtime->buffer_size - ofs : frames;
		if (runtime->access == SNDRV_PCM_ACCESS_INTERLEAVED) { /* left sample,  right sample */
			/* if (ops->silence) { sco_pcm_silence
				int err;
				err = ops->silence(runtime, -1, ofs, transfer);
				snd_BUG_ON(err < 0);
			} else {*/
				//unsigned char *hwbuf = runtime->dma_area + frames_to_bytes(runtime, ofs) * (runtime->sample_aligned_bits / runtime->sample_bits); /* fixme */
			unsigned char *hwbuf = runtime->dma_area + frames_to_bytes(runtime, ofs) * runtime->sample_aligned_bits / runtime->sample_phy_bits/* runtime->sample_bits*/; /* fixme */
			/* remove silence fill temprary ,fixme */
			snd_pcm_format_set_silence(runtime/*->format*/, hwbuf, transfer * runtime->channels );
			/*}*/
		} else {
			/* support lefts and rights */
			unsigned int c;
			unsigned int channels = runtime->channels;
			/* sco_pcm_silence dont implement
			if (ops->silence) {
				for (c = 0; c < channels; ++c) {
					int err;
					err = ops->silence(runtime, c, ofs, transfer);
					snd_BUG_ON(err < 0);
				}
			} else { */
				size_t dma_csize = runtime->dma_bytes  / channels;      /* left samples | right samples */
				for (c = 0; c < channels; ++c) {      /* fixme for sample aliged size in dma buff */
					char *hwbuf = (char *)runtime->dma_area + (c * dma_csize) + samples_to_bytes(runtime, ofs);
					snd_pcm_format_set_silence(runtime/*->format*/, hwbuf, transfer);
				}
		}
		runtime->silence_filled += transfer;
		frames -= transfer;
		ofs = 0;
	}
}


/*******************************************************************/
/* start ,stop,reset ,prepare */
/*
 * start callbacks
 */
struct action_ops {
	int (*pre_action)(struct snd_pcm_runtime *runtime, int state);
	int (*do_action)(struct snd_pcm_runtime *runtime, int state);
	void (*undo_action)(struct snd_pcm_runtime *runtime, int state);
	void (*post_action)(struct snd_pcm_runtime *runtime, int state);
};

static int snd_pcm_action(struct action_ops *ops,
			struct snd_pcm_runtime *runtime, int state)
{
	int res;

	/* snd_pcm_start  set state = SNDRV_PCM_STATE_RUNNING */
	res = ops->pre_action(runtime, state);
	if (res < 0)
		return res;
	res = ops->do_action(runtime, state);
	if (res == 0)
		ops->post_action(runtime, state);
	else if (ops->undo_action)
		ops->undo_action(runtime, state);
	return res;
}

static int snd_pcm_pre_start(struct snd_pcm_runtime *runtime, int state)
{
	if (runtime->status->state != SNDRV_PCM_STATE_PREPARED)
		return -1;
	return 0;
}

static int snd_pcm_do_start(struct snd_pcm_runtime *runtime, int state)
{
	// the order of triger start
	//dma triger start
	//cpu interface triger start
	return sco_pcm_trigger(runtime ,SNDRV_PCM_TRIGGER_START);
}

static void snd_pcm_undo_start(struct snd_pcm_runtime *runtime, int state)
{
	//dma triger stop
	//cpu interface triger stop 
	sco_pcm_trigger(runtime ,/*SNDRV_PCM_TRIGGER_START*/ SNDRV_PCM_TRIGGER_STOP);
}

static void snd_pcm_post_start(struct snd_pcm_runtime *runtime, int state)
{
	runtime->hw_ptr_ticks = get_curr_ticks();
	runtime->hw_ptr_buffer_ticks = (runtime->buffer_size * HZ) / runtime->rate;
	runtime->status->state = state;

	if (runtime->stream == SNDRV_PCM_STREAM_PLAYBACK  &&  runtime->silence_size > 0)
		snd_pcm_playback_silence(runtime, ULONG_MAX);
}

static struct action_ops snd_pcm_action_start = {
	.pre_action = snd_pcm_pre_start,
	.do_action = snd_pcm_do_start,
	.undo_action = snd_pcm_undo_start,
	.post_action = snd_pcm_post_start
};

static int snd_pcm_start(struct snd_pcm_runtime *runtime)
{
	/* previous state is prepare */
	int ret;
	ret = snd_pcm_action(&snd_pcm_action_start, runtime,
			      SNDRV_PCM_STATE_RUNNING);
        return ret;
}

/*
 * stop callbacks
 */
static int snd_pcm_pre_stop(struct snd_pcm_runtime *runtime, int state)
{
	if (runtime->status->state == SNDRV_PCM_STATE_OPEN)
		return -1;
	return 0;
}

static int snd_pcm_do_stop(struct snd_pcm_runtime *runtime, int state)
{
	if (snd_pcm_running(runtime)){
		//ops->trigger(runtime, SNDRV_PCM_TRIGGER_STOP);
		//dma triger stop
		//cpu interface triger stop
		return sco_pcm_trigger(runtime ,SNDRV_PCM_TRIGGER_STOP);
	}
	return 0; /* unconditonally stop all substreams */
}

static void snd_pcm_post_stop(struct snd_pcm_runtime *runtime, int state)
{
	if (runtime->status->state != state) {
		runtime->status->state = state;
	}
	// To wake_up something at this
}

static struct action_ops snd_pcm_action_stop = {
	.pre_action = snd_pcm_pre_stop,
	.do_action = snd_pcm_do_stop,
	.post_action = snd_pcm_post_stop
};

/**
 * try to stop all running streams in the substream group
 * The state of each stream is then changed to the given state unconditionally.
 */
int snd_pcm_stop(struct snd_pcm_runtime *runtime,  snd_pcm_state_t state)
{
	return snd_pcm_action(&snd_pcm_action_stop, runtime, state);
}

/*
 * reset
 */
static int snd_pcm_lib_reset(struct snd_pcm_runtime *runtime,
				   void *arg)
{
	unsigned long flags;
	snd_pcm_stream_lock_irqsave(runtime, flags);
	if (snd_pcm_running(runtime) &&
	    snd_pcm_update_hw_ptr(runtime) >= 0){
	    printf("reset at run time,hw_ptr >0\n");
		runtime->status->hw_ptr %= runtime->buffer_size;
		}
	else{
		//---printf("reset at not runtime \n");
		runtime->status->hw_ptr = 0;
		}
	snd_pcm_stream_unlock_irqrestore(runtime, flags);
	return 0;
}

static int snd_pcm_pre_reset(struct snd_pcm_runtime *runtime, int state)
{
	switch (runtime->status->state) {
	case SNDRV_PCM_STATE_RUNNING:
	case SNDRV_PCM_STATE_PREPARED:
	case SNDRV_PCM_STATE_PAUSED:
	case SNDRV_PCM_STATE_SUSPENDED:
		return 0;
	default:
		return -1;
	}
}

static int snd_pcm_do_reset(struct snd_pcm_runtime *runtime, int state)
{
	/* snd_pcm_lib_reset  => runtime->status->hw_ptr = 0; */
	int err = snd_pcm_lib_reset(runtime, NULL);
	if (err < 0)
		return err;
	runtime->hw_ptr_base = 0;
	runtime->hw_ptr_interrupt = runtime->status->hw_ptr -
		runtime->status->hw_ptr % runtime->period_size;
	runtime->silence_start = runtime->status->hw_ptr;
	runtime->silence_filled = 0;
	return 0;
}

static void snd_pcm_post_reset(struct snd_pcm_runtime *runtime, int state)
{
	runtime->control->appl_ptr = runtime->status->hw_ptr;
	/* only playback */
	if (runtime->stream == SNDRV_PCM_STREAM_PLAYBACK  &&  runtime->silence_size > 0)
		snd_pcm_playback_silence(runtime, ULONG_MAX);
}

static struct action_ops snd_pcm_action_reset = {
	.pre_action = snd_pcm_pre_reset,
	.do_action = snd_pcm_do_reset,
	.post_action = snd_pcm_post_reset
};

int snd_pcm_reset(struct snd_pcm_runtime *runtime)
{
	/* snd_pcm_start  set state = SNDRV_PCM_STATE_RUNNING */
	return snd_pcm_action(&snd_pcm_action_reset, runtime,
			      SNDRV_PCM_STATE_RUNNING);
}

/* we use the second argument for updating f_flags, 
	must prepare before runing */
static int snd_pcm_pre_prepare(struct snd_pcm_runtime *runtime, int f_flags)
{
	if (runtime->status->state == SNDRV_PCM_STATE_OPEN ||
	    runtime->status->state == SNDRV_PCM_STATE_DISCONNECTED)
		return -1;
	if (snd_pcm_running(runtime))   /*  if pcm in the running state */
		return -1;
	/*runtime->f_flags = f_flags;*/
	return 0;
}

static int snd_pcm_do_prepare(struct snd_pcm_runtime *runtime, int state)
{
#if 1
	sco_pcm_prepare(runtime);

	/* mark '*' is essential  ( sco_pcm_prepare )
		pre init prepare
		* dma prepare
		codec interface prepare
		cpu interface prepare
	*/		
#endif
	/* reset silence start = hw_ptr  */
	return snd_pcm_do_reset(runtime, 0);
}

static void snd_pcm_post_prepare(struct snd_pcm_runtime *runtime, int state)
{
	/* set appl_prt of write  equal the hw_ptr of read*/
	runtime->control->appl_ptr = runtime->status->hw_ptr;
	
	/* set prepared directly ? */
	runtime->status->state = SNDRV_PCM_STATE_PREPARED;
}

static struct action_ops snd_pcm_action_prepare = {
	.pre_action = snd_pcm_pre_prepare,
	.do_action = snd_pcm_do_prepare,
	.post_action = snd_pcm_post_prepare
};

/**
 * snd_pcm_prepare - prepare the PCM substream to be triggerable
 */
int snd_pcm_prepare(void * handle)
{
	int res;
	struct snd_pcm_runtime *runtime = (struct snd_pcm_runtime *)handle;
	// wait poweron ,clock on

	res = snd_pcm_action(&snd_pcm_action_prepare, runtime, 0);
	return res;
}

/**
* use to stop play 
*/
static int snd_pcm_drop(struct snd_pcm_runtime *runtime)
{
	int result = 0;
	
	if (runtime->status->state == SNDRV_PCM_STATE_OPEN ||
	    runtime->status->state == SNDRV_PCM_STATE_DISCONNECTED ||
	    runtime->status->state == SNDRV_PCM_STATE_SUSPENDED){
		pr_err(" Before snd_pcm_drop ,the state is error\n");
		return -1;
	}

	snd_pcm_stream_lock_irq(runtime);
	/* resume pause */
	//if (runtime->status->state == SNDRV_PCM_STATE_PAUSED)
 		//snd_pcm_pause(substream, 0); // runtime->hw_ptr_ticks = get_curr_ticks() - HZ * 1000;

	snd_pcm_stop(runtime, SNDRV_PCM_STATE_SETUP);
	/* runtime->control->appl_ptr = runtime->status->hw_ptr; */
	snd_pcm_stream_unlock_irq(runtime);

	return result;

}

/* start ,stop,reset ,prepare */
/*******************************************************************/

static void xrun(struct snd_pcm_runtime *runtime)
{
	//os_printf("XRUN\n");
#ifdef XRUN_AND_INTRLOST_DEBUG	
	pr_err("X\n");
#endif
	snd_pcm_stop(runtime, SNDRV_PCM_STATE_XRUN);
}

int snd_pcm_update_state(struct snd_pcm_runtime *runtime)
{
	snd_pcm_uframes_t avail;

	if (runtime->stream == SNDRV_PCM_STREAM_PLAYBACK)
		avail = snd_pcm_playback_avail(runtime);
	else
		avail = snd_pcm_capture_avail(runtime);

	if (avail > runtime->avail_max)
		runtime->avail_max = avail;
#if 0  /* don't support draining */
	if (runtime->status->state == SNDRV_PCM_STATE_DRAINING) {
		if (avail >= runtime->buffer_size) {
			snd_pcm_drain_done(runtime);
			return -1;
		}
	} else {
#endif
	if (avail >= runtime->stop_threshold) {
			os_printf("x%d\n",avail/*,runtime->stop_threshold*/);
			xrun(runtime);
			return -1;
	}
	return 0;
}

static int snd_pcm_update_hw_ptr0(struct snd_pcm_runtime *runtime,
				  unsigned int in_interrupt) /* flag indicate that called by dma complete intr */
{
	snd_pcm_uframes_t pos;
	snd_pcm_uframes_t old_hw_ptr, new_hw_ptr, hw_base;
	snd_pcm_sframes_t  hdelta, delta;
	unsigned long jdelta;

	old_hw_ptr = runtime->status->hw_ptr;

	pos = snd_pcm_pointer(runtime,in_interrupt); /* get hardware frame ptr => snd_imx_pcm_pointer */

	if (pos == /*-1*/SNDRV_PCM_POS_XRUN) {	/* indicate over or under run */
		return -1;
	}
	if (pos >= runtime->buffer_size) { /* current hardware pointer more than buffer size */
		os_printf("BUG: pos >= runtime->buffer_size, pos = %ld, "
				   "buffer size = %ld, period size = %ld\n",
				   pos, runtime->buffer_size,
				   runtime->period_size);
		pos = 0;
	}
	pos -= pos % runtime->min_align;

	hw_base = runtime->hw_ptr_base;
	new_hw_ptr = hw_base + pos;
	if (in_interrupt) {
		/* we know that one period was processed */
		/* delta = "expected next hw_ptr" for in_interrupt != 0 */
		delta = runtime->hw_ptr_interrupt + runtime->period_size;
		if (delta > new_hw_ptr) {
#ifdef CHECK_DOUBLE_ACKED_INTRS
			/* check for double acknowledged interrupts */
			hdelta = get_curr_ticks() - runtime->hw_ptr_ticks;
			if (hdelta > runtime->hw_ptr_buffer_ticks/2) {
				printf(" check for double acknowledged interrupts\n");
				hw_base += runtime->buffer_size;       /* reasign hw_base */
				if (hw_base >= runtime->boundary)
					hw_base = 0;
				new_hw_ptr = hw_base + pos;
				goto __delta;
			}
#else
			hw_base += runtime->buffer_size;
			if (hw_base >= runtime->boundary)
				hw_base = 0;
			new_hw_ptr = hw_base + pos;
			goto __delta;
			os_printf("\nd%d>%d\n",delta,new_hw_ptr);
#endif
		}
	}
	
	/* new_hw_ptr might be lower than old_hw_ptr in case when */
	/* pointer crosses the end of the ring buffer */
	if (new_hw_ptr < old_hw_ptr) {
		hw_base += runtime->buffer_size;
		if (hw_base >= runtime->boundary)
			hw_base = 0;
		new_hw_ptr = hw_base + pos;
	}
__delta:
	delta = new_hw_ptr - old_hw_ptr;
	if (delta < 0)
		delta += runtime->boundary;
	/* xrun debug message		
	snd_printd("_update: %s: pos=%u/%u/%u, "
		   "hwptr=%ld/%ld/%ld/%ld\n",
		   in_interrupt ? "period" : "hwptr",
		   (unsigned int)pos,
		   (unsigned int)runtime->period_size,
		   (unsigned int)runtime->buffer_size,
		   (unsigned long)delta,
		   (unsigned long)old_hw_ptr,
		   (unsigned long)new_hw_ptr,
		   (unsigned long)runtime->hw_ptr_base);
	*/

	//if (runtime->no_period_wakeup) {   
	//this is in not period interrupt condition,
	//(but period interrupt defined a interrupt occured when period data process completely )
	//}

	/* something must be really wrong */
	if (delta >= runtime->buffer_size + runtime->period_size) {
		printf("Unexpected hw_pointer value %s"
			       "( pos=%ld, new_hw_ptr=%ld, "
			       "old_hw_ptr=%ld)\n",
				     in_interrupt ? "[Q] " : "[P]",
				     (long)pos,
				     (long)new_hw_ptr, (long)old_hw_ptr);
		return 0;
	}
#if 0  /* don't support ticks check */
	/* Skip the ticks check for hardwares with BATCH flag.
	 * Such hardware usually just increases the position at each IRQ,
	 * thus it can't give any strange position.
	 */
	if (runtime->hw.info & SNDRV_PCM_INFO_BATCH)
		goto no_ticks_check;
	hdelta = delta;
	/* if (hdelta < runtime->delay)  // remove for no runtime delay (hdelta 's unit is frames)
		goto no_ticks_check; 
	hdelta -= runtime->delay; */
	jdelta = get_curr_ticks() - runtime->hw_ptr_ticks;
	if (((hdelta * HZ) / runtime->rate) > jdelta + HZ/100) {
		delta = jdelta /(((runtime->period_size * HZ) / runtime->rate) + HZ/100);
		/* move new_hw_ptr according ticks not pos variable */
		new_hw_ptr = old_hw_ptr;
		hw_base = delta;
		/* use loop to avoid checks for delta overflows */
		/* the delta value is small or zero in most cases */
		while (delta > 0) {
			new_hw_ptr += runtime->period_size;
			if (new_hw_ptr >= runtime->boundary)
				new_hw_ptr -= runtime->boundary;
			delta--;
		}
		/* align hw_base to buffer_size */
		/*
		pr_err(substream,
			     "hw_ptr skipping! %s"
			     "(pos=%ld, delta=%ld, period=%ld, "
			     "jdelta=%lu/%lu/%lu, hw_ptr=%ld/%ld)\n",
			     in_interrupt ? "[Q] " : "",
			     (long)pos, (long)hdelta,
			     (long)runtime->period_size, jdelta,
			     ((hdelta * HZ) / runtime->rate), hw_base,
			     (unsigned long)old_hw_ptr,
			     (unsigned long)new_hw_ptr); */
		/* reset values to proper state */
		delta = 0;
		hw_base = new_hw_ptr - (new_hw_ptr % runtime->buffer_size);
	}
#endif

 no_ticks_check:
	if (delta > runtime->period_size + runtime->period_size / 2) {
		/* printf("Lost interrupts? %s"
			     "(stream=%d, delta=%ld, new_hw_ptr=%ld, "
			     "old_hw_ptr=%ld)\n",
			     in_interrupt ? "[Q] " : "",
			     runtime->stream, (long)delta,
			     (long)new_hw_ptr,
			     (long)old_hw_ptr);*/
                //printf("Lost interrupts?\n");
#ifdef XRUN_AND_INTRLOST_DEBUG                
                pr_err("L\n");
#endif
	}

 no_delta_check:
	if (runtime->status->hw_ptr == new_hw_ptr)
		return 0;

	if (runtime->stream == SNDRV_PCM_STREAM_PLAYBACK
	    && runtime->silence_size > 0)
			snd_pcm_playback_silence(runtime, new_hw_ptr);

	if (in_interrupt) {
		delta = new_hw_ptr - runtime->hw_ptr_interrupt;
		if (delta < 0)
			delta += runtime->boundary;
		delta -= (snd_pcm_uframes_t)delta % runtime->period_size;
		runtime->hw_ptr_interrupt += delta;
		if (runtime->hw_ptr_interrupt >= runtime->boundary)
			runtime->hw_ptr_interrupt -= runtime->boundary;
	}
	runtime->hw_ptr_base = hw_base;
	runtime->status->hw_ptr = new_hw_ptr;
#ifdef CHECK_DOUBLE_ACKED_INTRS	
	runtime->hw_ptr_ticks = get_curr_ticks();
#endif

	return snd_pcm_update_state( runtime);
	//return 0;
}

/* pcm misc */
int pcm_format_width(snd_pcm_format_t format)
{
	int val;
	if ((INT)format < 0 || (INT)format > (INT)SNDRV_PCM_FORMAT_LAST)
		return -1;
	if ((val = pcm_formats[(INT)format].width) == 0)
		return -1;
	return val;
}

int pcm_format_physical_width(snd_pcm_format_t format)
{
	int val;
	if ((INT)format < 0 || (INT)format > (INT)SNDRV_PCM_FORMAT_LAST)
		return -1;
	if ((val = pcm_formats[(INT)format].phys) == 0)
		return -1;
	return val;
}

int pcm_format_little_endian(snd_pcm_format_t format)
{
	int val;
	if ((INT)format < 0 || (INT)format > (INT)SNDRV_PCM_FORMAT_LAST)
		return -1;
	if ((val = pcm_formats[(INT)format].le) < 0)
		return -1;
	return val;
}

/* CAUTION: call it with irq disabled */
int snd_pcm_update_hw_ptr(struct snd_pcm_runtime *runtime)
{
	return snd_pcm_update_hw_ptr0(runtime, 0);
}

/* called by period interrupt */
static void snd_pcm_period_elapsed(struct snd_pcm_runtime *runtime)
{
	unsigned long flags;

	snd_pcm_stream_lock_irqsave(runtime, flags);
	if (!snd_pcm_running(runtime) ||
	    snd_pcm_update_hw_ptr0(runtime, 1) < 0)   /* update hw ptr at dma buffer  ! */
		goto _end;

 _end:
	snd_pcm_stream_unlock_irqrestore(runtime, flags);
	/* after transfer callback ack end
	if (transfer_ack_end)
		transfer_ack_end(substream); */
	return;
}

/* write call back */
static int snd_pcm_write_transfer(struct snd_pcm_runtime *runtime,
				      unsigned int hwoff,
				      unsigned long data, unsigned int off,
				      snd_pcm_uframes_t frames)
{
	int err;
	/* the offset of playback write data */
	char  *buf = (char  *) data + frames_to_bytes(runtime, off);  
#if 0
	if (pcm_stream_copy) {
		if ((err = pcm_stream_copy(runtime, -1, hwoff, buf, frames)) < 0)
			return err;
	} else {
		/* copy to the place of dma_area hwbuf offset */
		char *hwbuf = runtime->dma_area + frames_to_bytes(runtime, hwoff);
		os_memcpy(hwbuf, buf, frames_to_bytes(runtime, frames));
	}
#else
	sco_pcm_stream_copy(runtime, -1, hwoff, buf, frames);
#endif
	return 0;
}

/*
 * Wait until avail_min data becomes available
 * Returns a negative error code if any error occurs during operation.
 * The available space is stored on availp.  When err = 0 and avail = 0
 * on the capture stream, it indicates the stream is in DRAINING state.
 */
static int wait_for_avail(struct snd_pcm_runtime *runtime,
			snd_pcm_uframes_t *availp)
{
	int is_playback = (runtime->stream == SNDRV_PCM_STREAM_PLAYBACK);
	/* wait_queue_t wait; */
	int err = 0;
	snd_pcm_uframes_t avail = 0;
	long wait_time/*, tout*/;

#if 0
	if (runtime->no_period_wakeup)
		wait_time = MAX_SCHEDULE_TIMEOUT;
	else {
		wait_time = 10;
		if (runtime->rate) {
			long t = runtime->period_size * 2 / runtime->rate;
			wait_time = max(t, wait_time);
		}
	}
#else
	struct sco_pcm_runtime_data *iprtd = (struct sco_pcm_runtime_data *)runtime->private_data;

	wait_time =  10;
	if (runtime->rate) {
		long t = /* runtime->period_size * 2 / runtime->rate */iprtd->period_time;
		wait_time = max(t, wait_time); /* use ms delay */
	}
#endif

	for (;;) {
		/*
		 * We need to check if space became available already
		 * (and thus the wakeup happened already) first to close
		 * the race of space already having become available.
		 * This check must happen after been added to the waitqueue
		 * and having current state be INTERRUPTIBLE.
		 */
		if (is_playback)
			avail = snd_pcm_playback_avail(runtime);
		else
			avail = snd_pcm_capture_avail(runtime);
		
		if (avail >= Wait_avail_min)
			break;
		snd_pcm_stream_unlock_irq(runtime);
		
		/* or use wait availd event (transfer_ack_end)*/
		os_msleep(wait_time);

		snd_pcm_stream_lock_irq(runtime);
	}
	*availp = avail;
	err = -(avail == 0);
	return err;
}

snd_pcm_sframes_t snd_pcm_lib_write(void * handle,
			unsigned long data, snd_pcm_uframes_t size ,int nonblock)
{
	snd_pcm_uframes_t xfer = 0;
	snd_pcm_uframes_t offset = 0;
	int err = 0;

	struct snd_pcm_runtime *runtime = (struct snd_pcm_runtime *)handle;
		
	snd_pcm_stream_lock_irq(runtime);

#if 1  /* open -> prepared -> running */
	switch (runtime->status->state) {
	case SNDRV_PCM_STATE_PREPARED:
	case SNDRV_PCM_STATE_RUNNING:
	case SNDRV_PCM_STATE_PAUSED:
		break;
	case SNDRV_PCM_STATE_XRUN:
		err = -1;
		goto _end_unlock;
	case SNDRV_PCM_STATE_SUSPENDED:
		err = -1;
		goto _end_unlock;
	default:
		err = -1;
		goto _end_unlock;
	}
#endif

	while (size > 0) {  /* while until size == 0 */
		snd_pcm_uframes_t  frames, appl_ptr, appl_ofs;
		snd_pcm_uframes_t  avail;
		snd_pcm_uframes_t  cont;
		if (runtime->status->state == SNDRV_PCM_STATE_RUNNING)
			snd_pcm_update_hw_ptr(runtime);
		avail = snd_pcm_playback_avail(runtime);
		if (!avail) {
			if (nonblock) {
				err = -1;
				goto _end_unlock;
			}
			/*
			runtime->twake = min_t(snd_pcm_uframes_t, size,
					runtime->control->avail_min ? : 1); */
			err = wait_for_avail(runtime, &avail);   /* wait for peroid consuming time to check has avild space ? */
			if (err < 0)
				goto _end_unlock;
		}
		
		frames = size > avail ? avail : size;   /* min of the size and  availd*/
		cont = runtime->buffer_size - runtime->control->appl_ptr % runtime->buffer_size;
		if (frames > cont) /* where space at the both of end and first of the dma buffer , process the end space filling firstly */
			frames = cont;

		if (snd_BUG_ON(!frames)) {
			//runtime->twake = 0;
			snd_pcm_stream_unlock_irq(runtime);
			return -1;
		}

		appl_ptr = runtime->control->appl_ptr;
		appl_ofs = appl_ptr % runtime->buffer_size;  /*  attention the mod! it caulate the indeed writing pointer  */
		snd_pcm_stream_unlock_irq(runtime);
		err = snd_pcm_write_transfer(runtime, appl_ofs, data, offset, frames);  /* snd_pcm_lib_write_transfer(...) */
		
		snd_pcm_stream_lock_irq(runtime);
		if (err < 0)
			goto _end_unlock;

		appl_ptr += frames;
		if (appl_ptr >= runtime->boundary)
			appl_ptr -= runtime->boundary;        /* update appl_ptr (+frames)*/
		runtime->control->appl_ptr = appl_ptr;
		// ack();

		offset += frames;
		size -= frames;
		xfer += frames;
		if (runtime->status->state == SNDRV_PCM_STATE_PREPARED &&
		    snd_pcm_playback_hw_avail(runtime) >= (snd_pcm_sframes_t)runtime->start_threshold) {
			err = snd_pcm_start(runtime);   /* start dma */
			if (err < 0)
				goto _end_unlock;
		}
	}
_end_unlock:

	if (xfer > 0 && err >= 0){
		snd_pcm_update_state(runtime);
	}
	snd_pcm_stream_unlock_irq(runtime);
	return xfer > 0 ? (snd_pcm_sframes_t)xfer : err;
}


size_t u_audio_playback(void * handle, void *buf, size_t count)
{
	ssize_t result;
	snd_pcm_sframes_t frames;

	struct snd_pcm_runtime *runtime = (struct snd_pcm_runtime *)handle;

try_again:
      if(!runtime)
        return 0;
	if (runtime->status->state == SNDRV_PCM_STATE_XRUN ||
		runtime->status->state == SNDRV_PCM_STATE_SUSPENDED) {
		result = snd_pcm_prepare(runtime);
		if (result < 0) {
			printf("Preparing sound card failed: %d\n", (int)result);
			return result;
		}
	}

	frames = bytes_to_frames(runtime, count);
	result =  snd_pcm_lib_write(runtime,(unsigned long)buf, frames , NON_BLOCK /*RW_BLOCK*/);  
	if (result != frames) {
		if ( runtime->status->state != SNDRV_PCM_STATE_XRUN ) {
			//count -= frames_to_bytes(runtime,result);
			os_msleep((HZ * runtime->period_size + (runtime->rate -1)) /runtime->rate);  /* or sleep 10ms */
			//printf("Playback %d error: %d\n", frames, (int)result);
#ifdef XRUN_AND_INTRLOST_DEBUG			
			printf("%d:%d\n", frames, (int)result);
#endif			
			return 0;
		}
		goto try_again;
	}

	return 0;
}

size_t file_playback(void * handle, void *buf, size_t count)
{
	ssize_t result;
	snd_pcm_sframes_t frames;

	struct snd_pcm_runtime *runtime = (struct snd_pcm_runtime *)handle;

try_again:
      if(!runtime)
        return 0;
	if (runtime->status->state == SNDRV_PCM_STATE_XRUN ||
		runtime->status->state == SNDRV_PCM_STATE_SUSPENDED) {
		result = snd_pcm_prepare(runtime);
		if (result < 0) {
			printf("Preparing sound card failed: %d\n", (int)result);
			return result;
		}
	}

	frames = bytes_to_frames(runtime, count);
	result =  snd_pcm_lib_write(runtime,(unsigned long)buf, frames ,/*NON_BLOCK*/RW_BLOCK);
	if (result != frames) {
		//_time_delay(2);
		//os_msleep(10);
		os_msleep(/* 10 */(HZ * runtime->period_size + (runtime->rate -1)) /runtime->rate);
		printf("Playback error: %d\n", (int)result);
		goto try_again;
	}

	return 0;
}

size_t sync_capture(void * handle, void *buf, size_t count)
{
	ssize_t result;
	snd_pcm_sframes_t frames;

	struct snd_pcm_runtime *runtime = (struct snd_pcm_runtime *)handle;

try_again:
      if(!runtime)
        return 0;
	if (runtime->status->state == SNDRV_PCM_STATE_XRUN ||
		runtime->status->state == SNDRV_PCM_STATE_SUSPENDED) {
		result = snd_pcm_prepare(runtime);
		if (result < 0) {
			printf("Preparing sound card failed: %d\n", (int)result);
			return result;
		}
	}

	frames = bytes_to_frames(runtime, count);
	result =  snd_pcm_lib_read(runtime,(unsigned long)buf, frames ,/*NON_BLOCK*/RW_BLOCK);
	if (result != frames) {
		//_time_delay(2);
		//os_msleep(10);
		os_msleep(/* 10 */(HZ * runtime->period_size + (runtime->rate -1)) /runtime->rate);
		printf("scapture error: %d\n", (int)result);
		goto try_again;
	}

	return 0 /*result*/;
}

static int snd_pcm_read_transfer(struct snd_pcm_runtime *runtime,
				     unsigned int hwoff,
				     unsigned long data, unsigned int off,
				     snd_pcm_uframes_t frames)
{
	int err;
	char *buf = (char *) data + frames_to_bytes(runtime, off);
#if 0
	if (pcm_copy) {
		if ((err = pcm_copy(runtime, -1, hwoff, buf, frames)) < 0)
			return err;
	} else {
		char *hwbuf = runtime->dma_area + frames_to_bytes(runtime, hwoff);
		os_memcpy(buf, hwbuf, frames_to_bytes(runtime, frames));
	}
#else
	sco_pcm_stream_copy(runtime, -1, hwoff, buf, frames);
	return 0;
#endif
}

/* capture */
snd_pcm_sframes_t snd_pcm_lib_read(void * handle,
					   unsigned long data, snd_pcm_uframes_t size , int nonblock)
{
	snd_pcm_uframes_t xfer = 0;
	snd_pcm_uframes_t offset = 0;
	int err = 0;

	struct snd_pcm_runtime *runtime = (struct snd_pcm_runtime *)handle;
		
	if (size == 0)
		return 0;

	snd_pcm_stream_lock_irq(runtime);

	switch (runtime->status->state) {
	case SNDRV_PCM_STATE_PREPARED:
		if (size >= runtime->start_threshold) {
			err = snd_pcm_start(runtime);
			if (err < 0)
				goto _end_unlock;
		}
		break;
	case SNDRV_PCM_STATE_DRAINING:
	case SNDRV_PCM_STATE_RUNNING:
	case SNDRV_PCM_STATE_PAUSED:
		break;
	case SNDRV_PCM_STATE_XRUN:
		err = -1;
		goto _end_unlock;
	case SNDRV_PCM_STATE_SUSPENDED:
		err = -1;
		goto _end_unlock;
	default:
		err = -1;
		goto _end_unlock;
	}

	while (size > 0) {
		snd_pcm_uframes_t frames, appl_ptr, appl_ofs;
		snd_pcm_uframes_t avail;
		snd_pcm_uframes_t cont;
		if (runtime->status->state == SNDRV_PCM_STATE_RUNNING)
			snd_pcm_update_hw_ptr(runtime);
		avail = snd_pcm_capture_avail(runtime);
		if (!avail) {
#if 0			
			if (runtime->status->state ==
			    SNDRV_PCM_STATE_DRAINING) {
				snd_pcm_stop(runtime, SNDRV_PCM_STATE_SETUP);
				goto _end_unlock;
			}
#endif			
#if 1			
			if (nonblock) {
				err = -1;
				goto _end_unlock;
			}
			/*
			runtime->twake = min_t(snd_pcm_uframes_t, size,
			runtime->control->avail_min ? : 1); */
			err = wait_for_avail(runtime, &avail);
			if (err < 0)
				goto _end_unlock;
			if (!avail)
				continue; /* draining */
#endif
		}
		frames = size > avail ? avail : size;
		cont = runtime->buffer_size - runtime->control->appl_ptr % runtime->buffer_size;
		if (frames > cont)
			frames = cont;
		if (snd_BUG_ON(!frames)) {
			//runtime->twake = 0;
			snd_pcm_stream_unlock_irq(runtime);
			return -1;
		}
		appl_ptr = runtime->control->appl_ptr;
		appl_ofs = appl_ptr % runtime->buffer_size;
		snd_pcm_stream_unlock_irq(runtime);
		err = /* transfer*/ snd_pcm_read_transfer(runtime, appl_ofs, data, offset, frames);
		snd_pcm_stream_lock_irq(runtime);
		if (err < 0)
			goto _end_unlock;

		switch (runtime->status->state) {
		case SNDRV_PCM_STATE_XRUN:
			err = -1;
			goto _end_unlock;
		case SNDRV_PCM_STATE_SUSPENDED:
			err = -1;
			goto _end_unlock;
		default:
			break;
		}

		appl_ptr += frames;
		if (appl_ptr >= runtime->boundary)
			appl_ptr -= runtime->boundary;
		runtime->control->appl_ptr = appl_ptr;
		// ack(runtime);

		offset += frames;
		size -= frames;
		xfer += frames;
	}
 _end_unlock:

	if (xfer > 0 && err >= 0)
		snd_pcm_update_state(runtime);
	snd_pcm_stream_unlock_irq(runtime);
	return xfer > 0 ? (snd_pcm_sframes_t)xfer : err;
}

 int snd_pcm_params(void * handle,
			struct snd_pcm_params *params)
{
	int err, usecs;
	unsigned int bits;
	snd_pcm_uframes_t frames;
	struct snd_pcm_runtime *runtime = (struct snd_pcm_runtime *)handle;

	/*if (PCM_RUNTIME_CHECK(runtime))
		return -1;*/
	snd_pcm_stream_lock_irq(runtime);
	switch (runtime->status->state) {
	case SNDRV_PCM_STATE_OPEN:
	case SNDRV_PCM_STATE_SETUP:
	case SNDRV_PCM_STATE_PREPARED:
		break;   /* set params only at the state of open,setup and prepared ! */
	default:
		snd_pcm_stream_unlock_irq(runtime);
		return -1;
	}
	snd_pcm_stream_unlock_irq(runtime);

	/* Default  params */
	/* SNDRV_PCM_ACCESS_INTERLEAVED; 
	    SNDRV_PCM_ACCESS_NONINTERLEAVED 
	    for two channels ,one channel SNDRV_PCM_ACCESS_INTERLEAVED */
	runtime->access = params->access; 

	runtime->format = params->format;
	runtime->channels  = params->channels;

	/* lookup sample bits from runtime format */
	bits = pcm_format_physical_width (runtime->format);
	runtime->sample_bits = bits;
	bits *= runtime->channels;
	runtime->frame_bits = bits;

	runtime->sample_aligned_bits = runtime->sample_bits;  /* hw buf ,sample aligned bits */
	runtime->sample_phy_bits = pcm_format_physical_width(runtime->format);

	frames = 1;
	while (bits % 8 != 0) {
		bits *= 2;
		frames *= 2;
	}
	/* runtime->byte_align = bits / 8; */
	runtime->min_align = frames;

	runtime->rate  = params->rate;

	/* must set before soc codec paramaters setting ! */
	runtime->period_size = params->period_size;

	err = sco_pcm_params(runtime,params);

	runtime->dma_bytes =  frames_to_bytes(runtime,runtime->buffer_size) 
		                          * runtime->sample_aligned_bits / runtime->sample_phy_bits;

	return err;
}

int pcm_stream_init(void ** handle , unsigned int direction,struct snd_pcm_params *params)
{
	/* static int sco_pcm_open(struct snd_pcm_substream *substream) platform ,sco,cpu dai init startup */
	int ret = 0;
	unsigned int bits;
	snd_pcm_uframes_t frames;
	//struct sco_pcm_runtime_data *iprtd = NULL;
	struct snd_pcm_runtime **runtime = (struct snd_pcm_runtime **)handle;
	static int sys_inited = 0;

	if(!sys_inited) {
		HZ = get_hz();
		sys_inited = 1;
		os_printf("system HZ is %d\n",HZ);
	}

	ret = snd_pcm_attach_runtime(runtime);
	if(ret)
		return ret;

	(*runtime)->stream = direction;

	//iprtd =  (struct sco_pcm_runtime_data *)(*runtime)->private_data;

	ret = sco_pcm_open(*runtime);
	if(ret)
		return ret;

	if(params) {
		ret = snd_pcm_params((void *)(*runtime),params);
		if(ret == 0)
			snd_pcm_prepare((void *)(*runtime));
	}	
		
	return ret;
}

void pcm_stream_deinit(void * handle)
{
	struct snd_pcm_runtime *runtime = (struct snd_pcm_runtime *)handle;
	//printf("pcm_stream_deinit+\n");
	if (PCM_RUNTIME_CHECK(runtime))
		return ;

	snd_pcm_drop(runtime);
	
	sco_pcm_close(runtime);

	//edma_free_chan(SSI_TX_DMA_CHN);

	snd_pcm_detach_runtime(runtime);
	handle = NULL;
	//printf("pcm_stream_deinit-\n");
}

/* This is first executed function when use this pcm lib , then  playback_pcm_runtime_init */
static int snd_pcm_attach_runtime(struct snd_pcm_runtime **runtime)
{
	size_t size;
	int extra_size = sizeof(struct sco_pcm_runtime_data);

	struct snd_pcm_runtime * pt ;

	*runtime = os_zalloc(sizeof(struct snd_pcm_runtime) + extra_size);
	if (*runtime == NULL)
		return -1;
	//printf("%d\n",sizeof(struct snd_pcm_runtime));
	os_memset((void *)(*runtime),0, sizeof(struct snd_pcm_runtime) + extra_size);

	pt = *runtime;
	
	size = sizeof(struct snd_pcm_rt_status);
	/* alloc and zero the snd_pcm_mmap_status  struct */
	pt->status = os_zalloc(size);
	if (pt->status == NULL) {
		PCM_mem_free(pt);
		return -1;
	}
	
        os_memset((void*)(pt->status), 0, sizeof(struct snd_pcm_rt_status));

	size = sizeof(struct snd_pcm_rt_control);
	pt->control = os_zalloc(size);
	if (pt->control == NULL) {
		PCM_mem_free((void*)pt->status);
		PCM_mem_free(pt);
		return -1;
	}
	os_memset((void*)(pt->control), 0, sizeof(struct snd_pcm_rt_control));

	pt->private_data = (char *)pt + sizeof(struct snd_pcm_runtime);

	/* first status */
	pt->status->state = SNDRV_PCM_STATE_OPEN;   

	return 0;
}

void snd_pcm_detach_runtime(struct snd_pcm_runtime *runtime)
{
	PCM_mem_free((void*)runtime->status);
	PCM_mem_free((void*)runtime->control);

	PCM_mem_free(runtime);
}

int pcm_stream_rate(void * handle)
{
	struct snd_pcm_runtime *runtime = (struct snd_pcm_runtime *)handle;
	unsigned int curr_ticks;
	// check handle not null
	if (PCM_RUNTIME_CHECK(runtime))
		return -1;

	if(snd_pcm_running(runtime)){
	 	//printf("%d\n",runtime->hw_ptr_ticks_inteval);
	 	return runtime->period_size * 1000 *100 / runtime->hw_ptr_ticks_inteval;
     	}
	else
		return 0;
}

int snd_pcm_get_avail_rate(void * handle)
{
	struct snd_pcm_runtime *runtime = (struct snd_pcm_runtime *)handle;
	int rate = 0;

	if(!snd_pcm_running(runtime))
           return -1;
	snd_pcm_stream_lock_irq(runtime);
	if (runtime->stream == SNDRV_PCM_STREAM_PLAYBACK)
		rate =  snd_pcm_playback_avail(runtime) * 100 / runtime->buffer_size;
	else
		rate =  snd_pcm_capture_avail(runtime)  * 100 / runtime->buffer_size;
	snd_pcm_stream_unlock_irq(runtime);
	return rate;
}

int snd_pcm_vol_ctrl(void * handle,int updown) /* 1 up , 0 down */
{
	if(updown)
		return sco_pcm_vol_up();
	else
		return sco_pcm_vol_down();
}


