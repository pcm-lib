
#include <mqx.h>
#include <bsp.h>
#include <i2c.h>

#include "audio_dev.h"

MQX_FILE_PTR fd = NULL;
#define I2C_DEVICE_POLLED "i2c0:"

#define SGTL5000_I2C_ADDR 0x0A

/*FUNCTION****************************************************************
* 
* Function Name    : sgtl_WriteReg
* Returned Value   : MQX error code
* Comments         :
*    Writes a value to the entire register. All
*    bit-fields of the register will be written.
*
*END*********************************************************************/
_mqx_int I2C_Init(void)
{
	uint_32 param;
	I2C_STATISTICS_STRUCT stats;

	if (fd == NULL)
	{
	   fd = fopen (I2C_DEVICE_POLLED, NULL);
	}
	if (fd == NULL) 
	{
		printf ("ERROR: Unable to open I2C driver!\n");
		return(-9);
	}
	param = 100000;
	if (I2C_OK != ioctl (fd, IO_IOCTL_I2C_SET_BAUD, &param))
	{
		return(-1);
	}
	if (I2C_OK != ioctl (fd, IO_IOCTL_I2C_SET_MASTER_MODE, NULL))
	{
		return(-2);
	}
	if (I2C_OK != ioctl (fd, IO_IOCTL_I2C_CLEAR_STATISTICS, NULL))
	{
		return(-3);
	}
	param = SGTL5000_I2C_ADDR;
	if (I2C_OK != ioctl (fd, IO_IOCTL_I2C_SET_DESTINATION_ADDRESS, &param))
	{
		return(-4);
	}
	
	/* Initiate start and send I2C bus address */
	param = fwrite (&param, 1, 0, fd);
	
	if (I2C_OK != ioctl (fd, IO_IOCTL_I2C_GET_STATISTICS, (pointer)&stats))
	{
		return(-5);
	}
   /* Stop I2C transfer */
 	if (I2C_OK != ioctl (fd, IO_IOCTL_I2C_STOP, NULL))
 	{
 		return(-6);
 	}
	/* Check ack (device exists) */
	if (I2C_OK == ioctl (fd, IO_IOCTL_FLUSH_OUTPUT, &param))
	{
	    if ((param) || (stats.TX_NAKS)) 
		{
	    	return(-7);
		}
	}
	else
	{
		return(-8);
	}
	return(MQX_OK);
}

/*FUNCTION****************************************************************
* 
* Function Name    : sgtl_WriteReg
* Returned Value   : MQX error code
* Comments         :
*    Writes a value to the entire register. All
*    bit-fields of the register will be written.
*
*END*********************************************************************/
_mqx_int sgtl_WriteReg(uint_16 reg, uint_16 reg_val)
{
	uint_8 buffer[4];
	uint_32 result;
	buffer[0] = (uint_8)((reg >> 8) & 0xFF);
	buffer[1] =	(uint_8)(reg & 0xFF);
	buffer[2] =	(uint_8)((reg_val >> 8) & 0xFF);
	buffer[3] =	(uint_8)(reg_val & 0xFF);
	result = write(fd, buffer, 4); 
	if (4 != result)
	{
		#ifdef SGTL5000_DEBUG	
		 printf("sgtl_WriteReg: Error - write to address ");
		 printf("0x%04X failed.\n", reg);
		#endif
		return(-1);
	} 
	result = fflush (fd);
	if (MQX_OK != result)
	{
		return(-3);
	} 
	/* Stop I2C transfer */
	if (I2C_OK != ioctl (fd, IO_IOCTL_I2C_STOP, NULL))
	{
		return(-2);
	}
	result = 0;
	return(MQX_OK);
}

/*FUNCTION****************************************************************
* 
* Function Name    : sgtl_WriteReg
* Returned Value   : MQX error code
* Comments         :
*    Reads value of register. 
*
*END*********************************************************************/
_mqx_int sgtl_ReadReg(uint_16 reg, uint_16_ptr dest_ptr)
{
	uint_8 buffer[2];
	uint_32 result, param;
	buffer[0] = (uint_8)((reg >> 8) & 0xFF);
	buffer[1] =	(uint_8)(reg & 0xFF);
	result = write(fd, buffer, 2);
	if (2 != result)
	{
		#ifdef SGTL5000_DEBUG	
		 printf("sgtl_ReadReg: Error - SGTL not responding.\n");
		#endif
		return(-1);
	}
	result = fflush (fd);
	if (MQX_OK != result)
	{
		return(-6);
	} 
	/* Send repeated start */
	if (I2C_OK != ioctl (fd, IO_IOCTL_I2C_REPEATED_START, NULL))
	{
		#ifdef SGTL5000_DEBUG	
		 printf("sgtl_ReadReg: Error - unable to send repeated start.\n");
		#endif
		return(-2);
	}
	/* Set read request */
	param = 2;
	if (I2C_OK != ioctl (fd, IO_IOCTL_I2C_SET_RX_REQUEST, &param))
	{
		#ifdef SGTL5000_DEBUG	
		 printf("sgtl_ReadReg: Error - unable to set number of bytes requested.\n");
		#endif
		return(-3);
	}
	result = 0;
	/* Read all data */ 
	result = read (fd, buffer, 2);		
	if (2 != result)
	{
		#ifdef SGTL5000_DEBUG	
		 printf("sgtl_ReadReg: Error - SGTL not responding.\n");
		#endif
		return(-4);
	}
	result = fflush (fd);
	if (MQX_OK != result)
	{
		return(-7);
	} 
	*dest_ptr = (buffer[1] & 0xFFFF) | ((buffer[0] & 0xFFFF) << 8);
	/* Stop I2C transfer */
	if (I2C_OK != ioctl (fd, IO_IOCTL_I2C_STOP, NULL))
	{
		return(-5);
	}
	return (MQX_OK);
}

/*FUNCTION****************************************************************
* 
* Function Name    : sgtl_ModifyReg
* Returned Value   : MQX error code
* Comments         :
*    Modifies value of register. Bits to set to zero are defined by first 
*	 mask, bits to be set to one are defined by second mask.
*
*END*********************************************************************/
_mqx_int sgtl_ModifyReg(uint_16 reg, uint_16 clr_mask, uint_16 set_mask)
{
	uint_16 reg_val = 0;
	if (MQX_OK != sgtl_ReadReg(reg, &reg_val))
	{
		#ifdef SGTL5000_DEBUG	
		 printf("sgtl_ModifyReg: Error - cannot read from SGTL.\n");
		#endif
		return(-1);
	}
	reg_val &= clr_mask;
	reg_val |= set_mask;
	if (MQX_OK != sgtl_WriteReg(reg, reg_val))
	{
		#ifdef SGTL5000_DEBUG
		 printf("sgtl_ModifyReg: Error - cannot write to SGTL.\n");
		#endif
		return(-2);
	}
	return(MQX_OK);	
}
