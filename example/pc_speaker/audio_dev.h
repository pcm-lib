
#ifndef _audio_dev_h_
#define _audio_dev_h_	1

#include "usb_descriptor.h"

/* configure */
#define OTG_DEV_TASK_POLLING

/******************************************************************************
 * Constants - None
 *****************************************************************************/

/******************************************************************************
 * Macro's
 *****************************************************************************/
#define DATA_BUFF_SIZE                     ((AUDIO_ENDPOINT_PACKET_SIZE) * 1)
/*
#define AUDIO_FORMAT_SAMPLE_RATE           (16000)
#define AUDIO_FORMAT_SAMPLE_RATE           (32000)
#define AUDIO_FORMAT_SAMPLE_RATE           (44100)
#define AUDIO_FORMAT_SAMPLE_RATE           (48000) */
#define AUDIO_FORMAT_SAMPLE_RATE           (96000)
#define AUDIO_I2S_FS_FREQ_DIV              (256)
/* (384) */

#define USBD_APP_TASK_DEINIT_EVENT_MASK     (0x01 << 0)
#define USBD_APP_BUFFER0_FULL_EVENT_MASK    (0x01 << 1)
#define USBD_APP_BUFFER1_FULL_EVENT_MASK    (0x01 << 2)
#define USBD_APP_PCM_STOP_EVENT_MASK        (0x01 << 3)            /* interface 0 */
#define USBD_APP_PCM_CHANGE_EVENT_MASK      (0x01 << 4)

#define USBD_APP_ALL_EVENTS_MASK  (USBD_APP_TASK_DEINIT_EVENT_MASK | USBD_APP_BUFFER0_FULL_EVENT_MASK | USBD_APP_BUFFER1_FULL_EVENT_MASK  \
                                  | USBD_APP_PCM_STOP_EVENT_MASK | USBD_APP_PCM_CHANGE_EVENT_MASK)

#define USB_AUDIO_ASYNC_USAGE    (0)

#if USB_AUDIO_ASYNC_USAGE == 1
#define USB_ASYNC
#endif

// usb dev play task parameters
#define USB_DEV_TASK_TEMPLATE_INDEX       0
#define USB_DEV_TASK_ADDRESS              usb_audio_dev_task
#define USB_DEV_TASK_STACKSIZE            1024
#define USB_DEV_TASK_NAME                 "usbd Task1"
#define USB_DEV_TASK_ATTRIBUTES           0
#define USB_DEV_TASK_CREATION_PARAMETER   0
#define USB_DEV_TASK_DEFAULT_TIME_SLICE   0
#define USBCFG_DEV_TASK_PRIORITY          7  /* priority must less than khci task !*/

#define USB_DEV_TASK_STATE_STOP           0
#define USB_DEV_TASK_STATE_READY          1


/*****************************************************************************
 * Types
 *****************************************************************************/
struct  _usb_features_
{
	uint_8 copy_protect;
	uint_8 cur_mute;
	uint_8 cur_volume[2];

	uint_8 min_volume[2];
	uint_8 max_volume[2];
	
	uint_8 res_volume[2];
	uint_8 cur_bass;
	uint_8 min_bass;
	
	uint_8 max_bass;
	uint_8 res_bass;
	uint_8 cur_mid;
	uint_8 min_mid;
	
	uint_8 max_mid;
	uint_8 res_mid;

	uint_8 cur_treble;
	uint_8 min_treble;
	uint_8 max_treble;
	uint_8 res_treble;

	uint_8 cur_graphic_equalizer[4];
	uint_8 min_graphic_equalizer[4];
	uint_8 max_graphic_equalizer[4];
	uint_8 res_graphic_equalizer[4];

	/* uint_8 cur_automatic_gain; */

	uint_8 cur_delay[2];
	uint_8 min_delay[2];
	uint_8 max_delay[2];
	uint_8 res_delay[2];

	uint_8 cur_bass_boost;

	uint_8 cur_loudness;

	uint_8 cur_automatic_gain;

	uint_8 cur_pitch;

	uint_8 cur_sampling_frequency[3];
	uint_8 min_sampling_frequency[3];
	uint_8 max_sampling_frequency[3];
	uint_8 res_sampling_frequency[3];
};

struct _usb_audio_dev_info_
{
	AUDIO_HANDLE   /* g_app_handle */app_handle;
	LWEVENT_STRUCT    app_event;

	/* current setting */
	uint_32			freq;      /* init to 0 */
	uint_8 alternate_interface[USB_MAX_SUPPORTED_INTERFACES];
	uint_8 cur_inf;      /* current interface num ,always 1 */
	uint_8  /*NewResFlag*/bit_resolution;
	
	uint_8  /*bSample_size*/subframe_size;

	uint_8 /* gu8NewConnection */state_connection;

#ifdef USB_ASYNC
	uint_8 feedback[3];
#endif
	/* allocate dynamic */
	uint_8 * adata_buff0;
	uint_8 *  cur_buff;

	uint_16 adata_size;     /* actual reveived data size */
	uint_16 usbd_task_state;

	struct _usb_features_ * usb_features;

	_task_id usbd_task_id;

#ifdef USB_ASYNC
	/* uint_8 gu8ResendFbk = 0; */
#endif
	void * pcm_handle;
};

extern struct _usb_audio_dev_info_  * audio_dev_info;

/*****************************************************************************
 * Global variables
 *****************************************************************************/


/*****************************************************************************
 * Global Functions
 *****************************************************************************/
extern uint_32 PeripheralInit(void);
extern void PeripheralUninit(void);
void usb_audio_dev_task(uint_32 param);

#endif 

/* EOF */
