
 
/******************************************************************************
 * Includes
 *****************************************************************************/
#include <mqx.h>
#include <lwevent.h>
#include <bsp.h>
#include <usb.h>
#include "audio_dev.h"
#include "io_gpio.h"

#include "i2c_kinets.h"

#include "sound_api.h"

/* dummy test
#define snd_init()
#define snd_umute()
#define snd_set_format(a,b,c)
#define snd_mute()
#define snd_deinit()
#define snd_write(a,b)
#define snd_read(a,b)
*/
void Main_Task ( uint_32 param );

TASK_TEMPLATE_STRUCT  MQX_template_list[] =
{
   { 10L, Main_Task, 2000L, 7L, "Main Task", MQX_AUTO_START_TASK, 0, 0},
    { 11L, usb_audio_dev_task,  1000L, 7L, "Play Task", MQX_AUTO_START_TASK, 0, 0},
   { 0L, 0L, 0L, 0L, 0L, 0L, 0, 0}
};



/*****************************************************************************
 * Constant and Macro's - None
 *****************************************************************************/
 
/*****************************************************************************
 * Global Functions Prototypes
 *****************************************************************************/

/****************************************************************************
 * Global Variables
 ****************************************************************************/ 
/* Add all the variables needed for audio_dev.c to this structure */
extern USB_ENDPOINTS                   usb_desc_ep;
extern USB_AUDIO_UNITS                 usb_audio_unit;
extern DESC_CALLBACK_FUNCTIONS_STRUCT  desc_callback;

struct _usb_audio_dev_info_  * audio_dev_info = NULL;

/*****************************************************************************
 * Local Types - None
 *****************************************************************************/
 
/*****************************************************************************
 * Local Functions Prototypes
 *****************************************************************************/
static void USB_App_Callback(uint_8 event_type, void* val,pointer arg);
static void USB_Notif_Callback(uint_8 event_type,void* val,pointer arg);

/*****************************************************************************
 * Local Variables 
 *****************************************************************************/


/*****************************************************************************
 * Local Functions
 *****************************************************************************/

	
void Main_Task
(
   uint_32 param
)
{
	UNUSED_ARGUMENT (param)
	I2C_Init(); 
	PeripheralInit();
	_time_delay(200);
	_task_block();
}

static void usbd_features_init(struct _usb_features_ * usb_features)
{
	usb_features->copy_protect = 0x01;
	usb_features->cur_mute = 0x01;
	*((uint_16 *)(usb_features->cur_volume)) = 0x8000;   /* {0x00,0x80};*/
	*((uint_16 *)(usb_features->min_volume)) = 0x8000;   /* {0x00,0x80};*/
	*((uint_16 *)(usb_features->max_volume)) = 0x7FFF;   /* {0xFF,0x7F};*/
	*((uint_16 *)(usb_features->res_volume)) = 0x0001;   /* {0x01,0x00};*/
	usb_features->cur_bass = 0x00;
	usb_features->min_bass = 0x80;
	usb_features->max_bass = 0x7F;
	usb_features->res_bass = 0x01;
	usb_features->cur_mid = 0x00;
	usb_features->min_mid = 0x80;
	usb_features->max_mid = 0x7F;
	usb_features->res_mid = 0x01;

	usb_features->cur_treble = 0x01;
	usb_features->min_treble = 0x80;
	usb_features->max_treble = 0x7F;
	usb_features->res_treble = 0x01;

	*((uint_32 *)(usb_features->cur_graphic_equalizer)) = 0x01010101; /*{0x01,0x01,0x01,0x01};*/
	*((uint_32 *)(usb_features->min_graphic_equalizer)) = 0x01010101; /*{0x01,0x01,0x01,0x01};*/
	*((uint_32 *)(usb_features->max_graphic_equalizer)) = 0x01010101; /*{0x01,0x01,0x01,0x01};*/
	*((uint_32 *)(usb_features->res_graphic_equalizer)) = 0x01010101; /*{0x01,0x01,0x01,0x01};*/

	usb_features->cur_automatic_gain = 0x01;

	*((uint_16 *)(usb_features->cur_delay)) = 0x4000;/*{0x00,0x40};*/
	*((uint_16 *)(usb_features->min_delay)) = 0x0000;/*{0x00,0x00};*/
	*((uint_16 *)(usb_features->max_delay)) = 0xFFFF;/*{0xFF,0xFF};*/
	*((uint_16 *)(usb_features->res_delay)) = 0x0100;/*{0x00,0x01};*/

	usb_features->cur_bass_boost = 0x01;

	usb_features->cur_loudness = 0x01;
	
	for (int_8 i = 0; i < 3 ; i++ ) { /* {0x00,0x00,0x01};*/
		usb_features->cur_sampling_frequency[i] = (i >> 1);
		usb_features->min_sampling_frequency[i] = (i >> 1);
		usb_features->max_sampling_frequency[i] = (i >> 1);
		usb_features->res_sampling_frequency[i] = (i >> 1);
	}

	usb_features->cur_pitch = 0x00;
}

uint_32 PeripheralInit(void)
{
	AUDIO_CONFIG_STRUCT       audio_config;
	USB_CLASS_AUDIO_ENDPOINT  * endPoint_ptr;
	uint_8 * buffers = NULL;

	_int_disable();
	audio_dev_info = USB_mem_alloc_zero(sizeof(struct _usb_audio_dev_info_));
	if(audio_dev_info == NULL){
		printf("alloc audio dev info fail\n");
		goto init_fail1;
	}

	buffers = USB_mem_alloc_zero(DATA_BUFF_SIZE);

	if(buffers == NULL){
		printf("alloc audio data buff fail\n");
		goto init_fail3;
	}
	audio_dev_info->adata_buff0 = buffers;

	/* audio_dev_info->cur_buff = buffers; */

	/* audio_dev_info->adata_buff0 = USB_mem_alloc_zero(DATA_BUFF_SIZE);
	if(audio_dev_info->adata_buff0 == NULL){
		printf("alloc audio data buff fail\n");
		goto init_fail3;
	} */

	audio_dev_info->usb_features = USB_mem_alloc_zero(sizeof(struct _usb_features_));
	if(audio_dev_info->usb_features == NULL) {
		printf("alloc dev features fail\n");
		goto init_fail4;
	}
	usbd_features_init(audio_dev_info->usb_features);

	desc_callback.handle = (uint_32)audio_dev_info;
	printf("audio_dev_info = 0x%x\n",audio_dev_info);
  
	{ /* usb dev init start */
		/* Pointer to audio endpoint entry */
		endPoint_ptr = USB_mem_alloc_zero(sizeof(USB_CLASS_AUDIO_ENDPOINT) * AUDIO_DESC_ENDPOINT_COUNT);
		if(endPoint_ptr == NULL) {
			printf("alloc endpoint entry fail\n");
			goto init_fail5;
		}
		/* USB descriptor endpoint */
		audio_config.usb_ep_data = &usb_desc_ep;
		/* USB audio unit */
		audio_config.usb_ut_data = &usb_audio_unit;
		/* Endpoint count */
		audio_config.desc_endpoint_cnt = AUDIO_DESC_ENDPOINT_COUNT;
		/* Application callback */
		audio_config.audio_class_callback.callback = USB_App_Callback;
		/* Application callback argurment */
		audio_config.audio_class_callback.arg = audio_dev_info /*&(audio_dev_info->app_handle)*/;
		/* Vendor callback */
		audio_config.vendor_req_callback.callback = NULL;
		/* Vendor callback argurment */
		audio_config.vendor_req_callback.arg = NULL;
		/* Param callback function */
		audio_config.param_callback.callback = USB_Notif_Callback;
		/* Param callback argurment */
		audio_config.param_callback.arg = audio_dev_info /*&(audio_dev_info->app_handle)*/;
		/* Memory param callback */
		audio_config.mem_param_callback.callback = NULL;
		/* Memory param callback argurment */
		audio_config.mem_param_callback.arg = audio_dev_info /*&(audio_dev_info->app_handle)*/;
		/* Descriptor callback pointer */
		audio_config.desc_callback_ptr =  &desc_callback;
		/* Audio enpoint pointer */
		audio_config.ep = endPoint_ptr;

		/* Initialize timer module */
		if (MQX_OK != _usb_device_driver_install(USBCFG_DEFAULT_DEVICE_CONTROLLER)) {
			printf("Driver could not be installed\n");
			goto init_fail6;
		}

		/* Initialize the USB interface */
		audio_dev_info->app_handle = USB_Class_Audio_Init(&audio_config);

		if (MQX_OK != _lwevent_create(&(audio_dev_info->app_event), LWEVENT_AUTO_CLEAR)) {  /* fixme ,use manual clear */
			printf("\n_lwevent_create app_event failed.\n");
			goto init_fail7;
		}

		/* wait enum complete ,then receive the first data */
		printf("Audio dev init compelete\n") ;
		/* Prepare buffer for first isochronous input */
	} /* usb dev init end */

	_int_enable();

	return  USB_OK;

init_fail7:
	_usb_device_driver_uninstall();
init_fail6:
	USB_mem_free(endPoint_ptr);
init_fail5:
	USB_mem_free(audio_dev_info->usb_features);
init_fail4:
	USB_mem_free(audio_dev_info->adata_buff0);  /* ? */
init_fail3:
	USB_mem_free(audio_dev_info);
init_fail1:
	audio_dev_info = NULL;
	_int_enable();
	return USBERR_ERROR;

}

/******************************************************************************
 * 
 *    @name        USB_App_Callback
 *    
 *    @brief       This function handles the callback  
 *                  
 *    @param       handle : handle to Identify the controller
 *    @param       event_type : value of the event  ( USB_APP_BUS_RESET,USB_APP_ERROR,USB_APP_CONFIG_CHANGED,USB_APP_ENUM_COMPLETE)
 *    @param       val : gives the configuration value 
 * 
 *    @return      None
 *
 *****************************************************************************/
static void USB_App_Callback(uint_8 event_type, void* val,pointer arg)
{
	struct _usb_audio_dev_info_ * dev_info = (struct _usb_audio_dev_info_  *)arg;
   /* UNUSED_ARGUMENT (val) */

	dev_info->state_connection = event_type;

   if(event_type == USB_APP_BUS_RESET) {
   		//printf("USB_APP_BUS_RESET\n");
   		/* if(dev_info->state_connection != USB_APP_BUS_RESET) {
			printf(" RESET deinit dev task at event %d \n",dev_info->state_connection);
			_lwevent_set(&(audio_dev_info->app_event), USBD_APP_TASK_DEINIT_EVENT_MASK);
   		}*/
		snd_deinit();
		// todo for bus reset
   }
   else if(event_type == USB_APP_CONFIG_CHANGED) {
	   //printf("USB_APP_CONFIG_CHANGED\n");
   }
   else if(event_type == USB_APP_ENUM_COMPLETE) {
		printf("USB_APP_ENUM_COMPLETE\n");
		/* start first receive */ /* fixme ,test the ,set freq, set inferface, USB_APP_ENUM_COMPLETE sequently ,liu0923 */
		USB_Class_Audio_Recv_Data(dev_info->app_handle, AUDIO_ISOCHRONOUS_ENDPOINT, dev_info->adata_buff0, DATA_BUFF_SIZE);
   }
   else if(event_type == USB_APP_ERROR) {
		/* add user code for error handling */
		//printf("USB_APP_ERROR ,buff prt=0x%x\n",(uint_32)val);
   }

   //dev_info->state_connection = event_type;
   return;
}

/******************************************************************************
 * 
 *    @name        USB_Notif_Callback
 *    
 *    @brief       This function handles the callback  
 *                  
 *    @param       handle:  handle to Identify the controller
 *    @param       event_type : value of the event
 *    @param       val : gives the configuration value 
 * 
 *    @return      None
 *
 *****************************************************************************/
static void USB_Notif_Callback(uint_8 event_type,void* val,pointer arg)
{
	struct _usb_audio_dev_info_ * dev_info = (struct _usb_audio_dev_info_ *)arg;
	// uint_32 handle = dev_info->app_handle;
	APP_DATA_STRUCT* data_receive = (APP_DATA_STRUCT*)val;

	if(dev_info->state_connection == USB_APP_ENUM_COMPLETE) {
		if(event_type == USB_APP_DATA_RECEIVED) {
			dev_info->adata_size = data_receive->data_size;

			if (MQX_OK !=_lwevent_set(&(dev_info->app_event), USBD_APP_BUFFER0_FULL_EVENT_MASK)) {
				printf("set evetn USBD_APP_BUFFER0_FULL_EVENT_MASK fail\n");
			}
			USB_Class_Audio_Recv_Data(dev_info->app_handle, AUDIO_ISOCHRONOUS_ENDPOINT, dev_info->adata_buff0, DATA_BUFF_SIZE);
		}
	}
}

void PeripheralUninit(void)
{
	snd_deinit();
	printf("App_PeripheralUninit\n");

	_lwevent_set(&(audio_dev_info->app_event), USBD_APP_TASK_DEINIT_EVENT_MASK);
	
	_int_disable();

	/* Uninitialize the USB interface */

	USB0_INTEN = 0;
	USB0_CTL = 0;

	USB0_ISTAT = 0xff;
	USB0_USBTRC0 &= ~USB_USBTRC0_USBRESMEN_MASK;
	USB0_TOKEN = 0;
	USB0_ENDPT0 = 0;
	USB0_ENDPT1 = 0;
	USB0_ENDPT2 = 0;
	USB0_ENDPT3 = 0;
	USB0_ENDPT4 = 0;
	USB0_ENDPT5 = 0;
	USB0_ENDPT6 = 0;
	USB0_ENDPT7 = 0;
	USB0_ENDPT8 = 0;
	USB0_ENDPT9 = 0;
	USB0_ENDPT10 = 0;
	USB0_ENDPT11 = 0;
	USB0_ENDPT12 = 0;
	USB0_ENDPT13 = 0;
	USB0_ENDPT14 = 0;
	USB0_ENDPT15 = 0;
	USB0_ADDR = 0;


	USB0_CTL = USB_CTL_HOSTMODEEN_MASK;
	/* liutest work around for usbhost issue after insterted and plug out periperal */
	//USB0_ISTAT = 0xff;
	
	USB0_ERREN = 0;

	//USB0_FRMNUML = 0;
	//USB0_FRMNUMH = 0;

	USB0_BDTPAGE1 = 0;
	USB0_BDTPAGE2 = 0;
	USB0_BDTPAGE3 = 0;

	USB0_USBCTRL = 0xC0;
	/* end */
	
	USB0_CTL = 0;

	_usb_device_driver_uninstall();

	USB_mem_free(audio_dev_info->adata_buff0);

	USB_mem_free(audio_dev_info->usb_features);
	
	_lwevent_destroy(&audio_dev_info->app_event);

	USB_mem_free(audio_dev_info);
	audio_dev_info = NULL;

	_int_enable();
}


/*FUNCTION*----------------------------------------------------------------
* 
* Function Name  : dev audio task
* Returned Value : None
* Comments       :
*     First function called.
*     callback functions.
* 
*END*--------------------------------------------------------------------*/
void usb_audio_dev_task(uint_32 param)
{
	struct _usb_audio_dev_info_  * dev_info = (struct _usb_audio_dev_info_ *)audio_dev_info;
	_mqx_int	result;
        _mqx_int        app_event_value;

	while (TRUE) {
		//printf("usb_audio_dev_task waitting event\n");
		result = _lwevent_wait_ticks(&(dev_info->app_event), USBD_APP_ALL_EVENTS_MASK, FALSE, 0);

		if(MQX_OK == result) {
			app_event_value = _lwevent_get_signalled();
			//app_event_value = dev_info->app_event.VALUE;
			if ((USBD_APP_TASK_DEINIT_EVENT_MASK & app_event_value)) {
				printf("deinit audio dev task\n");
				snd_deinit();
			}
			else {
				if ((USBD_APP_PCM_STOP_EVENT_MASK & app_event_value)) {
					printf("interface 0\n");
					snd_deinit();  /* or use mute */
				}
				else if ((USBD_APP_PCM_CHANGE_EVENT_MASK & app_event_value)) {
					printf("freq or interface(1<=>2) change to freq=%d,bits=%d,subfrm_bytes=%d \n",
					         dev_info->freq, dev_info->bit_resolution,dev_info->subframe_size);
					/* restart for format set */
                               
					if(dev_info->freq) {
						if(dev_info->bit_resolution == 0) {
							dev_info->bit_resolution = 16;
							printf("set default 16bits\n");
						}
						snd_set_format(dev_info->freq, dev_info->bit_resolution,2);
					}
				}
				else if ((USBD_APP_BUFFER0_FULL_EVENT_MASK & app_event_value)) {
					//printf("\nwirte pcm buff0=0x%x size=%d\n",dev_info->cur_buff,dev_info->adata_size);
					//printf("%d\n",dev_info->adata_size);
					snd_write(dev_info->adata_buff0, dev_info->adata_size);
				}
			}

		}
		else if(LWEVENT_WAIT_TIMEOUT == result) {
			printf("usb dev task wait event timeout\n");
		}
		else {
			printf("usb dev task wait event error\n");
		}
	} /* End while */
}


/* EOF */
