/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 * pcm_lib.h - lib declare head file
 * 
 */

#ifndef __PCM_LIB_H__
#define __PCM_LIB_H__

#define __LITTLE_ENDIAN
#define LIMITED_FORMATS
#define __force

#ifdef  __LITTLE_ENDIAN
#define SNDRV_LITTLE_ENDIAN
#else
#ifdef __BIG_ENDIAN
#define SNDRV_BIG_ENDIAN
#else
#error "Unsupported endian..."
#endif
#endif

typedef unsigned long snd_pcm_uframes_t;
typedef signed long snd_pcm_sframes_t;

enum {
	SNDRV_PCM_STREAM_PLAYBACK = 0,
	SNDRV_PCM_STREAM_CAPTURE,
	SNDRV_PCM_STREAM_LAST = SNDRV_PCM_STREAM_CAPTURE,
};

typedef int snd_pcm_state_t;
#define	/*SNDRV_*/SNDRV_PCM_STATE_OPEN		((__force snd_pcm_state_t) 0) /* stream is open */
#define	SNDRV_PCM_STATE_SETUP		((__force snd_pcm_state_t) 1) /* stream has a setup */
#define	SNDRV_PCM_STATE_PREPARED	((__force snd_pcm_state_t) 2) /* stream is ready to start */
#define	SNDRV_PCM_STATE_RUNNING		((__force snd_pcm_state_t) 3) /* stream is running */
#define	SNDRV_PCM_STATE_XRUN		((__force snd_pcm_state_t) 4) /* stream reached an xrun */
#define	SNDRV_PCM_STATE_DRAINING	((__force snd_pcm_state_t) 5) /* stream is draining */
#define	SNDRV_PCM_STATE_PAUSED		((__force snd_pcm_state_t) 6) /* stream is paused */
#define	SNDRV_PCM_STATE_SUSPENDED	((__force snd_pcm_state_t) 7) /* hardware is suspended */
#define	SNDRV_PCM_STATE_DISCONNECTED	((__force snd_pcm_state_t) 8) /* hardware is disconnected */
#define	SNDRV_PCM_STATE_LAST		SNDRV_PCM_STATE_DISCONNECTED

typedef int  snd_pcm_access_t;
#define	SNDRV_PCM_ACCESS_INTERLEAVED	((__force snd_pcm_access_t) 0) /* interleaved  ,only support this accesse */
#define	SNDRV_PCM_ACCESS_NONINTERLEAVED	((__force snd_pcm_access_t) 1) /* noninterleaved  */
#define	SNDRV_PCM_ACCESS_COMPLEX		((__force snd_pcm_access_t) 2) /* complex  */
#define	SNDRV_PCM_ACCESS_LAST		SNDRV_PCM_ACCESS_COMPLEX

/* pcm format */
typedef int   snd_pcm_format_t;
#define	SNDRV_PCM_FORMAT_S8	((__force snd_pcm_format_t) 0)
#define	SNDRV_PCM_FORMAT_U8	((__force snd_pcm_format_t) 1)
#define	SNDRV_PCM_FORMAT_S16_LE	((__force snd_pcm_format_t) 2)
#define	SNDRV_PCM_FORMAT_S16_BE	((__force snd_pcm_format_t) 3)
#define	SNDRV_PCM_FORMAT_U16_LE	((__force snd_pcm_format_t) 4)
#define	SNDRV_PCM_FORMAT_U16_BE	((__force snd_pcm_format_t) 5)
#define	SNDRV_PCM_FORMAT_S24_LE	((__force snd_pcm_format_t) 6) /* low three bytes */
#define	SNDRV_PCM_FORMAT_S24_BE	((__force snd_pcm_format_t) 7) /* low three bytes */
#define	SNDRV_PCM_FORMAT_U24_LE	((__force snd_pcm_format_t) 8) /* low three bytes */
#define	SNDRV_PCM_FORMAT_U24_BE	((__force snd_pcm_format_t) 9) /* low three bytes */
#define	SNDRV_PCM_FORMAT_S32_LE	((__force snd_pcm_format_t) 10)
#define	SNDRV_PCM_FORMAT_S32_BE	((__force snd_pcm_format_t) 11)
#define	SNDRV_PCM_FORMAT_U32_LE	((__force snd_pcm_format_t) 12)
#define	SNDRV_PCM_FORMAT_U32_BE	((__force snd_pcm_format_t) 13)
#if 0
#define	SNDRV_PCM_FORMAT_FLOAT_LE	((__force snd_pcm_format_t) 14) /* 4-byte float, IEEE-754 32-bit, range -1.0 to 1.0 */
#define	SNDRV_PCM_FORMAT_FLOAT_BE	((__force snd_pcm_format_t) 15) /* 4-byte float, IEEE-754 32-bit, range -1.0 to 1.0 */
#define	SNDRV_PCM_FORMAT_FLOAT64_LE	((__force snd_pcm_format_t) 16) /* 8-byte float, IEEE-754 64-bit, range -1.0 to 1.0 */
#define	SNDRV_PCM_FORMAT_FLOAT64_BE	((__force snd_pcm_format_t) 17) /* 8-byte float, IEEE-754 64-bit, range -1.0 to 1.0 */
#else
#define	SNDRV_PCM_FORMAT_S24_3LE	((__force snd_pcm_format_t) 14)	/* in three bytes */
#define	SNDRV_PCM_FORMAT_S24_3BE	((__force snd_pcm_format_t) 15)	/* in three bytes */
#define	SNDRV_PCM_FORMAT_U24_3LE	((__force snd_pcm_format_t) 16)	/* in three bytes */
#define	SNDRV_PCM_FORMAT_U24_3BE	((__force snd_pcm_format_t) 17)	/* in three bytes */
#endif
#define	SNDRV_PCM_FORMAT_IEC958_SUBFRAME_LE ((__force snd_pcm_format_t) 18) /* IEC-958 subframe, Little Endian */
#define	SNDRV_PCM_FORMAT_IEC958_SUBFRAME_BE ((__force snd_pcm_format_t) 19) /* IEC-958 subframe, Big Endian */
#define	SNDRV_PCM_FORMAT_MU_LAW		((__force snd_pcm_format_t) 20)
#define	SNDRV_PCM_FORMAT_A_LAW		((__force snd_pcm_format_t) 21)
#define	SNDRV_PCM_FORMAT_IMA_ADPCM	((__force snd_pcm_format_t) 22)
#define	SNDRV_PCM_FORMAT_MPEG		((__force snd_pcm_format_t) 23)
#define	SNDRV_PCM_FORMAT_GSM		((__force snd_pcm_format_t) 24)
#define	SNDRV_PCM_FORMAT_SPECIAL	((__force snd_pcm_format_t) 31)
#if 0
#define	SNDRV_PCM_FORMAT_S24_3LE	((__force snd_pcm_format_t) 32)	/* in three bytes */
#define	SNDRV_PCM_FORMAT_S24_3BE	((__force snd_pcm_format_t) 33)	/* in three bytes */
#define	SNDRV_PCM_FORMAT_U24_3LE	((__force snd_pcm_format_t) 34)	/* in three bytes */
#define	SNDRV_PCM_FORMAT_U24_3BE	((__force snd_pcm_format_t) 35)	/* in three bytes */
#else
#define	SNDRV_PCM_FORMAT_FLOAT_LE	((__force snd_pcm_format_t) 32) /* 4-byte float, IEEE-754 32-bit, range -1.0 to 1.0 */
#define	SNDRV_PCM_FORMAT_FLOAT_BE	((__force snd_pcm_format_t) 33) /* 4-byte float, IEEE-754 32-bit, range -1.0 to 1.0 */
#define	SNDRV_PCM_FORMAT_FLOAT64_LE	((__force snd_pcm_format_t) 34) /* 8-byte float, IEEE-754 64-bit, range -1.0 to 1.0 */
#define	SNDRV_PCM_FORMAT_FLOAT64_BE	((__force snd_pcm_format_t) 35) /* 8-byte float, IEEE-754 64-bit, range -1.0 to 1.0 */
#endif
#define	SNDRV_PCM_FORMAT_S20_3LE	((__force snd_pcm_format_t) 36)	/* in three bytes */
#define	SNDRV_PCM_FORMAT_S20_3BE	((__force snd_pcm_format_t) 37)	/* in three bytes */
#define	SNDRV_PCM_FORMAT_U20_3LE	((__force snd_pcm_format_t) 38)	/* in three bytes */
#define	SNDRV_PCM_FORMAT_U20_3BE	((__force snd_pcm_format_t) 39)	/* in three bytes */
#define	SNDRV_PCM_FORMAT_S18_3LE	((__force snd_pcm_format_t) 40)	/* in three bytes */
#define	SNDRV_PCM_FORMAT_S18_3BE	((__force snd_pcm_format_t) 41)	/* in three bytes */
#define	SNDRV_PCM_FORMAT_U18_3LE	((__force snd_pcm_format_t) 42)	/* in three bytes */
#define	SNDRV_PCM_FORMAT_U18_3BE	((__force snd_pcm_format_t) 43)	/* in three bytes */
#define	SNDRV_PCM_FORMAT_G723_24	((__force snd_pcm_format_t) 44) /* 8 samples in 3 bytes */
#define	SNDRV_PCM_FORMAT_G723_24_1B	((__force snd_pcm_format_t) 45) /* 1 sample in 1 byte */
#define	SNDRV_PCM_FORMAT_G723_40	((__force snd_pcm_format_t) 46) /* 8 Samples in 5 bytes */
#define	SNDRV_PCM_FORMAT_G723_40_1B	((__force snd_pcm_format_t) 47) /* 1 sample in 1 byte */
#ifndef  LIMITED_FORMATS
#define	SNDRV_PCM_FORMAT_LAST		SNDRV_PCM_FORMAT_G723_40_1B
#else
#if 0
#define	SNDRV_PCM_FORMAT_LAST		SNDRV_PCM_FORMAT_U32_BE
#else
#define	SNDRV_PCM_FORMAT_LAST		SNDRV_PCM_FORMAT_U24_3BE
#endif
#endif

#ifdef SNDRV_LITTLE_ENDIAN
#define	SNDRV_PCM_FORMAT_S16		SNDRV_PCM_FORMAT_S16_LE
#define	SNDRV_PCM_FORMAT_U16		SNDRV_PCM_FORMAT_U16_LE
#define	SNDRV_PCM_FORMAT_S24		SNDRV_PCM_FORMAT_S24_LE
#define	SNDRV_PCM_FORMAT_U24		SNDRV_PCM_FORMAT_U24_LE
#define	SNDRV_PCM_FORMAT_S32		SNDRV_PCM_FORMAT_S32_LE
#define	SNDRV_PCM_FORMAT_U32		SNDRV_PCM_FORMAT_U32_LE
#define	SNDRV_PCM_FORMAT_FLOAT		SNDRV_PCM_FORMAT_FLOAT_LE
#define	SNDRV_PCM_FORMAT_FLOAT64	SNDRV_PCM_FORMAT_FLOAT64_LE
#define	SNDRV_PCM_FORMAT_IEC958_SUBFRAME SNDRV_PCM_FORMAT_IEC958_SUBFRAME_LE
#endif
#ifdef SNDRV_BIG_ENDIAN
#define	SNDRV_PCM_FORMAT_S16		SNDRV_PCM_FORMAT_S16_BE
#define	SNDRV_PCM_FORMAT_U16		SNDRV_PCM_FORMAT_U16_BE
#define	SNDRV_PCM_FORMAT_S24		SNDRV_PCM_FORMAT_S24_BE
#define	SNDRV_PCM_FORMAT_U24		SNDRV_PCM_FORMAT_U24_BE
#define	SNDRV_PCM_FORMAT_S32		SNDRV_PCM_FORMAT_S32_BE
#define	SNDRV_PCM_FORMAT_U32		SNDRV_PCM_FORMAT_U32_BE
#define	SNDRV_PCM_FORMAT_FLOAT		SNDRV_PCM_FORMAT_FLOAT_BE
#define	SNDRV_PCM_FORMAT_FLOAT64	SNDRV_PCM_FORMAT_FLOAT64_BE
#define	SNDRV_PCM_FORMAT_IEC958_SUBFRAME SNDRV_PCM_FORMAT_IEC958_SUBFRAME_BE
#endif

/* pcm params  */
#define SNDRV_MASK_BITS		64	/* we use so far 64bits only */
#define SNDRV_MASK_SIZE		(SNDRV_MASK_BITS / 32)
#define MASK_OFS(i)			((i) >> 5)
#define MASK_BIT(i)			(1U << ((i) & 31))

struct snd_pcm_params {
	snd_pcm_access_t access;	/* access mode */
	//unsigned int fifo_bits;

	snd_pcm_format_t format;	/* SNDRV_PCM_FORMAT_* */
	unsigned int rate;		/* rate in Hz  ,sample rate */
	unsigned int channels;		/* channels */
	snd_pcm_uframes_t period_size;  /* interrput period ( sample unit ) */
	unsigned int periods;		/* periods */
	/* snd_pcm_uframes_t buffer_size; */
	unsigned int buffer_bytes;

	snd_pcm_uframes_t start_threshold;	/* min hw_avail frames for automatic start */
	snd_pcm_uframes_t stop_threshold;	/* min avail frames for automatic stop */
	snd_pcm_uframes_t silence_threshold;	/* min distance from noise for silence filling */
	snd_pcm_uframes_t silence_size;		/* silence block size */
	/* snd_pcm_uframes_t boundary;*/	/* pointers wrap point */
};

#define  NON_BLOCK   1
#define  RW_BLOCK     0

#define THRESHOLD_BOUNDARY     0xFFFFFFFF

extern unsigned int sys_dma_inited;     /* set by other driver that already inited dma */

/* declare function */
int pcm_format_width(snd_pcm_format_t format);
int pcm_format_physical_width(snd_pcm_format_t format);
int pcm_format_little_endian(snd_pcm_format_t format);

/* extern function */
size_t u_audio_playback(void * handle, void *buf, size_t count);
snd_pcm_sframes_t snd_pcm_lib_write(void * handle, unsigned long data, snd_pcm_uframes_t size ,int nonblock);
int	snd_pcm_params(void * handle, struct snd_pcm_params *params);
int	pcm_stream_init(void ** handle , unsigned int direction,struct snd_pcm_params *params);
void	pcm_stream_deinit(void * handle);
int pcm_stream_rate(void * handle);
int snd_pcm_get_avail_rate(void * handle);
snd_pcm_sframes_t snd_pcm_lib_read(void * handle,  unsigned long data,
									snd_pcm_uframes_t size  ,int nonblock);
size_t sync_capture(void * handle, void *buf, size_t count);

#endif  // __PCM_LIB_H__

