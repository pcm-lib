/**HEADER********************************************************************
* 
* Copyright (c) 2008-2009 Freescale Semiconductor;
* All Rights Reserved                       
*
*************************************************************************** 
*
* THIS SOFTWARE IS PROVIDED BY FREESCALE "AS IS" AND ANY EXPRESSED OR 
* IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES 
* OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  
* IN NO EVENT SHALL FREESCALE OR ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
* INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
* (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR 
* SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) 
* HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
* STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING 
* IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF 
* THE POSSIBILITY OF SUCH DAMAGE.
*
**************************************************************************
*
* $FileName: adc_demo.c$
* $Version : 3.8.35.0$
* $Date    : Sep-21-2012$
*
* Comments:
*
*   This file contains the source for the ADC example program.
*   Two channels are running, one is running in loop, the second one
*   only once.
*
*END************************************************************************/

#include <mqx.h>
#include <bsp.h>

#include "api.h"

#include "pcm_lib.h"
//#if MQX_USE_LWEVENTS
//#include <lwevent.h>
//#endif


/* Task IDs */
#define MAIN_TASK 5

/* Function prototypes */
extern void main_task(uint_32);

const TASK_TEMPLATE_STRUCT MQX_template_list[] =
{
    /* Task Index,  Function,  Stack,  Priority,    Name,       Attributes,             Param,  Time Slice */
    {MAIN_TASK,     main_task, 1000,      8,        "Main",     MQX_AUTO_START_TASK,    0,      0           },
    {0}
};

#define BUFF_SIZE  1024 /* 2048 */             /* same as the capture buff size  (frames)  4 * 256 */
uint_8  test_buff[BUFF_SIZE];
int capturing = 1;

LWGPIO_STRUCT btn1;

    
/*TASK*-----------------------------------------------------
** 
** Task Name    : main_task
** Comments     :
**
*END*-----------------------------------------------------*/
void main_task
   (
      uint_32 initial_data
   )
{
    snd_init(0);        // playback handle
    snd_init(1);        // capture handle

    if (!lwgpio_init(&btn1, BSP_BUTTON1, LWGPIO_DIR_INPUT, LWGPIO_VALUE_NOCHANGE))
    {
        printf("Initializing button 1 GPIO as input failed.\n");
        _task_block();
    }
    
    lwgpio_set_functionality(&btn1,1);
    lwgpio_set_attribute(&btn1, LWGPIO_ATTR_PULL_UP, LWGPIO_AVAL_ENABLE);
    
    printf("%d\n", lwgpio_get_value(&btn1));
    printf("%d\n", lwgpio_get_value(&btn1));

    while(lwgpio_get_value(&btn1) != 0){};
    printf("start capture");
    while(lwgpio_get_value(&btn1) != 1){};
    printf("...\n");

#if 1 /* test reinit */
     if(!snd_read(test_buff, BUFF_SIZE,RW_BLOCK)) { /* if this capture has data.*/
              snd_write(test_buff, BUFF_SIZE);
     }
     snd_deinit(0);        // playback handle
     snd_deinit(1);        // capture handle

     snd_init(0);        // playback handle
     snd_init(1);        // capture handle
#endif     
    
    // start loopback
    while (capturing /*&& !snd_read(test_buff, BUFF_SIZE,RW_BLOCK )*/) {
        printf(".");
        if(!snd_read(test_buff, BUFF_SIZE,RW_BLOCK)) { /* if this capture has data.*/
              snd_write(test_buff, BUFF_SIZE);
        }
        else {
            printf("x");
        }
            
        if(lwgpio_get_value(&btn1) == 0)
            break;
    }

    snd_deinit(0);        // playback handle
    snd_deinit(1);        // capture handle
    printf("\ndone!\n");
   
    _task_block();
}  

/* EOF */
