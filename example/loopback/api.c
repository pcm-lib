
/* usb device play sound api */

#include <mqx.h>
#include <bsp.h>
#include "pcm_lib.h"

// #define DAC_CODEC     /* If the sample define DAC_CODEC to playback at dac then the pcm lib must define KINETS_K60DAC in soc_config.h and rebuild !  */
struct snd_pcm_params  out_params;
void * pcm_out_handle = NULL;

struct snd_pcm_params  in_params;
void * pcm_in_handle = NULL;

extern size_t file_playback(void * handle, void *buf, size_t count);

/*
 * default set is the sample rate 44100, 16 bits, 2 channel
 * umute
 */
int32_t snd_init(int flag)
{
    struct snd_pcm_params *u_params;
    void ** pcm_handle;
    if(flag == SNDRV_PCM_STREAM_PLAYBACK) {
        u_params = &out_params;
        pcm_handle = &pcm_out_handle;
    }
    else {
        u_params = &in_params;
        pcm_handle = &pcm_in_handle;
    }

	_mem_zero((void *) u_params, sizeof(u_params));
	u_params->access = SNDRV_PCM_ACCESS_INTERLEAVED;
	u_params->channels =  1;
	u_params->format = SNDRV_PCM_FORMAT_U16  /* SNDRV_PCM_FORMAT_S16*/;
	u_params->periods =  4;                                 /* the max periods of driver is 4 */
      if(flag == SNDRV_PCM_STREAM_PLAYBACK)
           u_params->period_size  = 512;
      else
	     u_params->period_size  = 256;

	u_params->silence_size = /* u_params.period_size*/0;
	u_params->silence_threshold = THRESHOLD_BOUNDARY; /* THRESHOLD_BOUNDARY -> 0*/
	u_params->rate = 8000;

	u_params->start_threshold = u_params->periods * u_params->period_size / 2;            /* start at the half of the buffer */

      if(flag == SNDRV_PCM_STREAM_PLAYBACK)
        	u_params->stop_threshold = u_params->periods *  u_params->period_size;
      else
            u_params->stop_threshold = u_params->periods *  u_params->period_size * 10;

	if(pcm_stream_init(pcm_handle,/*SNDRV_PCM_STREAM_PLAYBACK*/flag,u_params)) {
		printf(" pcm init failed\n");
		return -1;
	}

	printf("snd_inited %s\n",flag == 0 ? "play" : "capture" );
        return 0;
}

#ifdef DAC_CODEC
extern void dac_skew_sample_rate(int skew);
#endif

/*
 * write samples to device
 * must have correct set before write
 */
int32_t snd_write(uint8_t * pcmStream, uint32_t pcmCnt)
{
#ifdef DAC_CODEC
	int  avail_rate = snd_pcm_get_avail_rate (pcm_out_handle);
        int res = 100 - avail_rate;     /* residue */
        
	if (avail_rate > 0) {      /* these get from ADK2012 */
		static int lastres[2];
		//if (res > (AUDBUFSIZE / 2)) {
		if (res > (50)) {
			// we're overrunning the dac, increase its sample rate
			static int a = 0;
			if (lastres[1] < lastres[0] && lastres[0] < res) {
				a++;
				if (a > 5) {
					int adj = res * 20 / 100 ;
					dac_skew_sample_rate(adj);
					//dbgPrintf("o%d +%d %d\n", res, adj, audsamplerate);
					a = 0;
				}
			}
		} else {
			// we're underunning the dac, decrease its sample rate
			static int a = 0;

			if (/*res == 1 || */ lastres[1] > lastres[0] && lastres[0] > res) {
				a++;
				if (a > 5) {
					int adj =  avail_rate * 20 / 100;
					dac_skew_sample_rate(-adj);
					//dbgPrintf("u%d -%d %d\n", res, adj, audsamplerate);
					a = 0;
				}
			}
		}
		lastres[1] = lastres[0];
		lastres[0] = res;

	}
#endif	
	return /*u_audio_playback*/file_playback(pcm_out_handle, pcmStream, pcmCnt);
}

int32_t snd_read(uint8_t * pcmStream, uint32_t pcmCnt,int block)
{
#if 0
	int result;
	snd_pcm_sframes_t frames;

	struct snd_pcm_runtime *runtime = (struct snd_pcm_runtime *)pcm_in_handle;

	frames = bytes_to_frames(runtime, pcmCnt);
	result = snd_pcm_lib_read(runtime,pcmStream,frames, block/*NON_BLOCK , RW_BLOCK*/);

	return result;
#else
	return sync_capture(pcm_in_handle, pcmStream, pcmCnt);
#endif
        
}

/*
 * collect resources of sound device
 */
int32_t snd_deinit(int flag)
{
    void * pcm_handle;
    if(flag == SNDRV_PCM_STREAM_PLAYBACK) {
        pcm_handle = pcm_out_handle;
    }
    else {
        pcm_handle = pcm_in_handle;
    }
    
	if (pcm_handle) {
		printf("snd_deinit\n");
		pcm_stream_deinit(pcm_handle);
		pcm_handle = NULL;
	}
	else {
		printf("denit pcm stream ,but handle is null\n");
	}
	return 0;
}

/*
 * set pcm sound format: sample rate, bit width, channel number
 */
int32_t snd_set_format(uint32_t sampleRate, uint8_t bitWidth, uint8_t chNum,int flag)
{
    struct snd_pcm_params *u_params;
    void ** pcm_handle;
    if(flag == SNDRV_PCM_STREAM_PLAYBACK) {
        u_params = &out_params;
        pcm_handle = &pcm_out_handle;
    }
    else {
        u_params = &in_params;
        pcm_handle = &pcm_in_handle;
    }
    
	_mem_zero((void *) u_params, sizeof(u_params));

#ifdef DAC_CODEC
	u_params->access = SNDRV_PCM_ACCESS_INTERLEAVED;//SNDRV_PCM_ACCESS_NONINTERLEAVED;
#else
	u_params->access = SNDRV_PCM_ACCESS_INTERLEAVED;
#endif
	u_params->channels =  1;

	u_params->format = SNDRV_PCM_FORMAT_U16;

	u_params->periods =  4;

        if(flag == SNDRV_PCM_STREAM_PLAYBACK)
	        u_params->period_size  = 512;
        else
            u_params->period_size  = 256;
        
	u_params->silence_size =  u_params->period_size;
	//u_params.silence_size =   0 ;
	u_params->silence_threshold = THRESHOLD_BOUNDARY; /* THRESHOLD_BOUNDARY -> 0*/
	u_params->rate = 8000;

	u_params->start_threshold = u_params->periods * u_params->period_size / 2;  /* start at the half of the buffer */
	u_params->stop_threshold = u_params->periods *  u_params->period_size;

	switch (bitWidth)
	{
	case 16:
		u_params->format = SNDRV_PCM_FORMAT_U16;
                break;
	case 24:
		u_params->format =   SNDRV_PCM_FORMAT_S24_3LE ;  /* SNDRV_PCM_FORMAT_S24 */
                break;
	default:
		printf("err:only support sample bits equal 16 or 24\n");
		return -1;
	}

	u_params->rate = sampleRate;
	u_params->periods = 4;
    
        if(flag == SNDRV_PCM_STREAM_PLAYBACK)
	        u_params->period_size  = 512;
        else
            u_params->period_size  = 256;
	//u_params.silence_size =  u_params.period_size;
#ifdef DAC_CODEC
	u_params->silence_size = 0;
#else
	u_params->silence_size = u_params->period_size ;
#endif

	/* start at the half of the buffer */
	u_params->start_threshold = u_params->periods * u_params->period_size / 2;
      if(flag == SNDRV_PCM_STREAM_PLAYBACK)
        	u_params->stop_threshold = u_params->periods *  u_params->period_size;
      else
            u_params->stop_threshold = u_params->periods *  u_params->period_size * 10;

	if (pcm_handle) {
		pcm_stream_deinit(pcm_handle);
                pcm_handle = NULL;
	}
        _time_delay(1);
	if(pcm_stream_init(pcm_handle,flag,u_params)) {
		printf(" set params failed\n");
		return -1;
	}
	/*
	int snd_pcm_params(void * handle,
			     struct snd_pcm_params *params) */

	return 0;
}

/*
 * turn up sound volume
 */
int32_t snd_vol_up(void)
{
	// todo
	return 0;
}

/*
 * turn down sound volume
 */
int32_t snd_vol_down(void)
{
	// todo
	return  0;
}

/*
 * mute sound device
 */
int32_t snd_mute(void)
{
	// todo
	return 0;
}
/*
 * umute device
 */
int32_t snd_umute(void)
{
	// todo
	return 0;
}

/*
 * flush the pcm stream out
 * 
 */
int32_t snd_flush(void)
{
	// todo
	return 0;
}
