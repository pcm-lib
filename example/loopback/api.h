#ifndef _SND_API_
#define _SND_API_



/*
 * sound API
 */

/*
 * default set is the sample rate 44100, 16 bits, 2 channel
 * umute
 */
int32_t snd_init(int flag);

/*
 * write samples to device
 * must have correct set before write
 */
int32_t snd_write(uint8_t * pcmStream, uint32_t pcmCnt);
int32_t snd_read(uint8_t * pcmStream,uint32_t pcmCnt, int block);
/*
 * collect resources of sound device
 */
int32_t snd_deinit(int flag);

/*
 * set pcm sound format: sample rate, bit width, channel number
 */
int32_t snd_set_format(uint32_t sampleRate, uint8_t bitWidth, uint8_t chNum,int flag);

/*
 * mute sound device
 */
int32_t snd_mute(void);
/*
 * umute device
 */
int32_t snd_umute(void);

/*
 * turn up sound volume
 */
int32_t snd_vol_up(void);

/*
 * turn down sound volume
 */
int32_t snd_vol_down(void);
#endif
