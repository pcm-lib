1.  Install MQX 4.0.1 and build all library. (Actually need psp.a bsp.a usbd.a)
2.  Copy pcm_lib to MQX root directory. (If already installed MQX then must change the MQX_ROOT_DIR in all project.)
3.  Open the example\pc_speaker\cw10\twrk60d100m.wsd at the codewarrior.
4.  Build pcm library.
5.  Cover the usbd.a in the MQX lib directory with example\test_images\fixed_usbd_lib\k60\usbd.a.
6.  Build speaker application. (Don't clean it before build.)
7.  Remove all the jumper in J16 at serial board.
8.  Run this image at tower k60d100m board.
9.  Open pc and playback an audio stream.
10. Connect pc host to device at tower SER board connector.


* 08/08/2014 added example of adc dac loopback.
